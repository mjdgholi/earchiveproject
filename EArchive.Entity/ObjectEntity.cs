﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <شی پایگاه داده>
    /// </summary>
public  class ObjectEntity
    {
                #region Properties :

        public Guid ObjectId { get; set; }
        public string ObjectDataBaseName { get; set; }

        public Guid SystemId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ObjectEntity()
        {
        }

        public ObjectEntity(Guid _ObjectId, string ObjectDataBaseName, Guid SystemId, string CreationDate, string ModificationDate, bool IsActive)
        {
            ObjectId = _ObjectId;
            this.ObjectDataBaseName = ObjectDataBaseName;
            this.SystemId = SystemId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
