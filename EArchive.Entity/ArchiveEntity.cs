﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <بایگانی>
    /// </summary>
   public class ArchiveEntity
    {
        
        #region Properties :

        public Guid ArchiveId { get; set; }
        public string ArchiveTitle { get; set; }

        public Guid? ArchiveOwnerId { get; set; }

        public int ArchiveChildNo { get; set; }

        public Guid ArchiveTypeId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ArchiveEntity()
        {
        }

        public ArchiveEntity(Guid _ArchiveId, string ArchiveTitle, Guid? ArchiveOwnerId, int ArchiveChildNo, Guid ArchiveTypeId, string CreationDate, string ModificationDate, bool IsActive)
        {
            ArchiveId = _ArchiveId;
            this.ArchiveTitle = ArchiveTitle;
            this.ArchiveOwnerId = ArchiveOwnerId;
            this.ArchiveChildNo = ArchiveChildNo;
            this.ArchiveTypeId = ArchiveTypeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
