﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <حوزه و موضوع>
    /// </summary>
  public  class Scope_SubjectEntity
    {
                #region Properties :

        public Guid Scope_SubjectId { get; set; }
        public Guid ScopeId { get; set; }

        public Guid SubjectId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public Scope_SubjectEntity()
        {
        }

        public Scope_SubjectEntity(Guid _Scope_SubjectId, Guid ScopeId, Guid SubjectId, string CreationDate, string ModificationDate, bool IsActive)
        {
            Scope_SubjectId = _Scope_SubjectId;
            this.ScopeId = ScopeId;
            this.SubjectId = SubjectId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
