﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <محتوی>
    /// </summary>
    public class EDocumentContentEntity
    {
        
        #region Properties :

        public Guid EDocumentContentId { get; set; }
        public Guid? EDocumentContentOwnerId { get; set; }

        public string EDocumentContentTitle { get; set; }

        public int EDocumentContentChildNo { get; set; }

        public Guid? ObjectId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }



        #endregion

        #region Constrauctors :

        public EDocumentContentEntity()
        {
        }

        public EDocumentContentEntity(Guid _EDocumentContentId, Guid? EDocumentContentOwnerId, string EDocumentContentTitle, int EDocumentContentChildNo, Guid? ObjectId, string CreationDate, string ModificationDate)
        {
            EDocumentContentId = _EDocumentContentId;
            this.EDocumentContentOwnerId = EDocumentContentOwnerId;
            this.EDocumentContentTitle = EDocumentContentTitle;
            this.EDocumentContentChildNo = EDocumentContentChildNo;
            this.ObjectId = ObjectId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;

        }

        #endregion
    }
}
