﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/07>
    /// Description: <حوزه>
    /// </summary>
   public class ScopeEntity
    {
                #region Properties :

        public Guid ScopeId { get; set; }
        public string ScopeTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ScopeEntity()
        {
        }

        public ScopeEntity(Guid _ScopeId, string ScopeTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            ScopeId = _ScopeId;
            this.ScopeTitle = ScopeTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
