﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <سطح دسترسی>
    /// </summary>
    public class AccessLevelEntity
    {
        
        #region Properties :

        public Guid AccessLevelId { get; set; }
        public string AccessLevelTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public AccessLevelEntity()
        {
        }

        public AccessLevelEntity(Guid _AccessLevelId, string AccessLevelTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            AccessLevelId = _AccessLevelId;
            this.AccessLevelTitle = AccessLevelTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
