﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <سطح امنیت>
    /// </summary>
    public class SecurityLevelEntity
    {
        
        #region Properties :

        public Guid SecurityLevelId { get; set; }
        public string SecurityLevelTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SecurityLevelEntity()
        {
        }

        public SecurityLevelEntity(Guid _SecurityLevelId, string SecurityLevelTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            SecurityLevelId = _SecurityLevelId;
            this.SecurityLevelTitle = SecurityLevelTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
