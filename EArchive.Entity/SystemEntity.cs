﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <سیستم>
    /// </summary>
  public   class SystemEntity
    {
      #region Properties :

        public Guid SystemId { get; set; }
        public string SystemTitle { get; set; }

        public string LinkedServerName { get; set; }

        public string DataBaseName { get; set; }

        public string SchemaName { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SystemEntity()
        {
        }

        public SystemEntity(Guid _SystemId, string SystemTitle, string LinkedServerName, string DataBaseName, string SchemaName, string CreationDate, string ModificationDate, bool IsActive)
        {
            SystemId = _SystemId;
            this.SystemTitle = SystemTitle;
            this.LinkedServerName = LinkedServerName;
            this.DataBaseName = DataBaseName;
            this.SchemaName = SchemaName;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
