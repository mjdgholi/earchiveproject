﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/14>
    /// Description: <نوع شی پایگاه داده>
    /// </summary>
  public  class ObjectFieldTypeEntity
    {
                #region Properties :

        public int ObjectFieldTypeId { get; set; }
        public string ObjectFieldTypeTitle { get; set; }

        public string ObjectFieldTypeDescription { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ObjectFieldTypeEntity()
        {
        }

        public ObjectFieldTypeEntity(int _ObjectFieldTypeId, string ObjectFieldTypeTitle, string ObjectFieldTypeDescription, string CreationDate, string ModificationDate, bool IsActive)
        {
            ObjectFieldTypeId = _ObjectFieldTypeId;
            this.ObjectFieldTypeTitle = ObjectFieldTypeTitle;
            this.ObjectFieldTypeDescription = ObjectFieldTypeDescription;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
