﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <فیلد>
    /// </summary>
    public class ObjectFieldEntity
    {
            #region Properties :

        public Guid ObjectFieldId { get; set; }
        public string ObjectFieldName { get; set; }

        public Guid ObjectId { get; set; }
        public int ObjectFieldTypeId { get; set; }
        
        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ObjectFieldEntity()
        {
        }

        public ObjectFieldEntity(Guid _ObjectFieldId, string ObjectFieldName, Guid ObjectId, int ObjectFieldTypeId, string CreationDate, string ModificationDate, bool IsActive)
        {
            ObjectFieldId = _ObjectFieldId;
            this.ObjectFieldName = ObjectFieldName;
            this.ObjectId = ObjectId;
            this.ObjectFieldTypeId = ObjectFieldTypeId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion

    }
}
