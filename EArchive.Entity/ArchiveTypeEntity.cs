﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/11>
    /// Description: <نوع بایگانی>
    /// </summary>
    public class ArchiveTypeEntity
    {
        
        #region Properties :

        public Guid ArchiveTypeId { get; set; }
        public string ArchiveTypeTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ArchiveTypeEntity()
        {
        }

        public ArchiveTypeEntity(Guid _ArchiveTypeId, string ArchiveTypeTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            ArchiveTypeId = _ArchiveTypeId;
            this.ArchiveTypeTitle = ArchiveTypeTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
