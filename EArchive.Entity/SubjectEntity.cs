﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <موضوع>
    /// </summary>
   public class SubjectEntity
    {
        
        #region Properties :

        public Guid SubjectId { get; set; }
        public string SubjectTitle { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public SubjectEntity()
        {
        }

        public SubjectEntity(Guid _SubjectId, string SubjectTitle, string CreationDate, string ModificationDate, bool IsActive)
        {
            SubjectId = _SubjectId;
            this.SubjectTitle = SubjectTitle;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
