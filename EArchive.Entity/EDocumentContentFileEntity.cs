﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/27>
    /// Description: <محتوای فایل>
    /// </summary>
   public class EDocumentContentFileEntity
    {
        

        #region Properties :

            public Guid EDocumentContentFileId { get; set; }
            public Guid? EDocumentContentId { get; set; }

            public Guid? EDocumentContentExternalId { get; set; }

            public string FileTitle { get; set; }

            public Guid FileFormatId { get; set; }

            public Guid SecurityLevelId { get; set; }

            public byte[] FilePath { get; set; }

            public int? PageNo { get; set; }

            public string KeyWord { get; set; }

            public string Description { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }

            public string FileFormatTypeMimeTitle { get; set; }

            public string FileFormatTypeExtensionTitle { get; set; }        

        #endregion

            #region Constrauctors :

            public EDocumentContentFileEntity()
            {
            }

            public EDocumentContentFileEntity(Guid _EDocumentContentFileId, Guid? EDocumentContentId, Guid? EDocumentContentExternalId, string FileTitle, Guid FileFormatId, Guid SecurityLevelId, byte[] FilePath, int? PageNo, string KeyWord, string Description, string CreationDate, string ModificationDate, string FileFormatTypeMimeTitle, string FileFormatTypeExtensionTitle)
            {
                EDocumentContentFileId = _EDocumentContentFileId;
                this.EDocumentContentId = EDocumentContentId;
                this.EDocumentContentExternalId = EDocumentContentExternalId;
                this.FileTitle = FileTitle;
                this.FileFormatId = FileFormatId;
                this.SecurityLevelId = SecurityLevelId;
                this.FilePath = FilePath;
                this.PageNo = PageNo;
                this.KeyWord = KeyWord;
                this.Description = Description;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;
                this.FileFormatTypeMimeTitle = FileFormatTypeMimeTitle;
                this.FileFormatTypeExtensionTitle = FileFormatTypeExtensionTitle;                
            }

            #endregion
    }
}
