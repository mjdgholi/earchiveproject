﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <وضیعت>
    /// </summary>
 public   class StatusEntity
    {
        
        #region Properties :

        public Guid StatusId { get; set; }
        public string StatusTitle { get; set; }

        public int Priority { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public StatusEntity()
        {
        }

        public StatusEntity(Guid _StatusId, string StatusTitle, int Priority, string CreationDate, string ModificationDate, bool IsActive)
        {
            StatusId = _StatusId;
            this.StatusTitle = StatusTitle;
            this.Priority = Priority;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
