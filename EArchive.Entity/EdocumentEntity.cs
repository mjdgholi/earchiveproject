﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <مستندالکترونیکی>
    /// </summary>
   public class EdocumentEntity
    {
        
        #region Properties :

        public Guid EdocumentId { get; set; }
        public string EdocumentNo { get; set; }

        public string EdocumentTitle { get; set; }

        public string EDocumentDate { get; set; }

        public Guid ContentTemplateTreeId { get; set; }

        public Guid EDocumentContentId { get; set; }

        public Guid ArchiveId { get; set; }

        public Guid ScopeId { get; set; }

        public Guid SubjectId { get; set; }

        public Guid StatusId { get; set; }

        public string Description { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }
        public string ContentTemplateTreeTitle { get; set; }
        public string EDocumentContentTitle { get; set; }
        

        #endregion

        #region Constrauctors :

        public EdocumentEntity()
        {
        }

        public EdocumentEntity(Guid _EdocumentId, string EdocumentNo, string EdocumentTitle, string EDocumentDate, Guid ContentTemplateTreeId, Guid EDocumentContentId, Guid ArchiveId, Guid ScopeId, Guid SubjectId, Guid StatusId, string Description, string CreationDate, string ModificationDate, string ContentTemplateTreeTitle, string EDocumentContentTitle)
        {
            EdocumentId = _EdocumentId;
            this.EdocumentNo = EdocumentNo;
            this.EdocumentTitle = EdocumentTitle;
            this.EDocumentDate = EDocumentDate;
            this.ContentTemplateTreeId = ContentTemplateTreeId;
            this.EDocumentContentId = EDocumentContentId;
            this.ArchiveId = ArchiveId;
            this.ScopeId = ScopeId;
            this.SubjectId = SubjectId;
            this.StatusId = StatusId;
            this.Description = Description;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;            
            this.ContentTemplateTreeTitle = ContentTemplateTreeTitle;
            this.EDocumentContentTitle = EDocumentContentTitle;

        }

        #endregion
    }
}
