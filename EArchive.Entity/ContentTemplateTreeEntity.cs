﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <درخت الگویی فهرست بندی>
    /// </summary>
    public class ContentTemplateTreeEntity
    {
                #region Properties :

        public Guid ContentTemplateTreeId { get; set; }
        public Guid? ContentTemplateTreeOwnerId { get; set; }

        public string ContentTemplateTreeTitle { get; set; }

        public int ContentTemplateTreeChildNo { get; set; }

        public Guid? ObjectId { get; set; }

        public string CreationDate { get; set; }

        public string ModificationDate { get; set; }

        public bool IsActive { get; set; }



        #endregion

        #region Constrauctors :

        public ContentTemplateTreeEntity()
        {
        }

        public ContentTemplateTreeEntity(Guid _ContentTemplateTreeId, Guid? ContentTemplateTreeOwnerId, string ContentTemplateTreeTitle, int ContentTemplateTreeChildNo, Guid? ObjectId, string CreationDate, string ModificationDate, bool IsActive)
        {
            ContentTemplateTreeId = _ContentTemplateTreeId;
            this.ContentTemplateTreeOwnerId = ContentTemplateTreeOwnerId;
            this.ContentTemplateTreeTitle = ContentTemplateTreeTitle;
            this.ContentTemplateTreeChildNo = ContentTemplateTreeChildNo;
            this.ObjectId = ObjectId;
            this.CreationDate = CreationDate;
            this.ModificationDate = ModificationDate;
            this.IsActive = IsActive;

        }

        #endregion
    }
}
