﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.Entity
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <نوع فرمت فایل>
    /// </summary>
    public class FileFormatTypeEntity
    {
        
                  #region Properties :

            public Guid FileFormatTypeId { get; set; }
            public string FileFormatTypeExtensionTitle { get; set; }

            public string FileFormatTypeMIMETitle { get; set; }

            public bool IsActive { get; set; }

            public string CreationDate { get; set; }

            public string ModificationDate { get; set; }



            #endregion

            #region Constrauctors :

            public FileFormatTypeEntity()
            {
            }

            public FileFormatTypeEntity(Guid _FileFormatTypeId, string FileFormatTypeExtensionTitle, string FileFormatTypeMIMETitle, bool IsActive, string CreationDate, string ModificationDate)
            {
                FileFormatTypeId = _FileFormatTypeId;
                this.FileFormatTypeExtensionTitle = FileFormatTypeExtensionTitle;
                this.FileFormatTypeMIMETitle = FileFormatTypeMIMETitle;
                this.IsActive = IsActive;
                this.CreationDate = CreationDate;
                this.ModificationDate = ModificationDate;

            }

            #endregion
    }
}
