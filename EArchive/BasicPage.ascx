﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BasicPage.ascx.cs" Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.BasicPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>

<table dir="rtl" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td align="center" class="HeaderOfHomePage" height="20">
            <asp:Label ID="lblSubControlTitle" runat="server" Font-Bold="True" Font-Names="B Titr"
                Font-Size="12pt" ForeColor="White">سامانه آرشیو</asp:Label>
        </td>
    </tr>
</table>
<telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Height="4000px"
    CssClass="MainTableOfASCX">
    <telerik:RadPane ID="RadPane1" runat="server" Width="22px" Scrolling="None">
        <telerik:RadSlidingZone ID="RadSlidingZone1" runat="server" Width="22px" DockedPaneId="RadSlidingPane1"
            ExpandedPaneId="RadSlidingPane1" ClickToOpen="True">
            <telerik:RadSlidingPane ID="RadSlidingPane1" runat="server" Title="مـنـوی اصلی" Width="250px"
                DockOnOpen="True">
                                               <cc1:CustomItcMenuWithRowAccess ID="ItcMenu" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    MenuImageUrl="Training\Images" OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True" >
                </cc1:CustomItcMenuWithRowAccess>
<%--                <cc1:CustomItcMenu ID="ItcMenu" runat="server" MenuTableName="[pub].[t_PubItcMenu]"
                    MenuImageUrl="Training\Images" OnItemClick="CustomItcMenu1_ItemClick" CausesValidation="False"
                    ExpandMode="SingleExpandedItem" AllowCollapseAllItems="True" Width="100%">
                    <ExpandAnimation Duration="600" Type="InCubic" />
                    <CollapseAnimation Duration="600" Type="Linear" />
                </cc1:CustomItcMenu>--%>
            </telerik:RadSlidingPane>
        </telerik:RadSlidingZone>
    </telerik:RadPane>
    <telerik:RadPane ID="RadPane2" runat="server" Height="5000px" Scrolling="None">
        <telerik:RadMultiPage ID="radmultipageMenu" runat="server" Width="100%" OnPageViewCreated="radmultipageMenu_PageViewCreated"
            Height="5000px">
        </telerik:RadMultiPage>
    </telerik:RadPane>
</telerik:RadSplitter>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="ItcMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ItcMenu" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radmultipageMenu">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="ItcMenu" />
                <telerik:AjaxUpdatedControl ControlID="radmultipageMenu" />
                <telerik:AjaxUpdatedControl ControlID="lblSubControlTitle" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
<cc1:ResourceJavaScript ID="ResourceJavaScript1" runat="server" />
