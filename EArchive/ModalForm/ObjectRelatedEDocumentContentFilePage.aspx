﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ObjectRelatedEDocumentContentFilePage.aspx.cs"
    Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.ModalForm.ObjectRelatedEDocumentContentFilePage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body dir="rtl">
    <form id="form1" runat="server" dir="rtl">
    <table width="100%" dir="rtl">
        <tr>
            <td dir="rtl" align="right">
                <telerik:RadScriptManager ID="RadScriptManager1" runat="server">
                </telerik:RadScriptManager>
                <telerik:RadGrid ID="grdObject" runat="server" AllowPaging="True" 
                    AllowSorting="True" AllowFilteringByColumn="True"
                    Width="100%" PageSize="10"  OnNeedDataSource="grdObject_NeedDataSource" 
                    oncolumncreated="grdObject_ColumnCreated">
                    <PagerStyle FirstPageToolTip="صفحه اول" LastPageToolTip="صفحه آخر" NextPagesToolTip="صفحات بعدی"
                        NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; تا &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                        PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                        VerticalAlign="Middle" HorizontalAlign="Center" />
                                              <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                                <ClientEvents OnRowSelected="RowSelected"></ClientEvents>
                                <Selecting AllowRowSelect="True"></Selecting>
                            </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>

    <script type="text/javascript">

        function RowSelected(sender, eventArgs) {
            var oArg = new Object();
            oArg.Title = eventArgs._dataKeyValues.ShowTitleInSelectcontrol;
            oArg.KeyId = eventArgs._dataKeyValues.ShowKeyInSelectcontrol;
            var oWnd = GetRadWindow();
            oWnd.close(oArg);
        }

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }
    </script>