﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.ModalForm
{
    public partial class ObjectRelatedEDocumentContentFilePage : PortalPage
    {
        #region PublicParam:

        private readonly EDocumentContentFileBL _eDocumentContentFileBL = new EDocumentContentFileBL();
        private readonly ObjectFieldBL _objectFieldBL = new ObjectFieldBL();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {




            }

        }

        protected void grdObject_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            if (!String.IsNullOrEmpty(Request["WhereClause"]))
            {
                string keyAndTileForShowinSelectControl;
                var whereClause = Request["WhereClause"];
                string[] array = whereClause.Split(',');
                var edocumentEntity = new EdocumentEntity()
                {
                    EDocumentContentId = Guid.Parse(array[1]),
                    EdocumentNo = array[0]
                };
                grdObject.DataSource = _eDocumentContentFileBL.ObjectRelatedEDocumentContentFile(edocumentEntity);

                //_objectFieldBL.GetKeyAndTileForShowinSelectControl(edocumentEntity, out keyAndTileForShowinSelectControl);
                string[] arr = { "ShowKeyInSelectcontrol,ShowTitleInSelectcontrol" };
                grdObject.MasterTableView.ClientDataKeyNames = arr[0].Split(',');
            }


        }

        protected void grdObject_ColumnCreated(object sender, Telerik.Web.UI.GridColumnCreatedEventArgs e)
        {
            GridColumn gridColumn = e.Column;

            if (gridColumn.ColumnType == "GridBoundColumn" && !IsPersinaText(e.Column.UniqueName))
            {
                e.Column.Visible = false;
            }
        }

        private bool IsPersinaText(string text)
        {
            text = text.ToLower();
            foreach (char chr in text)
            {
                if (chr <= 'z' && chr >= 'a')
                {
                    return false;
                }
            }
            return true;
        }
    }
}