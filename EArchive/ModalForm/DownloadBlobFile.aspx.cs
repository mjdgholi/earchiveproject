﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.ModalForm
{
    public partial class DownloadBlobFile : System.Web.UI.Page
    {
        private readonly EDocumentContentFileBL _eDocumentContentFileBL = new EDocumentContentFileBL();
        protected void Page_Load(object sender, EventArgs e)
        {            
            var eDocumentContentFileEntity = new EDocumentContentFileEntity()
            {
                EDocumentContentFileId = Guid.Parse(Request["EDocumentContentFileId"].ToString())
            };
            var myEDocumentContentFile = _eDocumentContentFileBL.GetSingleFilePathById(eDocumentContentFileEntity);
            WriteFile(myEDocumentContentFile.FilePath, myEDocumentContentFile.FileTitle, myEDocumentContentFile.FileFormatTypeMimeTitle, myEDocumentContentFile.FileFormatTypeExtensionTitle);
        }

        private void WriteFile(Byte[] bytes, string fileName, string fileFormatTypeMimeTitle, string fileFormatTypeExtensionTitle)
        {

            Response.Buffer = true;
            Response.Charset = "";
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);;
            //switch (fileFormatTypeExtensionTitle)
            //{
            //    case ".PDF":
            //        fileFormatTypeMimeTitle = "text/pdf";
            //        break;
            //    case ".DOC":
            //        fileFormatTypeMimeTitle = "text/doc";
            //        break;
            //    case ".xlsx":
            //        fileFormatTypeMimeTitle = "text/xlsx";
            //        break;
            //}

            Response.ContentType = fileFormatTypeMimeTitle;
            //Response.CacheControl = "no-cache";     
            //Response.AddHeader("Pragma", "no-cache;filename=" + fileName);
            Response.AddHeader("content-disposition", "attachment;filename=" + fileName + fileFormatTypeExtensionTitle);
            Response.BinaryWrite(bytes);
            Response.Flush();
            Response.End();
        }
    }
}