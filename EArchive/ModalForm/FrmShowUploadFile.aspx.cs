﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.ModalForm
{
    public partial class FrmShowUploadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Random rnd = new Random();
            ImageFileUpload.ImageUrl = "Handler.ashx?EDocumentContentFileId=''" + Request["WhereClause"] + "'' &unique=" + rnd.Next();
        }
    }
}