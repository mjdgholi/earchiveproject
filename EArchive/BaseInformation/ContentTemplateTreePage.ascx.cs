﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <درخت الگویی فهرست بندی>
    /// </summary>
    public partial class ContentTemplateTreePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ContentTemplateTreeBL _contentTemplateTreeBL = new ContentTemplateTreeBL();
        private readonly ObjectBL _objectBL = new ObjectBL();



        #endregion


   

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            cmbObject.ClearSelection();
            txtContentTemplateTreeChildNo.Text = "";
            txtContentTemplateTreeTitle.Text = "";
            radContentTemplateTree.UnselectAllNodes();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var contentTemplateTreeEntity = new ContentTemplateTreeEntity()
            {
                ContentTemplateTreeId = Guid.Parse(ViewState["ContentTemplateTreeId"].ToString())
            };
            var myecontentTemplateTree = _contentTemplateTreeBL.GetSingleById(contentTemplateTreeEntity);
            cmbObject.SelectedValue = myecontentTemplateTree.ObjectId.ToString();
            if (cmbObject.FindItemByValue(myecontentTemplateTree.ObjectId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردآبجکت انتخاب شده معتبر نمی باشد.");
            }
            txtContentTemplateTreeChildNo.Text = myecontentTemplateTree.ContentTemplateTreeChildNo.ToString();
            txtContentTemplateTreeTitle.Text = myecontentTemplateTree.ContentTemplateTreeTitle;
            SetSelectedNodeForTree((myecontentTemplateTree.ContentTemplateTreeOwnerId == null) ? (Guid?)null : Guid.Parse(myecontentTemplateTree.ContentTemplateTreeOwnerId.ToString()));
            cmbIsActive.SelectedValue = myecontentTemplateTree.IsActive.ToString();
        }

        private void SetSelectedNodeForTree(Guid? EDocumentContentId)
        {
            SetLoadTreeContentTemplateTree();
            if (EDocumentContentId != null)
            {
                foreach (var radTreeNode in radContentTemplateTree.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == EDocumentContentId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        void SetComboBox()
        {
            cmbObject.Items.Clear();
            cmbObject.Items.Insert(0, new RadComboBoxItem(""));
            cmbObject.DataTextField = "ObjectDataBaseName";
            cmbObject.DataValueField = "ObjectId";
            cmbObject.DataSource = _objectBL.GetAllIsActive();
            cmbObject.DataBind();


        }
        private void SetLoadTreeContentTemplateTree()
        {
            radContentTemplateTree.DataFieldID = "ContentTemplateTreeId";
            radContentTemplateTree.DataValueField = "ContentTemplateTreeId";
            radContentTemplateTree.DataFieldParentID = "ContentTemplateTreeOwnerId";
            radContentTemplateTree.DataTextField = "ContentTemplateTreeTitle";
            radContentTemplateTree.DataSource = _contentTemplateTreeBL.GetAllIsActive();
            radContentTemplateTree.DataBind();
        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                SetLoadTreeContentTemplateTree();
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid contentTemplateTreeId;
                var contentTemplateTreeOwnerId = (string.IsNullOrEmpty(radContentTemplateTree.SelectedValue) ? (Guid?)null : Guid.Parse(radContentTemplateTree.SelectedNode.Value));
                var contentTemplateTreeEntity = new ContentTemplateTreeEntity()
                {                    
                    ObjectId = (cmbObject.SelectedIndex>0?Guid.Parse(cmbObject.SelectedValue):(Guid?)null),
                    ContentTemplateTreeChildNo = int.Parse(txtContentTemplateTreeChildNo.Text),
                    ContentTemplateTreeTitle = txtContentTemplateTreeTitle.Text,
                    ContentTemplateTreeOwnerId = contentTemplateTreeOwnerId,       
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };
                _contentTemplateTreeBL.Add(contentTemplateTreeEntity, out contentTemplateTreeId);
                SetClearToolBox();
                SetSelectedNodeForTree(contentTemplateTreeId);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var contentTemplateTreeOwnerId = (string.IsNullOrEmpty(radContentTemplateTree.SelectedValue) ? (Guid?)null : Guid.Parse(radContentTemplateTree.SelectedNode.Value));
                var contentTemplateTreeEntity = new ContentTemplateTreeEntity()
                {
                    ContentTemplateTreeId = Guid.Parse(ViewState["ContentTemplateTreeId"].ToString()),
                    ObjectId = (cmbObject.SelectedIndex > 0 ? Guid.Parse(cmbObject.SelectedValue) : (Guid?)null),
                    ContentTemplateTreeChildNo = int.Parse(txtContentTemplateTreeChildNo.Text),
                    ContentTemplateTreeTitle = txtContentTemplateTreeTitle.Text,
                    ContentTemplateTreeOwnerId = contentTemplateTreeOwnerId,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value)
                };

                _contentTemplateTreeBL.Update(contentTemplateTreeEntity);
                SetClearToolBox();
                SetPanelFirst();
                SetSelectedNodeForTree(Guid.Parse(ViewState["ContentTemplateTreeId"].ToString()));
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetPanelFirst();
        }

        protected void radContentTemplateTree_ContextMenuItemClick(object sender, Telerik.Web.UI.RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    ViewState["ContentTemplateTreeId"] = Guid.Parse(clickedNode.Value);
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {
                    var contentTemplateTreeEntity = new ContentTemplateTreeEntity()
                    {
                        ContentTemplateTreeId = Guid.Parse(e.Node.Value),
                    };
                    _contentTemplateTreeBL.Delete(contentTemplateTreeEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetLoadTreeContentTemplateTree();
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}