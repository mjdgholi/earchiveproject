﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EDocumentContentPage.ascx.cs" Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation.EDocumentContentPage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
<script type="text/javascript" >
    function NodeClicking(sender, eventArgs) {
        var node = eventArgs.get_node();
        var isSelected = node.get_selected();

        if (isSelected) {
            node.set_selected(false);
        }
        else {
            node.set_selected(true);
        }

        eventArgs.set_cancel(true);
    }

</script>

</telerik:RadScriptBlock>

<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF; " dir="rtl">

        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" OnClick="btnSave_Click" ValidationGroup="validEDocumentContent">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click" ValidationGroup="validEDocumentContent">
                                    <icon primaryiconcssclass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click" CausesValidation="False">
                                    <icon primaryiconcssclass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                  
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                  
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Panel ID="pnlDetail" runat="server">
                    <table width="100%">
                           <tr>
                           <td width="10%">
                                <asp:Label runat="server" ID="lblEDocumentContentTitle">عنوان محتوی بایگانی <font color="red">*</font>:</asp:Label>
                           </td>
                            <td  >
                                <asp:TextBox ID="txtEDocumentContentTitle" runat="server" Width="400px"></asp:TextBox>
                                              <asp:RequiredFieldValidator ID="ValidEDocumentContentTitle" runat="server" ControlToValidate="txtEDocumentContentTitle" ValidationGroup="validEDocumentContent"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>           
                            </td>
                        </tr>
                                                   <tr>
                           <td >
                                <asp:Label runat="server" ID="lblObjec">آبجکت:</asp:Label>
                           </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbObject" runat="server" 
                                    AppendDataBoundItems="True" Width="400px">
                                </cc1:CustomRadComboBox>
                                        
                            </td>
                        </tr>
                                                                           <tr>
                           <td >
                                <asp:Label runat="server" ID="lblEDocumentContentChildNo">ترتیب<font color="red">*</font>:</asp:Label>
                           </td>
                            <td>
                                
                                <cc1:NumericTextBox ID="txtEDocumentContentChildNo" runat="server" Text=""></cc1:NumericTextBox> 
                                                           <asp:RequiredFieldValidator ID="rfvEDocumentContentChildNoo" runat="server" ControlToValidate="txtEDocumentContentChildNo" ValidationGroup="validEDocumentContent"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>        
                            </td>
                        </tr>


                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">

                    <table width="100%">
                        <tr>
                            <td>
                            <telerik:RadTreeView ID="radTreeEDocumentContent" 
                                     runat="server" BorderColor="#999999" BorderStyle="Solid"
                                    BorderWidth="1px" dir="rtl"  CausesValidation="False"
                                    OnClientNodeClicking="NodeClicking"                                      
                                       Height="500px" Width="100%" 
                                    oncontextmenuitemclick="radTreeEDocumentContent_ContextMenuItemClick" >
                                    <ContextMenus>
                                        <telerik:RadTreeViewContextMenu ID="MainContextMenu" Width="50%" runat="server" >
                                            <Items>
                                                <telerik:RadMenuItem Value="edit" Text="ویرایش" ImageUrl="../Images/Edit.png" PostBack="True">
                                                </telerik:RadMenuItem>
                                                <telerik:RadMenuItem Value="remove" Text="حذف" ImageUrl="../Images/Delete.png"  OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;">
                                                </telerik:RadMenuItem>
                                            </Items>
                                            <CollapseAnimation Type="OutQuint"></CollapseAnimation>
                                        </telerik:RadTreeViewContextMenu>
                                    </ContextMenus>
                                </telerik:RadTreeView>


                            </td>
                        </tr>
                    </table>
        
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="radTreeEDocumentContent" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="radTreeEDocumentContent" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="radTreeEDocumentContent" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radTreeEDocumentContent">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="radTreeEDocumentContent" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>