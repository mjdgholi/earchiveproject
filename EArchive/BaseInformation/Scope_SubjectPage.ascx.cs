﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <حوزه و موضوع>
    /// </summary>
    public partial class Scope_SubjectPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly Scope_SubjectBL _scopeSubjectBL = new Scope_SubjectBL();
        private  readonly  ScopeBL _scopeBL=new ScopeBL();
        private  readonly  SubjectBL _subjectBL=new SubjectBL();

        private const string TableName = "EArchive.v_ScopeSubject";
        private const string PrimaryKey = "Scope_SubjectId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            cmbScopeTitle.ClearSelection();
            cmbSubjectTitle.ClearSelection();
            cmbIsActive.SelectedIndex = 1;

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var scopeSubjectEntity = new Scope_SubjectEntity()
            {
                Scope_SubjectId = Guid.Parse(ViewState["Scope_SubjectId"].ToString())
            };
            var myscopeSubject = _scopeSubjectBL.GetSingleById(scopeSubjectEntity);
            cmbScopeTitle.SelectedValue = myscopeSubject.ScopeId.ToString();
            cmbSubjectTitle.SelectedValue = myscopeSubject.SubjectId.ToString();
            cmbIsActive.SelectedValue = myscopeSubject.IsActive.ToString();
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdScope_Subject,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }


        void SetComboBox()
        {
            cmbScopeTitle.Items.Clear();
            cmbScopeTitle.Items.Insert(0,new RadComboBoxItem(""));            
            cmbScopeTitle.DataTextField = "ScopeTitle";
            cmbScopeTitle.DataValueField = "ScopeId";
            cmbScopeTitle.DataSource = _scopeBL.GetAllIsActive();
            cmbScopeTitle.DataBind();

            cmbSubjectTitle.Items.Clear();
            cmbSubjectTitle.Items.Add(new RadComboBoxItem(""));            
            cmbSubjectTitle.DataTextField = "SubjectTitle";
            cmbSubjectTitle.DataValueField = "SubjectId";
            cmbSubjectTitle.DataSource = _subjectBL.GetAllIsActive();
            cmbSubjectTitle.DataBind();

        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetComboBox();
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Guid scopeSubjectId;
                ViewState["WhereClause"] = "";

                var scopeSubjectEntity = new Scope_SubjectEntity()
                {
                    ScopeId = Guid.Parse(cmbScopeTitle.SelectedValue),
                    SubjectId = Guid.Parse(cmbSubjectTitle.SelectedValue),                        
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };

                _scopeSubjectBL.Add(scopeSubjectEntity, out scopeSubjectId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, scopeSubjectId);
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "1 = 1";
                if (cmbScopeTitle.SelectedIndex >0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ScopeId ='" +
                                               cmbScopeTitle.SelectedValue + "'";
                if (cmbSubjectTitle.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SubjectId ='" +
                                               cmbSubjectTitle.SelectedValue + "'";
                if (cmbIsActive.SelectedIndex != 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                               (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                grdScope_Subject.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdScope_Subject.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                var scopeSubjectEntity = new Scope_SubjectEntity()
                {
                    Scope_SubjectId = Guid.Parse(ViewState["Scope_SubjectId"].ToString()),
                    ScopeId = Guid.Parse(cmbScopeTitle.SelectedValue),
                    SubjectId = Guid.Parse(cmbSubjectTitle.SelectedValue),
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                };

                _scopeSubjectBL.Update(scopeSubjectEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid(ViewState["Scope_SubjectId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdScope_Subject_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
 try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["Scope_SubjectId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var scopeSubjectEntity = new Scope_SubjectEntity()
                    {
                        Scope_SubjectId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _scopeSubjectBL.Delete(scopeSubjectEntity);
                    var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>

     

        protected void grdScope_Subject_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdScope_Subject_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdScope_Subject_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdScope_Subject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}