﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <موضوع>
    /// </summary>
    public partial class SubjectPage : ItcBaseControl
    {
            #region PublicParam:
            private readonly SubjectBL _subjectBL = new SubjectBL();

            private const string TableName = "EArchive.t_Subject";
            private const string PrimaryKey = "SubjectId";

            #endregion

            /// <summary>
            /// پیاده سازی PageLoad
            /// </summary>
            public override void InitControl()
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            #region Procedure:

            /// <summary>
            /// ست کردن وضعیت کنترل ها به حالت اولیه
            /// </summary>
            private void SetPanelFirst()
            {
                btnSave.Visible = true;
                btnSearch.Visible = true;
                btnBack.Visible = false;
                btnEdit.Visible = false;
                btnShowAll.Visible = false;
            }

            /// <summary>
            /// ست کردن وضعیت کنترل ها برای ویرایش
            /// </summary>
            private void SetPanelLast()
            {
                btnSave.Visible = false;
                btnSearch.Visible = false;
                btnBack.Visible = true;
                btnEdit.Visible = true;
            }

            /// <summary>
            /// خالی کردن کنترل ها
            /// </summary>
            private void SetClearToolBox()
            {
                txtSubjectTitle.Text = "";            
                cmbIsActive.SelectedIndex = 1;

            }

            /// <summary>
            /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
            /// </summary>
            private void SetDataShow()
            {
                var subjectEntity = new SubjectEntity()
                {
                    SubjectId = Guid.Parse(ViewState["SubjectId"].ToString())
                };
                var mysubject = _subjectBL.GetSingleById(subjectEntity);
                txtSubjectTitle.Text = mysubject.SubjectTitle;            
                cmbIsActive.SelectedValue = mysubject.IsActive.ToString();
            }

            /// <summary>
            ///  تعیین نوع مرتب سازی - صعودی یا نزولی
            /// </summary>
            private void SetSortType()
            {
                if (ViewState["SortType"].ToString() == "ASC")
                    ViewState["SortType"] = "DESC";
                else
                    ViewState["SortType"] = "ASC";

            }


            private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
            {
                var gridParamEntity = new GridParamEntity
                {
                    TableName = TableName,
                    PrimaryKey = PrimaryKey,
                    RadGrid = grdSubject,
                    PageSize = pageSize,
                    CurrentPage = currentpPage,
                    WhereClause = ViewState["WhereClause"].ToString(),
                    OrderBy = ViewState["SortExpression"].ToString(),
                    SortType = ViewState["SortType"].ToString(),
                    RowSelectGuidId = rowSelectGuidId
                };
                return gridParamEntity;
            }

            #endregion

            #region ControlEvent:

            protected void Page_Load(object sender, EventArgs e)
            {
                if (!IsPostBack)
                {
                    try
                    {
                        ViewState["WhereClause"] = "  ";
                        ViewState["SortExpression"] = " ";
                        ViewState["SortType"] = "Asc";
                        var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                        DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                        SetPanelFirst();
                        SetClearToolBox();
                    }
                    catch (Exception ex)
                    {

                        CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                    }
                }
            }

            /// <summary>
            /// متد ثبت اطلاعات وارد شده در پایگاه داده
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// 
            protected void btnSave_Click(object sender, EventArgs e)
            {
                try
                {
                    Guid subjectId;
                    ViewState["WhereClause"] = "";

                    var subjectEntity = new SubjectEntity()
                    {
                        SubjectTitle= txtSubjectTitle.Text.Trim(),                    
                        IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    };

                    _subjectBL.Add(subjectEntity, out subjectId);
                    SetClearToolBox();
                    SetPanelFirst();
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, subjectId);
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                catch (Exception ex)
                {
                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }

            }

            /// <summary>
            /// جستجو بر اساس آیتمهای انتخابی
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>

            protected void btnSearch_Click(object sender, EventArgs e)
            {
                try
                {
                    ViewState["WhereClause"] = "1 = 1";
                    if (txtSubjectTitle.Text.Trim() != "")
                        ViewState["WhereClause"] = ViewState["WhereClause"] + " and SubjectTitle Like N'%" +
                                                   FarsiToArabic.ToArabic(txtSubjectTitle.Text.Trim()) + "%'";                
                    if (cmbIsActive.SelectedIndex != 0)
                        ViewState["WhereClause"] = ViewState["WhereClause"] + " And IsActive='" +
                                                   (cmbIsActive.SelectedItem.Value.Trim()) + "'";
                    grdSubject.MasterTableView.CurrentPageIndex = 0;
                    SetPanelFirst();
                    btnShowAll.Visible = true;
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    if (grdSubject.VirtualItemCount == 0)
                    {
                        CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                    }
                }
                catch (Exception ex)
                {
                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// نمایش تمامی رکوردها در گرید
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            /// 
            protected void btnShowAll_Click(object sender, EventArgs e)
            {
                try
                {
                    SetPanelFirst();
                    SetClearToolBox();
                    ViewState["WhereClause"] = "";
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>

            protected void btnEdit_Click(object sender, EventArgs e)
            {
                try
                {
                    var subjectEntity = new SubjectEntity()
                    {
                        SubjectId = Guid.Parse(ViewState["SubjectId"].ToString()),
                        SubjectTitle = txtSubjectTitle.Text.Trim(),
                        IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    };

                    _subjectBL.Update(subjectEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid(ViewState["SubjectId"].ToString()));
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                catch (Exception ex)
                {
                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// برگرداندن صفحه به وضعیت اولیه
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>

            protected void btnBack_Click(object sender, EventArgs e)
            {
                try
                {
                    SetClearToolBox();
                    SetPanelFirst();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }

            /// <summary>
            /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdSubject_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["SubjectId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";

                    var subjectEntity = new SubjectEntity()
                    {
                        SubjectId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _subjectBL.Delete(subjectEntity);
                    var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>



        protected void grdSubject_PageIndexChanged(object sender, Telerik.Web.UI.GridPageChangedEventArgs e)
        {

            try
            {
                var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>
        protected void grdSubject_PageSizeChanged(object sender, Telerik.Web.UI.GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>


        protected void grdSubject_SortCommand(object sender, Telerik.Web.UI.GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdSubject.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion
    }
}