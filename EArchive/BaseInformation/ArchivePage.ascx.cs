﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <بایگانی>
    /// </summary>
    public partial class ArchivePage : ItcBaseControl
    {
        #region PublicParam:
        private readonly ArchiveBL _archiveBL = new ArchiveBL();
        private readonly ArchiveTypeBL _archiveTypeBL = new ArchiveTypeBL();        

        

        #endregion


        private void SetLoadTreeArchive()
        {
            radTreeArchive.DataFieldID = "ArchiveId";
            radTreeArchive.DataValueField = "ArchiveId";
            radTreeArchive.DataFieldParentID = "ArchiveOwnerId";
            radTreeArchive.DataTextField = "ArchiveTitle";
            radTreeArchive.DataSource = _archiveBL.GetAllIsActive();
            radTreeArchive.DataBind();
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;            
            btnBack.Visible = false;
            btnEdit.Visible = false;            
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;            
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            cmbArchiveType.ClearSelection();            
            cmbIsActive.SelectedIndex = 1;
            txtArchiveChildNo.Text = "";
            txtArchiveTypeTitle.Text = "";
            radTreeArchive.UnselectAllNodes();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var archiveEntity = new ArchiveEntity()
            {
                ArchiveId = Guid.Parse(ViewState["ArchiveId"].ToString())
            };
            var myarchive = _archiveBL.GetSingleById(archiveEntity);
            cmbArchiveType.SelectedValue = myarchive.ArchiveTypeId.ToString();            
            cmbIsActive.SelectedValue = myarchive.IsActive.ToString();
            txtArchiveChildNo.Text = myarchive.ArchiveChildNo.ToString();
            txtArchiveTypeTitle.Text = myarchive.ArchiveTitle;
            SetSelectedNodeForTree((myarchive.ArchiveOwnerId== null) ? (Guid?)null : Guid.Parse(myarchive.ArchiveOwnerId.ToString()));
        }

        private void SetSelectedNodeForTree(Guid? ArchiveId)
        {
            SetLoadTreeArchive();
            if (ArchiveId != null)
            {
                foreach (var radTreeNode in radTreeArchive.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == ArchiveId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        void SetComboBox()
        {
            cmbArchiveType.Items.Clear();
            cmbArchiveType.Items.Insert(0, new RadComboBoxItem(""));
            cmbArchiveType.DataTextField = "ArchiveTypeTitle";
            cmbArchiveType.DataValueField = "ArchiveTypeId";
            cmbArchiveType.DataSource = _archiveTypeBL.GetAll();
            cmbArchiveType.DataBind();


        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }


        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {                
                SetLoadTreeArchive();
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {                             
                Guid archiveId;
                var archiveOwnerId = (string.IsNullOrEmpty(radTreeArchive.SelectedValue)?(Guid?)null:Guid.Parse(radTreeArchive.SelectedNode.Value));
                var archiveEntity=new ArchiveEntity()
                {
                  ArchiveTitle  = txtArchiveTypeTitle.Text,
                  ArchiveTypeId = Guid.Parse(cmbArchiveType.SelectedValue),
                  ArchiveOwnerId = archiveOwnerId,
                  IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                  ArchiveChildNo = int.Parse(txtArchiveChildNo.Text),                  
                };
                
                _archiveBL.Add(archiveEntity, out archiveId);
                SetClearToolBox();
                SetSelectedNodeForTree(archiveId);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {             
                var archiveOwnerId = (string.IsNullOrEmpty(radTreeArchive.SelectedValue) ? (Guid?)null : Guid.Parse(radTreeArchive.SelectedNode.Value));
                var archiveEntity = new ArchiveEntity()
                {
                    ArchiveId = Guid.Parse(ViewState["ArchiveId"].ToString()),
                    ArchiveTitle = txtArchiveTypeTitle.Text,
                    ArchiveTypeId = Guid.Parse(cmbArchiveType.SelectedValue),
                    ArchiveOwnerId = archiveOwnerId,
                    IsActive = bool.Parse(cmbIsActive.SelectedItem.Value),
                    ArchiveChildNo = int.Parse(txtArchiveChildNo.Text),
                };

                _archiveBL.Update(archiveEntity);
                SetClearToolBox(); 
                SetPanelFirst();
                SetSelectedNodeForTree(Guid.Parse(ViewState["ArchiveId"].ToString()));
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetPanelFirst();
        }

        protected void radTreeArchive_ContextMenuItemClick(object sender, Telerik.Web.UI.RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    ViewState["ArchiveId"] = Guid.Parse(clickedNode.Value);
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {

                    RadTreeNode radTreeNode = e.Node.ParentNode;
                    var archiveEntity = new ArchiveEntity()
                    {
                        ArchiveId = Guid.Parse(clickedNode.Value)
                    };
                    _archiveBL.Delete(archiveEntity);                    
                    SetPanelFirst();
                    SetClearToolBox();
                    SetLoadTreeArchive();
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}