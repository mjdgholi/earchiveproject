﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArchivePage.ascx.cs" Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation.ArchivePage" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
<script type="text/javascript" >
    function NodeClicking(sender, eventArgs) {
        var node = eventArgs.get_node();
        var isSelected = node.get_selected();

        if (isSelected) {
            node.set_selected(false);
        }
        else {
            node.set_selected(true);
        }

        eventArgs.set_cancel(true);
    }

</script>

</telerik:RadScriptBlock>

<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF; " dir="rtl">

        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" OnClick="btnSave_Click">
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click">
                                    <icon primaryiconcssclass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click">
                                    <icon primaryiconcssclass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                  
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                  
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Panel ID="pnlDetail" runat="server">
                    <table width="100%">
                           <tr>
                           <td width="10%">
                                <asp:Label runat="server" ID="lblArchiveTypeTitle">عنوان  بایگانی<font color="red">*</font>:</asp:Label>
                           </td>
                            <td  >
                                <asp:TextBox ID="txtArchiveTypeTitle" runat="server"></asp:TextBox>
                                              <asp:RequiredFieldValidator ID="ValidArchiveTypeTitle" runat="server" ControlToValidate="txtArchiveTypeTitle"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>           
                            </td>
                        </tr>
                                                   <tr>
                           <td >
                                <asp:Label runat="server" ID="lblArchiveType">نوع بایگانی<font color="red">*</font>:</asp:Label>
                           </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbArchiveType" runat="server" AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                                                  <asp:RequiredFieldValidator ID="rfvArchiveType" runat="server" ControlToValidate="cmbArchiveType"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>          
                            </td>
                        </tr>
                                                                           <tr>
                           <td >
                                <asp:Label runat="server" ID="lblArchiveChildNo">ترتیب<font color="red">*</font>:</asp:Label>
                           </td>
                            <td>
                                
                                <cc1:NumericTextBox ID="txtArchiveChildNo" runat="server" Text=""></cc1:NumericTextBox> 
                                                           <asp:RequiredFieldValidator ID="rfvArchiveChildNo" runat="server" ControlToValidate="txtArchiveChildNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>        
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>
                                    وضیعت رکورد<font color="red">*</font>:</label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbIsActive" runat="server">
                                    <items>
                                        <telerik:RadComboBoxItem Text=""  Value="" />
                                        <telerik:RadComboBoxItem Text="فعال" Value="True" />
                                        <telerik:RadComboBoxItem Text="غیرفعال" Value="False" />
                                    </items>
                                </cc1:CustomRadComboBox>
                                                                                 <asp:RequiredFieldValidator ID="rfvalidatorVisibility" runat="server" ControlToValidate="cmbIsActive"
                                    ErrorMessage="وضیعت رکورد" InitialValue=" ">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>

                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">

                    <table width="100%">
                        <tr>
                            <td>
                            <telerik:RadTreeView ID="radTreeArchive" 
                                     runat="server" BorderColor="#999999" BorderStyle="Solid"
                                    BorderWidth="1px" dir="rtl"  CausesValidation="False"
                                    OnClientNodeClicking="NodeClicking"                                      
                                       Height="500px" Width="100%" 
                                    oncontextmenuitemclick="radTreeArchive_ContextMenuItemClick">
                                    <ContextMenus>
                                        <telerik:RadTreeViewContextMenu ID="MainContextMenu" Width="50%" runat="server" >
                                            <Items>
                                                <telerik:RadMenuItem Value="edit" Text="ویرایش" ImageUrl="../Images/Edit.png" PostBack="True">
                                                </telerik:RadMenuItem>
                                                <telerik:RadMenuItem Value="remove" Text="حذف" ImageUrl="../Images/Delete.png"  OnClientClick="if (!confirm('آیا اطلاعات رکورد حذف شود؟?')) return false;">
                                                </telerik:RadMenuItem>
                                            </Items>
                                            <CollapseAnimation Type="OutQuint"></CollapseAnimation>
                                        </telerik:RadTreeViewContextMenu>
                                    </ContextMenus>
                                </telerik:RadTreeView>


                            </td>
                        </tr>
                    </table>
        
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="radTreeArchive" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="radTreeArchive" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="radTreeArchive" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radTreeArchive">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="radTreeArchive" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" >
</telerik:RadAjaxLoadingPanel>
