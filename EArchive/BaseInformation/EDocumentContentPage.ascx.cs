﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BaseInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <محتوی>
    /// </summary>
    public partial class EDocumentContentPage : ItcBaseControl
    {
        #region PublicParam:
        private readonly EDocumentContentBL _eDocumentContentBL = new EDocumentContentBL();
        private readonly ObjectBL _objectBL = new ObjectBL();



        #endregion


        private void SetLoadTreeEDocumentContent()
        {

            radTreeEDocumentContent.DataFieldID = "EDocumentContentId";
            radTreeEDocumentContent.DataValueField = "EDocumentContentId";
            radTreeEDocumentContent.DataFieldParentID = "EDocumentContentOwnerId";
            radTreeEDocumentContent.DataTextField = "EDocumentContentTitle";
            var eDocumentContentEntity = new EDocumentContentEntity()
            {
                EDocumentContentId = Guid.Parse(Session["EDocumentContentId"].ToString())
            };
            radTreeEDocumentContent.DataSource = _eDocumentContentBL.GetAllByEDocumentContentId(eDocumentContentEntity);
            radTreeEDocumentContent.DataBind();
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            cmbObject.ClearSelection();            
            txtEDocumentContentChildNo.Text = "";
            txtEDocumentContentTitle.Text = "";
            radTreeEDocumentContent.UnselectAllNodes();

        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var eDocumentContentEntity = new EDocumentContentEntity()
            {
                EDocumentContentId = Guid.Parse(ViewState["EDocumentContentId"].ToString())
            };
            var myeDocumentContent = _eDocumentContentBL.GetSingleById(eDocumentContentEntity);
            cmbObject.SelectedValue = myeDocumentContent.ObjectId.ToString();            
            if (cmbObject.FindItemByValue(myeDocumentContent.ObjectId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردآبجکت انتخاب شده معتبر نمی باشد.");
            }
            txtEDocumentContentChildNo.Text = myeDocumentContent.EDocumentContentChildNo.ToString();
            txtEDocumentContentTitle.Text = myeDocumentContent.EDocumentContentTitle;
            SetSelectedNodeForTree((myeDocumentContent.EDocumentContentOwnerId== null) ? (Guid?)null : Guid.Parse(myeDocumentContent.EDocumentContentOwnerId.ToString()));
        }

        private void SetSelectedNodeForTree(Guid? EDocumentContentId)
        {
            SetLoadTreeEDocumentContent();
            if (EDocumentContentId != null)
            {
                foreach (var radTreeNode in radTreeEDocumentContent.GetAllNodes())
                {
                    if (Guid.Parse(radTreeNode.Value) == EDocumentContentId)
                    {
                        radTreeNode.ExpandParentNodes();
                        radTreeNode.Selected = true;

                    }
                }
            }
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        void SetComboBox()
        {
            cmbObject.Items.Clear();
            cmbObject.Items.Insert(0, new RadComboBoxItem(""));
            cmbObject.DataTextField = "ObjectDataBaseName";
            cmbObject.DataValueField = "ObjectId";
            cmbObject.DataSource = _objectBL.GetAllIsActive();
            cmbObject.DataBind();


        }
        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                SetLoadTreeEDocumentContent();
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (radTreeEDocumentContent.SelectedValue == "")
                {
                   throw (new ItcApplicationErrorManagerException("نود را از درخت انتخاب نمایید."));
                }
                Guid EDocumentContentId;
                var eDocumentContentOwnerId =(string.IsNullOrEmpty(radTreeEDocumentContent.SelectedValue) ? (Guid?)null : Guid.Parse(radTreeEDocumentContent.SelectedNode.Value));
                var eDocumentContentEntity = new EDocumentContentEntity()
                {
                    ObjectId = (cmbObject.SelectedIndex>0?Guid.Parse(cmbObject.SelectedValue):(Guid?)null),
                    EDocumentContentOwnerId = eDocumentContentOwnerId,
                    EDocumentContentTitle = txtEDocumentContentTitle.Text,
                    EDocumentContentChildNo = int.Parse(txtEDocumentContentChildNo.Text),                    
                };
                _eDocumentContentBL.Add(eDocumentContentEntity, out EDocumentContentId);
                SetClearToolBox();
                SetSelectedNodeForTree(EDocumentContentId);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (radTreeEDocumentContent.SelectedValue == "")
                {
                    throw (new ItcApplicationErrorManagerException("نود را از درخت انتخاب نمایید."));
                }
                var eDocumentContentOwnerId = (string.IsNullOrEmpty(radTreeEDocumentContent.SelectedValue) ? (Guid?)null : Guid.Parse(radTreeEDocumentContent.SelectedNode.Value));
                var eDocumentContentEntity = new EDocumentContentEntity()
                {
                    EDocumentContentId = Guid.Parse(ViewState["EDocumentContentId"].ToString()),
                    ObjectId = (cmbObject.SelectedIndex > 0 ? Guid.Parse(cmbObject.SelectedValue) : (Guid?)null),
                    EDocumentContentOwnerId = eDocumentContentOwnerId,
                    EDocumentContentTitle = txtEDocumentContentTitle.Text,
                    EDocumentContentChildNo = int.Parse(txtEDocumentContentChildNo.Text),
                };

                _eDocumentContentBL.Update(eDocumentContentEntity);
                SetClearToolBox();
                SetPanelFirst();
                SetSelectedNodeForTree(Guid.Parse(ViewState["EDocumentContentId"].ToString()));
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        protected void btnBack_Click(object sender, EventArgs e)
        {
            SetClearToolBox();
            SetPanelFirst();
        }

        protected void radTreeEDocumentContent_ContextMenuItemClick(object sender, Telerik.Web.UI.RadTreeViewContextMenuEventArgs e)
        {
            try
            {
                RadTreeNode clickedNode = e.Node;
                if (e.MenuItem.Value.ToLower() == "edit")
                {
                    ViewState["EDocumentContentId"] = Guid.Parse(clickedNode.Value);
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.MenuItem.Value.ToLower() == "remove")
                {                    
                    var eDocumentContentEntity = new EDocumentContentEntity()
                    {
                        EDocumentContentId = Guid.Parse(clickedNode.Value)
                    };
                    _eDocumentContentBL.Delete(eDocumentContentEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetLoadTreeEDocumentContent();
                    CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #endregion
    }
}