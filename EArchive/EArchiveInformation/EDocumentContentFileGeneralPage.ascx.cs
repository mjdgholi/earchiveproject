﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.EArchiveInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/04/11>
    /// Description: <آپلود کلی بین تمامی سیستم ها>
    /// </summary>
    public partial class EDocumentContentFileGeneralPage : ItcBaseControl
    {
        #region PublicParam:

        private readonly EDocumentContentBL _eDocumentContentBL = new EDocumentContentBL();
        private readonly EDocumentContentFileBL _eDocumentContentFileBL = new EDocumentContentFileBL();
        private readonly SecurityLevelBL _securityLevelBL = new SecurityLevelBL();
        private readonly FileFormatTypeBL _fileFormatBL = new FileFormatTypeBL();

        private const string TableName = "EArchive.V_EDocumentContentFile";
        private const string PrimaryKey = "EDocumentContentFileId";

        #endregion
        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtFileTitle.Text = "";
            txtKeyWord.Text = "";
            txtPageNo.Text = "";
            cmbSecurityLevel.ClearSelection();                        
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var eDocumentContentFileEntity = new EDocumentContentFileEntity()
            {
                EDocumentContentFileId = Guid.Parse(ViewState["EDocumentContentFileId"].ToString())

            };
            var myEDocumentContentFile = _eDocumentContentFileBL.GetSingleById(eDocumentContentFileEntity);
            txtDescription.Text = myEDocumentContentFile.Description;
            txtFileTitle.Text = myEDocumentContentFile.FileTitle;
            txtKeyWord.Text = myEDocumentContentFile.KeyWord;
            txtPageNo.Text = myEDocumentContentFile.PageNo.ToString();
            cmbSecurityLevel.SelectedValue = myEDocumentContentFile.SecurityLevelId.ToString();
            if (cmbSecurityLevel.FindItemByValue(myEDocumentContentFile.SecurityLevelId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردسطح دسترسی انتخاب شده معتبر نمی باشد.");
            }            

            ViewState["FilePath"] = null;
            ViewState["FileFormatTypeMimeTitle"] = myEDocumentContentFile.FileFormatTypeMimeTitle;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdEDocumentContentFile,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        private void SetComboBox()
        {
            cmbSecurityLevel.Items.Clear();
            cmbSecurityLevel.Items.Insert(0, new RadComboBoxItem(""));
            cmbSecurityLevel.DataTextField = "SecurityLevelTitle";
            cmbSecurityLevel.DataValueField = "SecurityLevelId";
            cmbSecurityLevel.DataSource = _securityLevelBL.GetAllIsActive();
            cmbSecurityLevel.DataBind();
        }

        #endregion
        #region ControlEvent:

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "EDocumentContentExternalId = '" + Guid.Parse(Request["WhereClause"]) + "'";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetComboBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {                
                Guid eDocumentContentFileId;
                byte[] bytes = null;
                if (RadUpload1.UploadedFiles.Count <= 0)
                {
                    throw (new ItcApplicationErrorManagerException("فایل را انتخاب نمایید"));
                }
                var pageNo = (txtPageNo.Text == "" ? (int?)null : int.Parse(txtPageNo.Text));

                foreach (UploadedFile file in RadUpload1.UploadedFiles)
                {
                    bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                    var eDocumentContentFileEntity = new EDocumentContentFileEntity()
                    {                        
                        FilePath = bytes,
                        FileTitle = txtFileTitle.Text,
                        KeyWord = txtKeyWord.Text,
                        Description = txtDescription.Text,
                        PageNo = pageNo,
                        SecurityLevelId = Guid.Parse(cmbSecurityLevel.SelectedValue),
                        FileFormatTypeMimeTitle = file.ContentType,
                        EDocumentContentId = (Guid?)null,
                        EDocumentContentExternalId = Guid.Parse(Request["WhereClause"].ToString())                            
                    };
                    _eDocumentContentFileBL.Add(eDocumentContentFileEntity, out eDocumentContentFileId);
                }
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "EDocumentContentExternalId = '" + Guid.Parse(Request["WhereClause"]) + "'";
                if (txtFileTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FileTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtFileTitle.Text.Trim()) + "%'";
                if (txtKeyWord.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and KeyWord Like N'%" +
                                               FarsiToArabic.ToArabic(txtKeyWord.Text.Trim()) + "%'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                
                if (txtPageNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PageNo ='" +
                                               txtPageNo.Text.Trim() + "'";

                grdEDocumentContentFile.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdEDocumentContentFile.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "EDocumentContentExternalId = '" + Guid.Parse(Request["WhereClause"]) + "'";
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadUpload1.UploadedFiles.Count > 0)
                {
                    var bytes = new byte[RadUpload1.UploadedFiles[0].ContentLength];
                    RadUpload1.UploadedFiles[0].InputStream.Read(bytes, 0, RadUpload1.UploadedFiles[0].ContentLength);
                    ViewState["FilePath"] = bytes;
                    ViewState["FileFormatTypeMimeTitle"] = RadUpload1.UploadedFiles[0].ContentType;
                    ViewState["FlagUpload"] = true;
                }
                var pageNo = (txtPageNo.Text == "" ? (int?)null : int.Parse(txtPageNo.Text));
                EDocumentContentFileEntity eDocumentContentFileEntity = new EDocumentContentFileEntity
                {
                    EDocumentContentFileId = Guid.Parse(ViewState["EDocumentContentFileId"].ToString()),
                    EDocumentContentExternalId = Guid.Parse(Request["WhereClause"]),
                    EDocumentContentId = (Guid?)null,
                    FileTitle = txtFileTitle.Text,
                    KeyWord = txtKeyWord.Text,
                    Description = txtDescription.Text,
                    PageNo = pageNo,
                    SecurityLevelId = Guid.Parse(cmbSecurityLevel.SelectedValue),
                    FileFormatTypeMimeTitle = ViewState["FileFormatTypeMimeTitle"].ToString(),                    
                    FilePath = (ViewState["FilePath"] == null
                        ? (byte[])null
                        : (byte[])ViewState["FilePath"])
                };
                _eDocumentContentFileBL.Update(eDocumentContentFileEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0,
                    new Guid(ViewState["EDocumentContentFileId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));                
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdEDocumentContentFile_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["EDocumentContentFileId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "EDocumentContentExternalId = '" + Guid.Parse(Request["WhereClause"]) + "'";
                    var eDocumentContentFileEntity = new EDocumentContentFileEntity()
                    {
                        EDocumentContentFileId = Guid.Parse(e.CommandArgument.ToString()),

                    };
                    _eDocumentContentFileBL.Delete(eDocumentContentFileEntity);
                    var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdEDocumentContentFile_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdEDocumentContentFile_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdEDocumentContentFile_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion


    }
}