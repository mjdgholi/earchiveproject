﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.EArchiveInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <مستندالکترونیکی>
    /// </summary>
    public partial class EdocumentPage : ItcBaseControl
    {       
        #region PublicParam:
        private readonly ContentTemplateTreeBL _contentTemplateTreeBL = new ContentTemplateTreeBL();
        private readonly EDocumentContentBL _eDocumentContentBL = new EDocumentContentBL();
        private readonly StatusBL _statusBL = new StatusBL();
        private readonly SubjectBL _subjectBL = new SubjectBL();
        private readonly ScopeBL _scopeBL = new ScopeBL();
        private readonly ArchiveBL _archiveBL = new ArchiveBL();
        private readonly  EdocumentBL _edocumentBL=new EdocumentBL();
        private const string TableName = "EArchive.V_Edocument";
        private const string PrimaryKey = "EdocumentId";

        #endregion




        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnEdit.Visible = false;
            btnShowAll.Visible = false;
            cmbContentTemplateTree.Enabled = true;
            PnlOtherInformation.Visible = false;            
            grdEdocument.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnEdit.Visible = true;
            cmbContentTemplateTree.Enabled = false;
            PnlOtherInformation.Visible = false;
            grdEdocument.MasterTableView.GetColumnSafe("ShowAll").Visible = false;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            radTreeArchive.UnselectAllNodes();            
            cmbContentTemplateTree.ClearSelection();                        
            cmbScope.ClearSelection();
            cmbStatus.ClearSelection();
            cmbSubject.ClearSelection();
            txtDescription.Text = "";
            txtEDocumentDate.Text = "";
            txtEdocumentNo.Text = "";
            txtEdocumentTitle.Text = "";                                    
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var edocumentEntity = new EdocumentEntity()
            {
                EdocumentId = Guid.Parse(ViewState["EdocumentId"].ToString())
            };
            var myEdocument = _edocumentBL.GetSingleById(edocumentEntity);

            cmbScope.SelectedValue = myEdocument.ScopeId.ToString();
            if (cmbScope.FindItemByValue(myEdocument.ScopeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردحوزه انتخاب شده معتبر نمی باشد.");
            }
            cmbStatus.SelectedValue = myEdocument.StatusId.ToString();
            if (cmbStatus.FindItemByValue(myEdocument.StatusId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردوضیعت انتخاب شده معتبر نمی باشد.");
            }
            cmbSubject.SelectedValue = myEdocument.SubjectId.ToString();
            if (cmbSubject.FindItemByValue(myEdocument.SubjectId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردموضوع انتخاب شده معتبر نمی باشد.");
            }
            cmbContentTemplateTree.SelectedValue = myEdocument.ContentTemplateTreeId.ToString();
            if (cmbContentTemplateTree.FindItemByValue(myEdocument.ContentTemplateTreeId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردالگوی فهرست بندی انتخاب شده معتبر نمی باشد.");
            }
            txtDescription.Text = myEdocument.Description;
            txtEDocumentDate.Text = myEdocument.EDocumentDate;
            txtEdocumentNo.Text = myEdocument.EdocumentNo;
            txtEdocumentTitle.Text = myEdocument.EdocumentTitle;
            var radtreenode =radTreeArchive.FindNodeByValue(myEdocument.ArchiveId.ToString());
            radtreenode.Selected = true;
            foreach (var raditemnod in radTreeArchive.GetAllNodes())
            {
                if (raditemnod.Parent != null)
                {
                    raditemnod.Expanded = true;
                }
                
            }

        }
        /// <summary>
        /// صفحه مورد نظر که از طریق کلیک بر روی سربرگ انتخاب شده است به مالتی پیج اضافه میگردد
        /// </summary>
        private void AddPageView()
        {
            var pageView = new RadPageView
            {
                ID = ViewState["MenuPageName"].ToString()
            };
            RadMultiPage1.PageViews.Add(pageView);
        }
        //private void SetSelectedNodeForContentTemplateTree(Guid? contentTemplateTreeId)
        //{
        //    SetLoadTreeContentTemplateTree();
        //     var contentTemplateTree = (RadTreeView)cmbContentTemplateTree.Items[0].FindControl("radtreecmbContentTemplateTree");
        //    if (contentTemplateTreeId != null)
        //    {
        //        foreach (var radTreeNode in contentTemplateTree.GetAllNodes())
        //        {
        //            if (Guid.Parse(radTreeNode.Value) == contentTemplateTreeId)
        //            {
        //                radTreeNode.ExpandParentNodes();
        //                radTreeNode.Selected = true;
        //            }
        //        }
        //    }
        //}


        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        void SetComboBox()
        {
            cmbContentTemplateTree.Items.Clear();
            cmbContentTemplateTree.Items.Insert(0, new RadComboBoxItem(""));
            cmbContentTemplateTree.DataTextField = "ContentTemplateTreeTitle";
            cmbContentTemplateTree.DataValueField = "ContentTemplateTreeId";
            IList<ContentTemplateTreeEntity> listContentTemplateTree = _contentTemplateTreeBL.GetAllIsActive();            
            cmbContentTemplateTree.DataSource = listContentTemplateTree.Where(t => t.ContentTemplateTreeOwnerId == null).ToList(); 
            cmbContentTemplateTree.DataBind();

            cmbScope.Items.Clear();
            cmbScope.Items.Insert(0, new RadComboBoxItem(""));
            cmbScope.DataTextField = "ScopeTitle";
            cmbScope.DataValueField = "ScopeId";
            cmbScope.DataSource = _scopeBL.GetAllIsActive();
            cmbScope.DataBind();

            cmbStatus.Items.Clear();
            cmbStatus.Items.Insert(0, new RadComboBoxItem(""));
            cmbStatus.DataTextField = "StatusTitle";
            cmbStatus.DataValueField = "StatusId";
            cmbStatus.DataSource = _statusBL.GetAllIsActive();
            cmbStatus.DataBind();


            cmbSubject.Items.Clear();
            cmbSubject.Items.Insert(0, new RadComboBoxItem(""));
            cmbSubject.DataTextField = "SubjectTitle";
            cmbSubject.DataValueField = "SubjectId";
            cmbSubject.DataSource = _subjectBL.GetAllIsActive();
            cmbSubject.DataBind();
        }


        private void SetRadTreeArchive()
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            const string objectTitlesForRowAccessStr = "EArchive.t_Archive";
            radTreeArchive.DataFieldID = "ArchiveId";
            radTreeArchive.DataValueField = "ArchiveId";
            radTreeArchive.DataFieldParentID = "ArchiveOwnerId";
            radTreeArchive.DataTextField = "ArchiveTitle";
            radTreeArchive.DataSource = _archiveBL.GetAllIsActivePerRowAccess(userId,objectTitlesForRowAccessStr);
            radTreeArchive.DataBind();
        }

        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var userId = Int32.Parse(HttpContext.Current.User.Identity.Name);
            const string objectTitlesForRowAcess = "EArchive.t_Archive";
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdEdocument,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId,
                UserId = userId,
                ObjectTitlesForRowAcess = objectTitlesForRowAcess
            };
            return gridParamEntity;
        }

        void CheckValidation()
        {
            if (radTreeArchive.SelectedValue == "" | radTreeArchive.SelectedValue == null)
                throw (new ItcApplicationErrorManagerException("از درخت بایگانی یک نود را انتخاب نمایید"));
        }
        #endregion



        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {
                ViewState["WhereClause"] = "  ";
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
                SetRadTreeArchive();                
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "  ";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    SetPanelFirst();
                    SetComboBox();
                    SetClearToolBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                Guid edocumentId;
                ViewState["WhereClause"] = "";                
                var edocumentEntity = new EdocumentEntity()
                {
                    ArchiveId = Guid.Parse(radTreeArchive.SelectedValue),
                    ScopeId = Guid.Parse(cmbScope.SelectedValue),
                    SubjectId = Guid.Parse(cmbSubject.SelectedValue),
                    EdocumentTitle = txtEdocumentTitle.Text,
                    EdocumentNo = txtEdocumentNo.Text,
                    EDocumentDate = ItcToDate.ShamsiToMiladi(txtEDocumentDate.Text),
                    StatusId = Guid.Parse(cmbStatus.SelectedValue),
                    Description = txtDescription.Text,
                    ContentTemplateTreeId = Guid.Parse(cmbContentTemplateTree.SelectedValue)                                     
                };

                _edocumentBL.Add(edocumentEntity, out edocumentId);
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, edocumentId);
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                
                ViewState["WhereClause"] = "1 = 1";
                if (radTreeArchive.SelectedValue !="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ArchiveId ='" +
                                               radTreeArchive.SelectedValue + "'";
                if (cmbScope.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ScopeId ='" +
                                               cmbScope.SelectedValue + "'";
                if (cmbStatus.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and StatusId ='" +
                                               cmbStatus.SelectedValue + "'";
                if (cmbContentTemplateTree.Text!="")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and ContentTemplateTreeId ='" +
                                               cmbContentTemplateTree.SelectedValue + "'";

                if (cmbSubject.SelectedIndex > 0)
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and SubjectId ='" +
                                               cmbSubject.SelectedValue + "'";
                if (txtEdocumentTitle.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EdocumentTitle Like N'%" +
                           FarsiToArabic.ToArabic(txtEdocumentTitle.Text.Trim()) + "%'";
                if (txtDescription.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                           FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";
                if (txtEdocumentNo.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EdocumentNo Like N'%" +
                           FarsiToArabic.ToArabic(txtEdocumentNo.Text.Trim()) + "%'";
                if (txtEDocumentDate.Text != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and EDocumentDate ='" +
                                              txtEDocumentDate.Text+ "'";
                
                grdEdocument.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                if (grdEdocument.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "";
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                CheckValidation();
                var edocumentEntity = new EdocumentEntity()
                {
                    EdocumentId = Guid.Parse(ViewState["EdocumentId"].ToString()),
                    ArchiveId = Guid.Parse(radTreeArchive.SelectedValue),
                    ScopeId = Guid.Parse(cmbScope.SelectedValue),
                    SubjectId = Guid.Parse(cmbSubject.SelectedValue),
                    EdocumentTitle = txtEdocumentTitle.Text,
                    EdocumentNo = txtEdocumentNo.Text,
                    EDocumentDate = ItcToDate.ShamsiToMiladi(txtEDocumentDate.Text),
                    StatusId = Guid.Parse(cmbStatus.SelectedValue),
                    Description = txtDescription.Text,
                  
                };

                _edocumentBL.Update(edocumentEntity);
                SetPanelFirst();
                SetClearToolBox();
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid(ViewState["EdocumentId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>
        protected void grdEdocument_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["EdocumentId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "";
                    var edocumentEntity = new EdocumentEntity()
                    {
                        EdocumentId = Guid.Parse(e.CommandArgument.ToString()),
                    };
                    _edocumentBL.Delete(edocumentEntity);
                    var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
                if (e.CommandName == "ContentTree")
                {
                    PnlOtherInformation.Visible = true;
                    var edocumentId = Guid.Parse(e.CommandArgument.ToString());
                    var edocumentEntity = new EdocumentEntity()
                    {
                        EdocumentId = edocumentId
                    };
                    var myEdocument = _edocumentBL.GetSingleById(edocumentEntity);
                    Session["EDocumentContentId"] = myEdocument.EDocumentContentId;
                    Session["EdocumentNo"] = myEdocument.EdocumentNo;
                    ViewState["WhereClause"] = "EdocumentId='" + edocumentId+"'";
                    var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, edocumentId);
                    DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                    e.Item.Selected = true;                    
                    grdEdocument.MasterTableView.GetColumnSafe("ShowAll").Visible = true;
                    ViewState["MenuPageName"] = "EDocumentContentPage";
                    RadTabStripTabClick(new object(), new RadTabStripEventArgs(RadTabStrip.FindTabByValue("EDocumentContentPage")));
                }
                if (e.CommandName == "_ShowAll")
                {
                    try
                    {
                        ViewState["WhereClause"] = "";
                        var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                        DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
                        SetPanelFirst();
                    }
                    catch (Exception ex)
                    {
                        CustomMessageErrorControl.ShowErrorMessage(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>


        protected void grdEdocument_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdEdocument_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdEdocument_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        #endregion

        protected void radTreeArchive_NodeClick(object sender, RadTreeNodeEventArgs e)
        {
            ViewState["WhereClause"] = "ArchiveId= '" + e.Node.Value+"'";
            var gridParamEntity = SetGridParam(grdEdocument.MasterTableView.PageSize, 0, new Guid());
            DatabaseManager.ItcGetPageForTableGuidKeyWithRowAccess(gridParamEntity);
        }


            
        /// <summary>
        /// پس از انتخاب سربرگ پیج متناظر با سربرگ به مالتی پیج اضافه میگردد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">نمایانگر تب انتخابی میباشد-</param>
        protected void RadTabStripTabClick(object sender, RadTabStripEventArgs e)
        {
            ViewState["MenuPageName"] = e.Tab.Value;
            RadMultiPage1.PageViews.Clear();
            switch (e.Tab.Value)
            {
                case "EDocumentContentPage":
                    {
                        ViewState["control"] = @"DesktopModules\EArchiveProject\EArchive\BaseInformation\EDocumentContentPage.ascx";
                        AddPageView();
                        e.Tab.Selected = true;
                        break;
                    }
                case "EDocumentContentFilePage":
                    {
                        ViewState["control"] = @"DesktopModules\EArchiveProject\EArchive\EArchiveInformation\EDocumentContentFilePage.ascx";
                        AddPageView();
                        e.Tab.Selected = true;
                        break;
                    }
                
                    
                default:
                    return;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void RadMultiPage1PageViewCreated1(object sender, RadMultiPageEventArgs e)
        {
            if (ViewState["control"] != null)
            {
                string userControlName = ViewState["control"].ToString();
                var userControl = (Page.LoadControl(userControlName));
                if (userControl != null)
                {
                    userControl.ID = ViewState["MenuPageName"] + "_userControl";
                }
                if (userControl != null) e.PageView.Controls.Add(userControl);
                var tabedControl = (RadMultiPage1.PageViews[0]).Controls[0] as ITC.Library.Classes.ITabedControl;
                if (tabedControl != null) tabedControl.InitControl();
                e.PageView.Selected = true;
            }
        }
    }
}