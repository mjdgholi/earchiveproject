﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EDocumentContentFileGeneralPage.ascx.cs" Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.EArchiveInformation.EDocumentContentFileGeneralPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>
<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl">
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" OnClick="btnSave_Click"></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" CausesValidation="False"
                                    OnClick="btnSearch_Click"><Icon PrimaryIconCssClass="rbSearch" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" CausesValidation="False"
                                    OnClick="btnShowAll_Click"><Icon PrimaryIconCssClass="rbRefresh" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" OnClick="btnEdit_Click"><Icon PrimaryIconCssClass="rbEdit" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" OnClick="btnBack_Click"><Icon PrimaryIconCssClass="rbPrevious" /></cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top">
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">                 
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblFileTitle">عنوان فایل<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtFileTitle" runat="server" Width="170px">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="ValidObjectFieldName" runat="server" ControlToValidate="txtFileTitle"
                                ValidationGroup="ValidEDocumentContentFile"    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="blSecurityLevel">عنوان سطح امنیت<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbSecurityLevel" runat="server" 
                                    AppendDataBoundItems="True" Width="170px">
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvSecurityLevel" runat="server" ControlToValidate="cmbSecurityLevel"
                                ValidationGroup="ValidEDocumentContentFile"    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblFilePath">مسیر فایل<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadUpload ID="RadUpload1" runat="server" ControlObjectsVisibility="None">
                                </telerik:RadUpload>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblPageNo">تعداد صفحه:</asp:Label>
                            </td>
                            <td>
                                <cc1:NumericTextBox ID="txtPageNo" runat="server" Width="170px"></cc1:NumericTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblKeyWord">کلمات کلیدی:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtKeyWord" runat="server" Width="170px">
                                </telerik:RadTextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblDescription">توضیح:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtDescription" runat="server" Width="170px" 
                                    TextMode="MultiLine" >
                                </telerik:RadTextBox>
                            </td>
                        </tr>

                        </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                                    <telerik:RadGrid ID="grdEDocumentContentFile" runat="server" AllowCustomPaging="True"
                                                    AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                                    AutoGenerateColumns="False" 
                                                        OnItemCommand="grdEDocumentContentFile_ItemCommand" 
                                                        onpageindexchanged="grdEDocumentContentFile_PageIndexChanged" 
                                                        onpagesizechanged="grdEDocumentContentFile_PageSizeChanged" 
                                                        onsortcommand="grdEDocumentContentFile_SortCommand" >
                                                    <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Office2007">
                                                    </HeaderContextMenu>
                                                    <MasterTableView DataKeyNames="EDocumentContentFileId" Dir="RTL" GroupsDefaultExpanded="False"
                                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                                        <Columns>
                                                            
                                                            <telerik:GridBoundColumn DataField="FileTitle" HeaderText="عنوان فایل" SortExpression="FileTitle">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="SecurityLevelTitle" HeaderText="عنوان سطح امنیت"
                                                                SortExpression="SecurityLevelTitle">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                            </telerik:GridBoundColumn>
                                                               <telerik:GridBoundColumn DataField="Filesize" HeaderText="سایز فایل(KB)"
                                                                SortExpression="Filesize">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridBoundColumn DataField="PageNo" HeaderText="تعداد صفحه فایل" SortExpression="PageNo">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" />
                                                            </telerik:GridBoundColumn>
                                   <telerik:GridBoundColumn DataField="KeyWord" HeaderText="کلمات کلیدی" SortExpression="KeyWord">
                                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle"  />
                                                            </telerik:GridBoundColumn>
                                                            <telerik:GridTemplateColumn HeaderText="دریافت فایل" FilterControlAltText="Filter TemplateColumn1 column"
                                                                UniqueName="TemplateColumn1">
                                                                <ItemTemplate>
                                                                    <img onclick="if (document.URL.search('localhost')=='-1') { window.open('DownloadBlobFile.aspx?EDocumentContentFileId='+'<%#Eval("EDocumentContentFileId").ToString() %>'); }
                                                                 else  { window.open('DownloadBlobFile.aspx?EDocumentContentFileId='+'<%#Eval("EDocumentContentFileId").ToString() %>'); }"
                                                                        src="../Images/download.png" width="25"
                                                                        height="25" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                                UniqueName="TemplateColumn1">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("EDocumentContentFileId").ToString() %>'
                                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </telerik:GridTemplateColumn>
                                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                                UniqueName="TemplateColumn">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                                        CommandArgument='<%#Eval("EDocumentContentFileId").ToString() %>' CommandName="_MyِDelete"
                                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </telerik:GridTemplateColumn>
                                                        </Columns>
                                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                                        </RowIndicatorColumn>
                                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                                        </ExpandCollapseColumn>
                                                        <EditFormSettings>
                                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                            </EditColumn>
                                                        </EditFormSettings>
                                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                                            VerticalAlign="Middle" />
                                                    </MasterTableView>
                                                    <FilterMenu EnableImageSprites="False">
                                                    </FilterMenu>
                                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>


