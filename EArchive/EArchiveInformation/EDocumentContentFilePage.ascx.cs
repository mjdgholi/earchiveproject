﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;
using ITC.Library.Classes.ItcException;
using Telerik.Web.UI;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.EArchiveInformation
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/27>
    /// Description: <محتوای فایل>
    /// </summary>
    public partial class EDocumentContentFilePage : ItcBaseControl
    {
        #region PublicParam:

        private readonly EDocumentContentBL _eDocumentContentBL = new EDocumentContentBL();
        private readonly EDocumentContentFileBL _eDocumentContentFileBL = new EDocumentContentFileBL();
        private readonly SecurityLevelBL _securityLevelBL = new SecurityLevelBL();
        private const string TableName = "EArchive.V_EDocumentContentFile";
        private const string PrimaryKey = "EDocumentContentFileId";

        #endregion

        /// <summary>
        /// پیاده سازی PageLoad
        /// </summary>
        public override void InitControl()
        {
            try
            {                
                ViewState["SortExpression"] = " ";
                ViewState["SortType"] = "Asc";     
                SetPanelFirst();
                SetClearToolBox();
                SetComboBox();
                SetRadTreeContent();
                pnlDetail.Visible = false;
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        private void SetRadTreeContent()
        {
            radTreeEDocumentContent.DataFieldID = "EDocumentContentId";
            radTreeEDocumentContent.DataValueField = "EDocumentContentId";
            radTreeEDocumentContent.DataFieldParentID = "EDocumentContentOwnerId";
            radTreeEDocumentContent.DataTextField = "EDocumentContentTitle";
            var eDocumentContentEntity = new EDocumentContentEntity()
            {
                EDocumentContentId = Guid.Parse(Session["EDocumentContentId"].ToString())
            };
            radTreeEDocumentContent.DataSource = _eDocumentContentBL.GetAllByEDocumentContentId(eDocumentContentEntity);
            radTreeEDocumentContent.DataBind();
        }

        #region Procedure:

        /// <summary>
        /// ست کردن وضعیت کنترل ها به حالت اولیه
        /// </summary>
        private void SetPanelFirst()
        {
            btnOperationWithoutAjaxSave.Visible = true;
            btnSearch.Visible = true;
            btnBack.Visible = false;
            btnOperationWithoutAjaxEdit.Visible = false;
            btnShowAll.Visible = false;
        }

        /// <summary>
        /// ست کردن وضعیت کنترل ها برای ویرایش
        /// </summary>
        private void SetPanelLast()
        {
            btnOperationWithoutAjaxSave.Visible = false;
            btnSearch.Visible = false;
            btnBack.Visible = true;
            btnOperationWithoutAjaxEdit.Visible = true;
        }

        /// <summary>
        /// خالی کردن کنترل ها
        /// </summary>
        private void SetClearToolBox()
        {
            txtDescription.Text = "";
            txtFileTitle.Text = "";
            txtKeyWord.Text = "";
            txtPageNo.Text = "";
            cmbSecurityLevel.ClearSelection();            
            SelectEDocumentContentExternal.KeyId = "";
            SelectEDocumentContentExternal.Title = "";
        }

        /// <summary>
        /// ست کردن مقادیر کنترل ها بر اساس رکورد انتخابی
        /// </summary>
        private void SetDataShow()
        {
            var eDocumentContentFileEntity = new EDocumentContentFileEntity()
            {
                EDocumentContentFileId = Guid.Parse(ViewState["EDocumentContentFileId"].ToString())

            };
            var myEDocumentContentFile = _eDocumentContentFileBL.GetSingleById(eDocumentContentFileEntity);
            txtDescription.Text = myEDocumentContentFile.Description;
            txtFileTitle.Text = myEDocumentContentFile.FileTitle;
            txtKeyWord.Text = myEDocumentContentFile.KeyWord;
            txtPageNo.Text = myEDocumentContentFile.PageNo.ToString();
            cmbSecurityLevel.SelectedValue = myEDocumentContentFile.SecurityLevelId.ToString();
            if (cmbSecurityLevel.FindItemByValue(myEDocumentContentFile.SecurityLevelId.ToString()) == null)
            {
                CustomMessageErrorControl.ShowWarningMessage("وضیعت رکوردسطح دسترسی انتخاب شده معتبر نمی باشد.");
            }
            if (myEDocumentContentFile.EDocumentContentExternalId != null)
            {
                SelectEDocumentContentExternal.KeyId = myEDocumentContentFile.EDocumentContentExternalId.ToString();
                SelectEDocumentContentExternal.Title =
                    _eDocumentContentFileBL.GetValueTileForShowinSelectControl(
                        Guid.Parse(ViewState["EDocumentContentFileId"].ToString()), Session["EDocumentNo"].ToString())
                        .Tables[0].Rows[0][0].ToString();      
            }
                      
            ViewState["FilePath"] = null;            
            ViewState["FileFormatTypeMimeTitle"] = myEDocumentContentFile.FileFormatTypeMimeTitle;
        }

        /// <summary>
        ///  تعیین نوع مرتب سازی - صعودی یا نزولی
        /// </summary>
        private void SetSortType()
        {
            if (ViewState["SortType"].ToString() == "ASC")
                ViewState["SortType"] = "DESC";
            else
                ViewState["SortType"] = "ASC";

        }


        private GridParamEntity SetGridParam(int pageSize, int currentpPage, Guid rowSelectGuidId)
        {
            var gridParamEntity = new GridParamEntity
            {
                TableName = TableName,
                PrimaryKey = PrimaryKey,
                RadGrid = grdEDocumentContentFile,
                PageSize = pageSize,
                CurrentPage = currentpPage,
                WhereClause = ViewState["WhereClause"].ToString(),
                OrderBy = ViewState["SortExpression"].ToString(),
                SortType = ViewState["SortType"].ToString(),
                RowSelectGuidId = rowSelectGuidId
            };
            return gridParamEntity;
        }

        private void SetComboBox()
        {
            cmbSecurityLevel.Items.Clear();
            cmbSecurityLevel.Items.Insert(0, new RadComboBoxItem(""));
            cmbSecurityLevel.DataTextField = "SecurityLevelTitle";
            cmbSecurityLevel.DataValueField = "SecurityLevelId";
            cmbSecurityLevel.DataSource = _securityLevelBL.GetAllIsActive();
            cmbSecurityLevel.DataBind();
        }


        #endregion

        #region ControlEvent:

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    ViewState["WhereClause"] = "EDocumentContentId = '" +
                                               Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";
                    ViewState["SortExpression"] = " ";
                    ViewState["SortType"] = "Asc";
                    var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                    SetPanelFirst();
                    SetClearToolBox();
                    SetComboBox();
                }
                catch (Exception ex)
                {

                    CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
                }
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات وارد شده در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                ViewState["WhereClause"] = "EDocumentContentId = '" +
                                           Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";
                byte[] bytes = null;
                if (RadUpload1.UploadedFiles.Count <= 0)
                {
                    throw (new ItcApplicationErrorManagerException("فایل را انتخاب نمایید"));
                }
                var pageNo = (txtPageNo.Text == "" ? (int?) null : int.Parse(txtPageNo.Text));

                foreach (UploadedFile file in RadUpload1.UploadedFiles)
                {
                    bytes = new byte[file.ContentLength];
                    file.InputStream.Read(bytes, 0, file.ContentLength);
                    var eDocumentContentFileEntity = new EDocumentContentFileEntity()
                    {
                        EDocumentContentId = Guid.Parse(radTreeEDocumentContent.SelectedNode.Value),
                        FilePath = bytes,
                        FileTitle = txtFileTitle.Text,
                        KeyWord = txtKeyWord.Text,
                        Description = txtDescription.Text,
                        PageNo = pageNo,
                        SecurityLevelId = Guid.Parse(cmbSecurityLevel.SelectedValue),
                        FileFormatTypeMimeTitle = file.ContentType,
                        EDocumentContentExternalId =
                            (SelectEDocumentContentExternal.KeyId == ""
                                ? (Guid?) null
                                : Guid.Parse(SelectEDocumentContentExternal.KeyId)),                     
                    };
                    Guid eDocumentContentFileId;
                    _eDocumentContentFileBL.Add(eDocumentContentFileEntity, out eDocumentContentFileId);
                }
                SetClearToolBox();
                SetPanelFirst();
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));

            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }
        /// <summary>
        /// جستجو بر اساس آیتمهای انتخابی
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["WhereClause"] = "EDocumentContentId = '" +
                                       Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";
                if (txtFileTitle.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and FileTitle Like N'%" +
                                               FarsiToArabic.ToArabic(txtFileTitle.Text.Trim()) + "%'";
                if (txtKeyWord.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and KeyWord Like N'%" +
                                               FarsiToArabic.ToArabic(txtKeyWord.Text.Trim()) + "%'";
                if (txtDescription.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and Description Like N'%" +
                                               FarsiToArabic.ToArabic(txtDescription.Text.Trim()) + "%'";

                if (txtPageNo.Text.Trim() != "")
                    ViewState["WhereClause"] = ViewState["WhereClause"] + " and PageNo ='" +
                                               txtPageNo.Text.Trim() + "'";

                grdEDocumentContentFile.MasterTableView.CurrentPageIndex = 0;
                SetPanelFirst();
                btnShowAll.Visible = true;
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                if (grdEDocumentContentFile.VirtualItemCount == 0)
                {
                    CustomMessageErrorControl.ShowWarningMessage(ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.NotFindSearch)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// نمایش تمامی رکوردها در گرید
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnShowAll_Click(object sender, EventArgs e)
        {
            try
            {
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "EDocumentContentId = '" +
                                       Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// متد ثبت اطلاعات اصلاحی کاربر در پایگاه داده
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                if (RadUpload1.UploadedFiles.Count > 0)
                {
                    var bytes = new byte[RadUpload1.UploadedFiles[0].ContentLength];
                    RadUpload1.UploadedFiles[0].InputStream.Read(bytes, 0, RadUpload1.UploadedFiles[0].ContentLength);
                    ViewState["FilePath"] = bytes;
                    ViewState["FileFormatTypeMimeTitle"] = RadUpload1.UploadedFiles[0].ContentType;
                    ViewState["FlagUpload"] = true;
                }
                var pageNo = (txtPageNo.Text == "" ? (int?) null : int.Parse(txtPageNo.Text));
                EDocumentContentFileEntity eDocumentContentFileEntity = new EDocumentContentFileEntity
                {
                    EDocumentContentFileId = Guid.Parse(ViewState["EDocumentContentFileId"].ToString()),
                    EDocumentContentId = Guid.Parse(radTreeEDocumentContent.SelectedNode.Value),
                    FileTitle = txtFileTitle.Text,
                    KeyWord = txtKeyWord.Text,
                    Description = txtDescription.Text,
                    PageNo = pageNo,
                    SecurityLevelId = Guid.Parse(cmbSecurityLevel.SelectedValue),
                    FileFormatTypeMimeTitle = ViewState["FileFormatTypeMimeTitle"].ToString(),
                    EDocumentContentExternalId = (SelectEDocumentContentExternal.KeyId == ""
                        ? (Guid?) null
                        : Guid.Parse(SelectEDocumentContentExternal.KeyId)),
                    FilePath = (ViewState["FilePath"] == null
                        ? (byte[]) null
                        : (byte[]) ViewState["FilePath"])
                };
                _eDocumentContentFileBL.Update(eDocumentContentFileEntity);
                SetPanelFirst();
                SetClearToolBox();
                ViewState["WhereClause"] = "EDocumentContentId = '" +
                                           Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0,
                    new Guid(ViewState["EDocumentContentFileId"].ToString()));
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
                CustomMessageErrorControl.ShowSuccesMessage(
                    ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                btnOperationWithoutAjaxSave.Focus();
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }

        /// <summary>
        /// برگرداندن صفحه به وضعیت اولیه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                SetClearToolBox();
                SetPanelFirst();
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// اعمال عملیات مختلف بر روی سطر انتخابی از گرید - ویرایش حذف و غیره
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">رکورد انتخابی از گرید</param>

        protected void grdEDocumentContentFile_ItemCommand(object sender, GridCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "_MyِEdit")
                {
                    ViewState["EDocumentContentFileId"] = e.CommandArgument;
                    e.Item.Selected = true;
                    SetDataShow();
                    SetPanelLast();
                }
                if (e.CommandName == "_MyِDelete")
                {
                    ViewState["WhereClause"] = "EDocumentContentId = '" +
                                               Guid.Parse(radTreeEDocumentContent.SelectedNode.Value) + "'";

                    var eDocumentContentFileEntity = new EDocumentContentFileEntity()
                    {
                        EDocumentContentFileId = Guid.Parse(e.CommandArgument.ToString()),

                    };
                    _eDocumentContentFileBL.Delete(eDocumentContentFileEntity);
                    var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                    DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

                    SetPanelFirst();
                    SetClearToolBox();
                    CustomMessageErrorControl.ShowSuccesMessage(
                        ItcMessageError.GetMessage(Convert.ToInt16(ItcPublicMessages.PublicMessages.SucessAdd)));
                }
            }
            catch (Exception ex)
            {
                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));

            }
        }




        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس شماره صفحه انتخابی کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">شماره صفحه انتخابی کاربر</param>
        protected void grdEDocumentContentFile_PageIndexChanged(object sender, GridPageChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, e.NewPageIndex, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }
        /// <summary>
        /// بارگزاری اطلاعات در گرید بر اساس تعداد رکوردهایی که باید در یک صفحه نمایش داده شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">تعداد رکوردهای هر صفحه</param>

        protected void grdEDocumentContentFile_PageSizeChanged(object sender, GridPageSizeChangedEventArgs e)
        {
            try
            {
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }
        }


        /// <summary>
        /// مرتب سازی اسلاعات در گرید بر اساس ستون انتخاب شده توسط کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">فیلد انتخاب شده جهت مرتب سازی</param>

        protected void grdEDocumentContentFile_SortCommand(object sender, GridSortCommandEventArgs e)
        {
            try
            {
                if (ViewState["SortExpression"].ToString() == e.SortExpression)
                {
                    ViewState["SortExpression"] = e.SortExpression;
                    SetSortType();
                }
                else
                {
                    ViewState["SortType"] = "ASC";
                    ViewState["SortExpression"] = e.SortExpression;
                }
                var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
                DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);

            }
            catch (Exception ex)
            {

                CustomMessageErrorControl.ShowErrorMessage(ItcMessageError.GetMessage(ex));
            }

        }

        /// <summary>
        /// کلیک کردن روی نود درخت
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void radTreeEDocumentContent_NodeClick(object sender, RadTreeNodeEventArgs e)
        {

            var edocumentEntity = new EdocumentEntity()
            {
                EDocumentContentId = Guid.Parse(e.Node.Value),
                EdocumentNo = Session["EdocumentNo"].ToString()
            };

            _eDocumentContentFileBL.InitializedEDocumentContentIdinEDocumentContentFile(edocumentEntity);
            ViewState["WhereClause"] = "EDocumentContentId = '" + Guid.Parse(e.Node.Value) + "'";
            var gridParamEntity = SetGridParam(grdEDocumentContentFile.MasterTableView.PageSize, 0, new Guid());
            SelectEDocumentContentExternal.WhereClause = Session["EdocumentNo"] + "," + e.Node.Value;
            pnlDetail.Visible = true;
            SetClearToolBox();
            DatabaseManager.ItcGetPageForTableGuidKey(gridParamEntity);
        }

        #endregion

    }
}
