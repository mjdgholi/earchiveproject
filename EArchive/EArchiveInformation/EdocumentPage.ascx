﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EdocumentPage.ascx.cs"
    Inherits="Intranet.DesktopModules.EArchiveProject.EArchive.EArchiveInformation.EdocumentPage" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="cc1" Namespace="ITC.Library.Controls" Assembly="ITC.Library" %>


<asp:Panel ID="Panel1" runat="server">
    <table style="width: 100%; background-color: #F0F8FF;" dir="rtl"  >
        <tr>
            <td dir="rtl" style="width: 100%;" valign="top">
                <asp:Panel ID="pnlButton" runat="server">
                    <table>
                        <tr>
                            <td>
                                <cc1:CustomRadButton ID="btnSave" runat="server" CustomeButtonType="Add" 
                                    onclick="btnSave_Click" >
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnSearch" runat="server" CustomeButtonType="Search" 
                                    CausesValidation="False" onclick="btnSearch_Click"
                                    >
                                    <Icon PrimaryIconCssClass="rbSearch" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnShowAll" runat="server" CustomeButtonType="ShowAll" 
                                    CausesValidation="False" onclick="btnShowAll_Click"
                                    >
                                    <Icon PrimaryIconCssClass="rbRefresh" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnEdit" runat="server" CustomeButtonType="Edit" 
                                    onclick="btnEdit_Click" >
                                    <Icon PrimaryIconCssClass="rbEdit" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomRadButton ID="btnBack" runat="server" CustomeButtonType="Back" 
                                    onclick="btnBack_Click" >
                                    <Icon PrimaryIconCssClass="rbPrevious" />
                                </cc1:CustomRadButton>
                            </td>
                            <td>
                                <cc1:CustomMessageErrorControl ID="CustomMessageErrorControl" runat="server" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" >
                <asp:Panel ID="pnlDetail" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td align="right" valign="top" width="30%">
                                                      <telerik:RadTreeView ID="radTreeArchive" 
                                     runat="server" BorderColor="#999999" BorderStyle="Solid" 
                                    BorderWidth="1px" dir="rtl" CausesValidation="False" Height="350"
                                                                       
                           Width="100%" onnodeclick="radTreeArchive_NodeClick" 
                               >
                                    
                                </telerik:RadTreeView>
                            </td>
                            <td width="70%" align="right" valign="top">
                    <table>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblEdocumentNo">شماره مستند<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtEdocumentNo" runat="server">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="rfvEdocumentNo" runat="server" ControlToValidate="txtEdocumentNo"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblEdocumentTitle">عنوان مستند<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtEdocumentTitle" runat="server">
                                </telerik:RadTextBox>
                                <asp:RequiredFieldValidator ID="ValidEdocumentTitle" runat="server" ControlToValidate="txtEdocumentTitle"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblEDocumentDate">تاریخ تشکیل مستند<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomItcCalendar ID="txtEDocumentDate" runat="server"></cc1:CustomItcCalendar>
                                <asp:RequiredFieldValidator ID="rfvEDocumentDate" runat="server" ControlToValidate="txtEDocumentDate"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                                          <td>
                                <asp:Label runat="server" ID="lblContentTemplateTree">الگوی فهرست بندی<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbContentTemplateTree" runat="server" MarkFirstMatch="True" AppendDataBoundItems="True">              
                                </cc1:CustomRadComboBox>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblScope">حوزه<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbScope" runat="server" AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvScope" runat="server" ControlToValidate="cmbScope"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblSubject">موضوع<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbSubject" runat="server" AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ControlToValidate="cmbSubject"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblStatus">عنوان وضعیت مستند<font color="red">*</font>:</asp:Label>
                            </td>
                            <td>
                                <cc1:CustomRadComboBox ID="cmbStatus" runat="server" AppendDataBoundItems="True">
                                </cc1:CustomRadComboBox>
                                <asp:RequiredFieldValidator ID="rfvStatus" runat="server" ControlToValidate="cmbStatus"
                                    ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label runat="server" ID="lblDescription">توضیحات:</asp:Label>
                            </td>
                            <td>
                                <telerik:RadTextBox ID="txtDescription" runat="server" TextMode="MultiLine">
                                </telerik:RadTextBox>
                  
                            </td>
                        </tr>

                    </table>            
                            </td>
                        </tr>
                    </table>
                    
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td valign="top" bgcolor="White" width="80%">
                <asp:Panel ID="pnlGrid" runat="server">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="grdEdocument" runat="server" AllowCustomPaging="True" AllowPaging="True"
                                    AllowSorting="True" CellSpacing="0" GridLines="None" HorizontalAlign="Center"
                                    AutoGenerateColumns="False" onitemcommand="grdEdocument_ItemCommand" 
                                    onpageindexchanged="grdEdocument_PageIndexChanged" 
                                    onpagesizechanged="grdEdocument_PageSizeChanged" 
                                    onsortcommand="grdEdocument_SortCommand">
                                   
                                    <MasterTableView DataKeyNames="EdocumentId" Dir="RTL" GroupsDefaultExpanded="False"
                                        NoMasterRecordsText="اطلاعاتی برای نمایش یافت نشد">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="EdocumentNo" HeaderText="شماره مستند" SortExpression="EdocumentNo">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EdocumentTitle" HeaderText="عنوان مستند" SortExpression="EdocumentTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EDocumentDate" HeaderText="تاریخ تشکیل مستند"
                                                SortExpression="EDocumentDate">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ContentTemplateTreeTitle" HeaderText="عنوان الگوی فهرست بندی"
                                                SortExpression="ContentTemplateTreeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EDocumentContentTitle" HeaderText="عنوان محتوی"
                                                SortExpression="EDocumentContentTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ArchiveTitle" HeaderText="عنوان بایگانی" SortExpression="ArchiveTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ScopeTitle" HeaderText="عنوان حوزه" SortExpression="ScopeTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="SubjectTitle" HeaderText="عنوان موضوع" SortExpression="SubjectTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="StatusTitle" HeaderText="عنوان وضیعت" SortExpression="StatusTitle">
                                                <HeaderStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                                <ItemStyle HorizontalAlign="Right" VerticalAlign="Middle" Width="400px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridCheckBoxColumn DataField="IsActive" HeaderText="وضیعت رکورد" SortExpression="IsActive">
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridCheckBoxColumn>
                                               <telerik:GridTemplateColumn HeaderText="اطلاعات تکمیلی" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButtonContentTree" runat="server" CausesValidation="false" CommandArgument='<%#Eval("EdocumentId").ToString() %>'
                                                        CommandName="ContentTree" ForeColor="#000066" ImageUrl="../Images/AddOtherInfo.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="ویرایش" FilterControlAltText="Filter TemplateColumn1 column"
                                                UniqueName="TemplateColumn1">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton" runat="server" CausesValidation="false" CommandArgument='<%#Eval("EdocumentId").ToString() %>'
                                                        CommandName="_MyِEdit" ForeColor="#000066" ImageUrl="../Images/Edit.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="حذف" FilterControlAltText="Filter TemplateColumn column"
                                                UniqueName="TemplateColumn">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="ImgButton1" runat="server" CausesValidation="false" OnClientClick="return confirm('آیا اطلاعات رکورد حذف شود؟');"
                                                        CommandArgument='<%#Eval("EdocumentId").ToString() %>' CommandName="_MyِDelete"
                                                        ForeColor="#000066" ImageUrl="../Images/Delete.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="نمایش همه"
                                                UniqueName="ShowAll">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="imgShoaAll" runat="server" CausesValidation="false" CommandArgument='<%#Eval("EdocumentId").ToString() %>'
                                                        CommandName="_ShowAll" ForeColor="#000066" ImageUrl="../Images/ShowAllInGrid.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column">
                                        </ExpandCollapseColumn>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                        <PagerStyle FirstPageToolTip="صفحه اول" HorizontalAlign="Center" LastPageToolTip="صفحه آخر"
                                            NextPagesToolTip="صفحات بعدی" NextPageToolTip="صفحه بعدی" PagerTextFormat="تغییر صفحه: {4} &amp;nbsp;صفحه &lt;strong&gt;{0}&lt;/strong&gt; از &lt;strong&gt;{1}&lt;/strong&gt;, آیتم &lt;strong&gt;{2}&lt;/strong&gt; به &lt;strong&gt;{3}&lt;/strong&gt; از &lt;strong&gt;{5}&lt;/strong&gt;."
                                            PageSizeLabelText="اندازه صفحه" PrevPagesToolTip="صفحات قبلی" PrevPageToolTip="صفحه قبلی"
                                            VerticalAlign="Middle" />
                                    </MasterTableView>
                                    <FilterMenu EnableImageSprites="False">
                                    </FilterMenu>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="PnlOtherInformation" runat="server" HorizontalAlign="Right" Width="100%">
    <table width="100%" dir="rtl">
        <tr>
            <td align="right" valign="top">
                <telerik:RadTabStrip ID="RadTabStrip" runat="server" SelectedIndex="0" Align="Right"
                    AutoPostBack="True" CausesValidation="False" OnTabClick="RadTabStripTabClick"
                    >
                    <Tabs>
                        <telerik:RadTab runat="server" Text=" درخت محتوی مستند" Value="EDocumentContentPage">
                        </telerik:RadTab>
                        <telerik:RadTab runat="server" Text="فایل مستند" Value="EDocumentContentFilePage">
                        </telerik:RadTab>
                    </Tabs>
                </telerik:RadTabStrip>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadMultiPage ID="RadMultiPage1" runat="server" Height="100%" OnPageViewCreated="RadMultiPage1PageViewCreated1"
                    Width="100%">
                </telerik:RadMultiPage>
            </td>
        </tr>
    </table>
</asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<telerik:RadAjaxManagerProxy ID="RadAjaxManagerProxy1" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="btnSave">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnSearch">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnShowAll">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEdit">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnBack">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="radTreeArchive">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="grdEdocument">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="pnlButton" />
                <telerik:AjaxUpdatedControl ControlID="pnlDetail" />
                <telerik:AjaxUpdatedControl ControlID="pnlGrid" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
                <telerik:AjaxUpdatedControl ControlID="PnlOtherInformation" />
            </UpdatedControls>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="RadTabStrip">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="RadTabStrip" />
                <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" 
                    LoadingPanelID="RadAjaxLoadingPanel1" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManagerProxy>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server">
</telerik:RadAjaxLoadingPanel>
