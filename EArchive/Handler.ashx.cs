﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Intranet.DesktopModules.EArchiveProject.EArchive.BL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive
{
    /// <summary>
    /// Summary description for Handler
    /// </summary>
    public class Handler : IHttpHandler
    {
        private readonly EDocumentContentFileBL _eDocumentContentFileBL = new EDocumentContentFileBL();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request["EDocumentContentFileId"] != "")
            {
                var eDocumentContentFileId = Guid.Parse(context.Request["EDocumentContentFileId"]);
                var eDocumentContentFileEntity = new EDocumentContentFileEntity()
                {
                    EDocumentContentFileId = eDocumentContentFileId
                };
                var myEDocumentContentFile = _eDocumentContentFileBL.GetSingleById(eDocumentContentFileEntity);

                context.Response.BinaryWrite((byte[])myEDocumentContentFile.FilePath);                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}