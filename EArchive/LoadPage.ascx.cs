﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intranet.Configuration.Settings;

namespace Intranet.DesktopModules.EArchiveProject.EArchive
{
    public partial class LoadPage : ModuleControls
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect(Intranet.Common.IntranetUI.BuildUrl("/Page.aspx?mID=" + ModuleConfiguration.ModuleID + "&page=EArchiveProject/EArchive/BasicPage"));
        }
    }
}