﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/11>
    /// Description: <نوع بایگانی>
    /// </summary>
   
    public class ArchiveTypeDB
    {
        #region Methods :

        public void AddArchiveTypeDB(ArchiveTypeEntity archiveTypeEntityParam, out Guid ArchiveTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ArchiveTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(archiveTypeEntityParam.ArchiveTypeTitle);            
            parameters[2].Value = archiveTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ArchiveTypeAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ArchiveTypeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateArchiveTypeDB(ArchiveTypeEntity archiveTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ArchiveTypeTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = archiveTypeEntityParam.ArchiveTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(archiveTypeEntityParam.ArchiveTypeTitle);
            parameters[2].Value = archiveTypeEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ArchiveTypeUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteArchiveTypeDB(ArchiveTypeEntity ArchiveTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ArchiveTypeEntityParam.ArchiveTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ArchiveTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ArchiveTypeEntity GetSingleArchiveTypeDB(ArchiveTypeEntity ArchiveTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ArchiveTypeEntityParam.ArchiveTypeId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ArchiveTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetArchiveTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ArchiveTypeEntity> GetAllArchiveTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArchiveTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ArchiveTypeGetAll", new IDataParameter[] { }));
        }
        public List<ArchiveTypeEntity> GetAllArchiveTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArchiveTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ArchiveTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ArchiveTypeEntity> GetPageArchiveTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ArchiveType";
            parameters[5].Value = "ArchiveTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetArchiveTypeDBCollectionFromDataSet(ds, out count);
        }

        public ArchiveTypeEntity GetArchiveTypeDBFromDataReader(IDataReader reader)
        {
            return new ArchiveTypeEntity(Guid.Parse(reader["ArchiveTypeId"].ToString()),
                                    reader["ArchiveTypeTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ArchiveTypeEntity> GetArchiveTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ArchiveTypeEntity> lst = new List<ArchiveTypeEntity>();
                while (reader.Read())
                    lst.Add(GetArchiveTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }
            public List<ArchiveTypeEntity> GetArchiveTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetArchiveTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        #endregion
    }    
}
