﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <بایگانی>
    /// </summary>

    public class ArchiveDB
    {
        #region Methods :

        public void AddArchiveDB(ArchiveEntity archiveEntityParam, out Guid ArchiveId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ArchiveTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ArchiveOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ArchiveChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(archiveEntityParam.ArchiveTitle);
            parameters[2].Value = (archiveEntityParam.ArchiveOwnerId == null ? System.DBNull.Value : (object)archiveEntityParam.ArchiveOwnerId);
            parameters[3].Value = archiveEntityParam.ArchiveChildNo;
            parameters[4].Value = archiveEntityParam.ArchiveTypeId;
            parameters[5].Value = archiveEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ArchiveAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ArchiveId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateArchiveDB(ArchiveEntity archiveEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ArchiveTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ArchiveOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ArchiveChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = archiveEntityParam.ArchiveId;
            parameters[1].Value = FarsiToArabic.ToArabic(archiveEntityParam.ArchiveTitle);
            parameters[2].Value = (archiveEntityParam.ArchiveOwnerId==null?System.DBNull.Value:(object)archiveEntityParam.ArchiveOwnerId);
            parameters[3].Value = archiveEntityParam.ArchiveChildNo;
            parameters[4].Value = archiveEntityParam.ArchiveTypeId;
            parameters[5].Value = archiveEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ArchiveUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteArchiveDB(ArchiveEntity archiveEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = archiveEntityParam.ArchiveId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ArchiveDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ArchiveEntity GetSingleArchiveDB(ArchiveEntity ArchiveEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ArchiveEntityParam.ArchiveId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ArchiveGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetArchiveDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ArchiveEntity> GetAllArchiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArchiveDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ArchiveGetAll", new IDataParameter[] { }));
        }

        public List<ArchiveEntity> GetAllArchiveIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetArchiveDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ArchiveIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<ArchiveEntity> GetAllArchiveIsActivePerRowAccessDB(int userId, string objectTitlesForRowAccessStr)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@WebUserId", SqlDbType.Int),
                new SqlParameter("@ObjectTitlesForRowAccess_Str", SqlDbType.NVarChar, 100),
            };
            parameters[0].Value = userId;
            parameters[1].Value = objectTitlesForRowAccessStr;
            return GetArchiveDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("EArchive.p_ArchiveIsActiveGetAllPerRowAccess", parameters));
        }

        public List<ArchiveEntity> GetPageArchiveDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Archive";
            parameters[5].Value = "ArchiveId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetArchiveDBCollectionFromDataSet(ds, out count);
        }

        public ArchiveEntity GetArchiveDBFromDataReader(IDataReader reader)
        {
            return new ArchiveEntity(Guid.Parse(reader["ArchiveId"].ToString()),
                                    reader["ArchiveTitle"].ToString(),
                                    Convert.IsDBNull(reader["ArchiveOwnerId"]) ? null : (Guid?)reader["ArchiveOwnerId"],                                        
                                    int.Parse(reader["ArchiveChildNo"].ToString()),
                                    Guid.Parse(reader["ArchiveTypeId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ArchiveEntity> GetArchiveDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ArchiveEntity> lst = new List<ArchiveEntity>();
                while (reader.Read())
                    lst.Add(GetArchiveDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ArchiveEntity> GetArchiveDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetArchiveDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<ArchiveEntity> GetArchiveDBCollectionByArchiveTypeDB(ArchiveEntity archiveEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ArchiveTypeId", SqlDbType.UniqueIdentifier);
            parameter.Value = archiveEntityParam.ArchiveTypeId;
            return GetArchiveDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_ArchiveGetByArchiveType", new[] { parameter }));
        }


        #endregion

        
    }    
}
