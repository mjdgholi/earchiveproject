﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/14>
    /// Description: <نوع شی پایگاه داده>
    /// </summary>



    public class ObjectFieldTypeDB
    {



        #region Methods :

        public void AddObjectFieldTypeDB(ObjectFieldTypeEntity objectFieldTypeEntityParam, out int ObjectFieldTypeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ObjectFieldTypeId", SqlDbType.Int),
								 new SqlParameter("@ObjectFieldTypeTitle", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@ObjectFieldTypeDescription", SqlDbType.NVarChar,350) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(objectFieldTypeEntityParam.ObjectFieldTypeTitle);
            parameters[2].Value = FarsiToArabic.ToArabic(objectFieldTypeEntityParam.ObjectFieldTypeDescription);                                  
            parameters[3].Value =  objectFieldTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectFieldTypeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ObjectFieldTypeId = int.Parse(parameters[0].Value.ToString());
        }

        public void UpdateObjectFieldTypeDB(ObjectFieldTypeEntity objectFieldTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ObjectFieldTypeId", SqlDbType.Int),
						 new SqlParameter("@ObjectFieldTypeTitle", SqlDbType.NVarChar,150) ,
											  new SqlParameter("@ObjectFieldTypeDescription", SqlDbType.NVarChar,350) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = objectFieldTypeEntityParam.ObjectFieldTypeId;
            parameters[1].Value = FarsiToArabic.ToArabic(objectFieldTypeEntityParam.ObjectFieldTypeTitle);
            parameters[2].Value = FarsiToArabic.ToArabic(objectFieldTypeEntityParam.ObjectFieldTypeDescription);
            parameters[3].Value = objectFieldTypeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ObjectFieldTypeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteObjectFieldTypeDB(ObjectFieldTypeEntity ObjectFieldTypeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ObjectFieldTypeId", SqlDbType.Int),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ObjectFieldTypeEntityParam.ObjectFieldTypeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectFieldTypeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ObjectFieldTypeEntity GetSingleObjectFieldTypeDB(ObjectFieldTypeEntity ObjectFieldTypeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ObjectFieldTypeId", SqlDbType.Int)		                                   		  
						      };
                parameters[0].Value = ObjectFieldTypeEntityParam.ObjectFieldTypeId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ObjectFieldTypeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetObjectFieldTypeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectFieldTypeEntity> GetAllObjectFieldTypeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetObjectFieldTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ObjectFieldTypeGetAll", new IDataParameter[] { }));
        }

        public List<ObjectFieldTypeEntity> GetAllObjectFieldTypeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetObjectFieldTypeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ObjectFieldTypeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ObjectFieldTypeEntity> GetPageObjectFieldTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ObjectFieldType";
            parameters[5].Value = "ObjectFieldTypeId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetObjectFieldTypeDBCollectionFromDataSet(ds, out count);
        }

        public ObjectFieldTypeEntity GetObjectFieldTypeDBFromDataReader(IDataReader reader)
        {
            return new ObjectFieldTypeEntity(int.Parse(reader["ObjectFieldTypeId"].ToString()),
                                    reader["ObjectFieldTypeTitle"].ToString(),
                                    reader["ObjectFieldTypeDescription"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ObjectFieldTypeEntity> GetObjectFieldTypeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ObjectFieldTypeEntity> lst = new List<ObjectFieldTypeEntity>();
                while (reader.Read())
                    lst.Add(GetObjectFieldTypeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectFieldTypeEntity> GetObjectFieldTypeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetObjectFieldTypeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion


    }

    
}