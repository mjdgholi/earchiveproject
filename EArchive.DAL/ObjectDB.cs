﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <شی پایگاه داده>
    /// </summary>
  
    public class ObjectDB
    {
        #region Methods :

        public void AddObjectDB(ObjectEntity objectEntityParam, out Guid ObjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ObjectDataBaseName", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(objectEntityParam.ObjectDataBaseName);
            parameters[2].Value = objectEntityParam.SystemId;            
            parameters[3].Value = objectEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ObjectId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateObjectDB(ObjectEntity objectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ObjectDataBaseName", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = objectEntityParam.ObjectId;
            parameters[1].Value = FarsiToArabic.ToArabic(objectEntityParam.ObjectDataBaseName);
            parameters[2].Value = objectEntityParam.SystemId;            
            parameters[3].Value = objectEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ObjectUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteObjectDB(ObjectEntity ObjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ObjectEntityParam.ObjectId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ObjectEntity GetSingleObjectDB(ObjectEntity ObjectEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ObjectEntityParam.ObjectId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ObjectGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetObjectDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectEntity> GetAllObjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetObjectDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ObjectGetAll", new IDataParameter[] { }));
        }
        public List<ObjectEntity> GetAllObjectIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetObjectDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ObjectIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ObjectEntity> GetPageObjectDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Object";
            parameters[5].Value = "ObjectId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetObjectDBCollectionFromDataSet(ds, out count);
        }

        public ObjectEntity GetObjectDBFromDataReader(IDataReader reader)
        {
            return new ObjectEntity(Guid.Parse(reader["ObjectId"].ToString()),
                                    reader["ObjectDataBaseName"].ToString(),
                                    Guid.Parse(reader["SystemId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ObjectEntity> GetObjectDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ObjectEntity> lst = new List<ObjectEntity>();
                while (reader.Read())
                    lst.Add(GetObjectDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectEntity> GetObjectDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetObjectDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<ObjectEntity> GetObjectDBCollectionBySystemDB(ObjectEntity objectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier);
            parameter.Value = objectEntityParam.SystemId;
            return GetObjectDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_ObjectGetBySystem", new[] { parameter }));
        }


        #endregion



    }

    
}
