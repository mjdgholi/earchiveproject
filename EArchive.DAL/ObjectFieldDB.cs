﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <فیلد>
    /// </summary>


    public class ObjectFieldDB
    {

    
        #region Methods :

        public void AddObjectFieldDB(ObjectFieldEntity objectFieldEntityParam, out Guid ObjectFieldId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ObjectFieldId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ObjectFieldName", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,	
										      new SqlParameter("@ObjectFieldTypeId",SqlDbType.Int), 
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(objectFieldEntityParam.ObjectFieldName);
            parameters[2].Value = objectFieldEntityParam.ObjectId;
            parameters[3].Value = objectFieldEntityParam.ObjectFieldTypeId;
            parameters[4].Value = objectFieldEntityParam.IsActive;

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectFieldAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ObjectFieldId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateObjectFieldDB(ObjectFieldEntity objectFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ObjectFieldId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ObjectFieldName", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,	
										      new SqlParameter("@ObjectFieldTypeId",SqlDbType.Int), 
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = objectFieldEntityParam.ObjectFieldId;
            parameters[1].Value = FarsiToArabic.ToArabic(objectFieldEntityParam.ObjectFieldName);
            parameters[2].Value = objectFieldEntityParam.ObjectId;
            parameters[3].Value = objectFieldEntityParam.ObjectFieldTypeId;
            parameters[4].Value = objectFieldEntityParam.IsActive;
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ObjectFieldUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteObjectFieldDB(ObjectFieldEntity ObjectFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ObjectFieldId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ObjectFieldEntityParam.ObjectFieldId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ObjectFieldDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ObjectFieldEntity GetSingleObjectFieldDB(ObjectFieldEntity ObjectFieldEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ObjectFieldId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ObjectFieldEntityParam.ObjectFieldId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ObjectFieldGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetObjectFieldDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectFieldEntity> GetAllObjectFieldDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetObjectFieldDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ObjectFieldGetAll", new IDataParameter[] { }));
        }

        public List<ObjectFieldEntity> GetPageObjectFieldDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ObjectField";
            parameters[5].Value = "ObjectFieldId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetObjectFieldDBCollectionFromDataSet(ds, out count);
        }

        public ObjectFieldEntity GetObjectFieldDBFromDataReader(IDataReader reader)
        {
            return new ObjectFieldEntity(Guid.Parse(reader["ObjectFieldId"].ToString()),
                                    reader["ObjectFieldName"].ToString(),
                                    Guid.Parse(reader["ObjectId"].ToString()),
                                    int.Parse(reader["ObjectFieldTypeId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ObjectFieldEntity> GetObjectFieldDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ObjectFieldEntity> lst = new List<ObjectFieldEntity>();
                while (reader.Read())
                    lst.Add(GetObjectFieldDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ObjectFieldEntity> GetObjectFieldDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetObjectFieldDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<ObjectFieldEntity> GetObjectFieldDBCollectionByObjectDB(ObjectFieldEntity objectFieldEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier);
            parameter.Value = objectFieldEntityParam.ObjectId;
            return GetObjectFieldDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_ObjectFieldGetByObject", new[] { parameter }));
        }


        #endregion

        public void GetKeyAndTileForShowinSelectControlDB(EdocumentEntity edocumentEntity, out string keyAndTileForShowinSelectControl)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
																 									  
											  new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier) ,	
										  	new SqlParameter("@KeyAndTileForShowinSelectControl",SqlDbType.NVarChar,350), 										  								 
							};            
            parameters[0].Value = edocumentEntity.EDocumentContentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("[EArchive].[p_GetKeyAndTileForShowinSelectControl]", parameters);
            keyAndTileForShowinSelectControl = parameters[1].Value.ToString();

        }
    }
    
}
