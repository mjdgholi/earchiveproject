﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <سطح امنیت>
    /// </summary>

    
    public class SecurityLevelDB
    {
        #region Methods :

        public void AddSecurityLevelDB(SecurityLevelEntity securityLevelEntityParam, out Guid SecurityLevelId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SecurityLevelTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(securityLevelEntityParam.SecurityLevelTitle);
            parameters[2].Value = securityLevelEntityParam.Priority;
            parameters[3].Value = securityLevelEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SecurityLevelAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SecurityLevelId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSecurityLevelDB(SecurityLevelEntity securityLevelEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SecurityLevelTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = securityLevelEntityParam.SecurityLevelId;
            parameters[1].Value = FarsiToArabic.ToArabic(securityLevelEntityParam.SecurityLevelTitle);
            parameters[2].Value = securityLevelEntityParam.Priority;
            parameters[3].Value = securityLevelEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_SecurityLevelUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSecurityLevelDB(SecurityLevelEntity SecurityLevelEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SecurityLevelEntityParam.SecurityLevelId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SecurityLevelDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SecurityLevelEntity GetSingleSecurityLevelDB(SecurityLevelEntity SecurityLevelEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SecurityLevelEntityParam.SecurityLevelId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_SecurityLevelGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSecurityLevelDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SecurityLevelEntity> GetAllSecurityLevelDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSecurityLevelDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SecurityLevelGetAll", new IDataParameter[] { }));
        }
        public List<SecurityLevelEntity> GetAllSecurityLevelIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSecurityLevelDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SecurityLevelIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<SecurityLevelEntity> GetPageSecurityLevelDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_SecurityLevel";
            parameters[5].Value = "SecurityLevelId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetSecurityLevelDBCollectionFromDataSet(ds, out count);
        }

        public SecurityLevelEntity GetSecurityLevelDBFromDataReader(IDataReader reader)
        {
            return new SecurityLevelEntity(Guid.Parse(reader["SecurityLevelId"].ToString()),
                                    reader["SecurityLevelTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SecurityLevelEntity> GetSecurityLevelDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SecurityLevelEntity> lst = new List<SecurityLevelEntity>();
                while (reader.Read())
                    lst.Add(GetSecurityLevelDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SecurityLevelEntity> GetSecurityLevelDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSecurityLevelDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

        
    }

    
}
