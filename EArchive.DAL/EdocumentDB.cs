﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <مستندالکترونیکی>
    /// </summary>

    public class EdocumentDB
    {
        #region Methods :

        public void AddEdocumentDB(EdocumentEntity edocumentEntityParam, out Guid EdocumentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EdocumentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EdocumentNo", SqlDbType.NVarChar,15) ,
											  new SqlParameter("@EdocumentTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@EDocumentDate", SqlDbType.SmallDateTime) ,
											  new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = edocumentEntityParam.EdocumentNo;
            parameters[2].Value = FarsiToArabic.ToArabic(edocumentEntityParam.EdocumentTitle);
            parameters[3].Value = edocumentEntityParam.EDocumentDate;
            parameters[4].Value = edocumentEntityParam.ContentTemplateTreeId;            
            parameters[5].Value = edocumentEntityParam.ArchiveId;
            parameters[6].Value = edocumentEntityParam.ScopeId;
            parameters[7].Value = edocumentEntityParam.SubjectId;
            parameters[8].Value = edocumentEntityParam.StatusId;
            parameters[9].Value = FarsiToArabic.ToArabic(edocumentEntityParam.Description);

            parameters[10].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EdocumentAdd", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EdocumentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEdocumentDB(EdocumentEntity EdocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EdocumentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EdocumentNo", SqlDbType.NVarChar,15) ,
											  new SqlParameter("@EdocumentTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@EDocumentDate", SqlDbType.SmallDateTime) ,											  										  
											  new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@Description", SqlDbType.NVarChar,-1) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = EdocumentEntityParam.EdocumentId;
            parameters[1].Value = FarsiToArabic.ToArabic(EdocumentEntityParam.EdocumentNo);
            parameters[2].Value = FarsiToArabic.ToArabic(EdocumentEntityParam.EdocumentTitle);
            parameters[3].Value = EdocumentEntityParam.EDocumentDate;            
            parameters[4].Value = EdocumentEntityParam.ArchiveId;
            parameters[5].Value = EdocumentEntityParam.ScopeId;
            parameters[6].Value = EdocumentEntityParam.SubjectId;
            parameters[7].Value = EdocumentEntityParam.StatusId;
            parameters[8].Value = FarsiToArabic.ToArabic(EdocumentEntityParam.Description);

            parameters[9].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_EdocumentUpdate", parameters);
            var messageError = parameters[9].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEdocumentDB(EdocumentEntity EdocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EdocumentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EdocumentEntityParam.EdocumentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EdocumentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EdocumentEntity GetSingleEdocumentDB(EdocumentEntity EdocumentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EdocumentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EdocumentEntityParam.EdocumentId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_EdocumentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEdocumentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EdocumentEntity> GetAllEdocumentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEdocumentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_EdocumentGetAll", new IDataParameter[] { }));
        }

        public List<EdocumentEntity> GetPageEdocumentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Edocument";
            parameters[5].Value = "EdocumentId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetEdocumentDBCollectionFromDataSet(ds, out count);
        }

        public EdocumentEntity GetEdocumentDBFromDataReader(IDataReader reader)
        {
            return new EdocumentEntity(Guid.Parse(reader["EdocumentId"].ToString()),
                                    reader["EdocumentNo"].ToString(),
                                    reader["EdocumentTitle"].ToString(),
                                    reader["EDocumentDate"].ToString(),
                                    Guid.Parse(reader["ContentTemplateTreeId"].ToString()),
                                    Guid.Parse(reader["EDocumentContentId"].ToString()),
                                    Guid.Parse(reader["ArchiveId"].ToString()),
                                    Guid.Parse(reader["ScopeId"].ToString()),
                                    Guid.Parse(reader["SubjectId"].ToString()),
                                    Guid.Parse(reader["StatusId"].ToString()),
                                    reader["Description"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    reader["ContentTemplateTreeTitle"].ToString(),
                                    reader["EDocumentContentTitle"].ToString()
                                    );
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EdocumentEntity> lst = new List<EdocumentEntity>();
                while (reader.Read())
                    lst.Add(GetEdocumentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEdocumentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionByArchiveDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ArchiveId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.ArchiveId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetByArchive", new[] { parameter }));
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionByContentTemplateTreeDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.ContentTemplateTreeId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetByContentTemplateTree", new[] { parameter }));
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionByEDocumentContentDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.EDocumentContentId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetByEDocumentContent", new[] { parameter }));
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionByScopeDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.ScopeId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetByScope", new[] { parameter }));
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionByStatusDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.StatusId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetByStatus", new[] { parameter }));
        }

        public List<EdocumentEntity> GetEdocumentDBCollectionBySubjectDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier);
            parameter.Value = edocumentEntityParam.SubjectId;
            return GetEdocumentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EdocumentGetBySubject", new[] { parameter }));
        }


        #endregion



    }

    }
