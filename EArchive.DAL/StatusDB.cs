﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <وضیعت>
    /// </summary>
    #region Class "StatusDB"

    public class StatusDB
    {
        #region Methods :

        public void AddStatusDB(StatusEntity statusEntityParam, out Guid StatusId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@StatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(statusEntityParam.StatusTitle);
            parameters[2].Value = statusEntityParam.Priority;
            parameters[3].Value = statusEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_StatusAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            StatusId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateStatusDB(StatusEntity statusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@StatusTitle", SqlDbType.NVarChar,100) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = statusEntityParam.StatusId;
            parameters[1].Value = FarsiToArabic.ToArabic(statusEntityParam.StatusTitle);
            parameters[2].Value = statusEntityParam.Priority;
            parameters[3].Value = statusEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_StatusUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteStatusDB(StatusEntity StatusEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = StatusEntityParam.StatusId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_StatusDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public StatusEntity GetSingleStatusDB(StatusEntity StatusEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@StatusId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = StatusEntityParam.StatusId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_StatusGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetStatusDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<StatusEntity> GetAllStatusDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_StatusGetAll", new IDataParameter[] { }));
        }
        public List<StatusEntity> GetAllStatusIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetStatusDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_StatusIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<StatusEntity> GetPageStatusDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Status";
            parameters[5].Value = "StatusId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetStatusDBCollectionFromDataSet(ds, out count);
        }

        public StatusEntity GetStatusDBFromDataReader(IDataReader reader)
        {
            return new StatusEntity(Guid.Parse(reader["StatusId"].ToString()),
                                    reader["StatusTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<StatusEntity> GetStatusDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<StatusEntity> lst = new List<StatusEntity>();
                while (reader.Read())
                    lst.Add(GetStatusDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<StatusEntity> GetStatusDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetStatusDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
