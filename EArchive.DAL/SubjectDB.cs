﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <موضوع>
    /// </summary>

    public class SubjectDB
    {
        #region Methods :

        public void AddSubjectDB(SubjectEntity subjectEntityParam, out Guid SubjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SubjectTitle", SqlDbType.NVarChar,350) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(subjectEntityParam.SubjectTitle);            
            parameters[2].Value = subjectEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SubjectAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SubjectId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSubjectDB(SubjectEntity subjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@SubjectTitle", SqlDbType.NVarChar,350) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = subjectEntityParam.SubjectId;
            parameters[1].Value = FarsiToArabic.ToArabic(subjectEntityParam.SubjectTitle);            
            parameters[2].Value = subjectEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_SubjectUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSubjectDB(SubjectEntity SubjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SubjectEntityParam.SubjectId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SubjectDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SubjectEntity GetSingleSubjectDB(SubjectEntity SubjectEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SubjectEntityParam.SubjectId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_SubjectGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSubjectDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SubjectEntity> GetAllSubjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSubjectDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SubjectGetAll", new IDataParameter[] { }));
        }
        public List<SubjectEntity> GetAllSubjectIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSubjectDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SubjectIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<SubjectEntity> GetPageSubjectDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Subject";
            parameters[5].Value = "SubjectId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetSubjectDBCollectionFromDataSet(ds, out count);
        }

        public SubjectEntity GetSubjectDBFromDataReader(IDataReader reader)
        {
            return new SubjectEntity(Guid.Parse(reader["SubjectId"].ToString()),
                                    reader["SubjectTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SubjectEntity> GetSubjectDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SubjectEntity> lst = new List<SubjectEntity>();
                while (reader.Read())
                    lst.Add(GetSubjectDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SubjectEntity> GetSubjectDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSubjectDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }
    
}
