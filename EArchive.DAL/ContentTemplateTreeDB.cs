﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <درخت الگویی فهرست بندی>
    /// </summary>

    public class ContentTemplateTreeDB
    {
        #region Methods :

        public void AddContentTemplateTreeDB(ContentTemplateTreeEntity contentTemplateTreeEntityParam, out Guid ContentTemplateTreeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ContentTemplateTreeOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ContentTemplateTreeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ContentTemplateTreeChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (contentTemplateTreeEntityParam.ContentTemplateTreeOwnerId == null ? System.DBNull.Value : (object)contentTemplateTreeEntityParam.ContentTemplateTreeOwnerId);
            parameters[2].Value = contentTemplateTreeEntityParam.ContentTemplateTreeTitle;
            parameters[3].Value = contentTemplateTreeEntityParam.ContentTemplateTreeChildNo;
            parameters[4].Value = (contentTemplateTreeEntityParam.ObjectId == null ? System.DBNull.Value : (object)contentTemplateTreeEntityParam.ObjectId);                        
            parameters[5].Value = contentTemplateTreeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ContentTemplateTreeAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ContentTemplateTreeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateContentTemplateTreeDB(ContentTemplateTreeEntity contentTemplateTreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ContentTemplateTreeOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@ContentTemplateTreeTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@ContentTemplateTreeChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = contentTemplateTreeEntityParam.ContentTemplateTreeId;
            parameters[1].Value = (contentTemplateTreeEntityParam.ContentTemplateTreeOwnerId == null ? System.DBNull.Value : (object)contentTemplateTreeEntityParam.ContentTemplateTreeOwnerId);
            parameters[2].Value = contentTemplateTreeEntityParam.ContentTemplateTreeTitle;
            parameters[3].Value = contentTemplateTreeEntityParam.ContentTemplateTreeChildNo;
            parameters[4].Value = (contentTemplateTreeEntityParam.ObjectId==null?System.DBNull.Value:(object)contentTemplateTreeEntityParam.ObjectId);            
            parameters[5].Value = contentTemplateTreeEntityParam.IsActive;

            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ContentTemplateTreeUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteContentTemplateTreeDB(ContentTemplateTreeEntity ContentTemplateTreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ContentTemplateTreeEntityParam.ContentTemplateTreeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ContentTemplateTreeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ContentTemplateTreeEntity GetSingleContentTemplateTreeDB(ContentTemplateTreeEntity ContentTemplateTreeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ContentTemplateTreeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ContentTemplateTreeEntityParam.ContentTemplateTreeId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ContentTemplateTreeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetContentTemplateTreeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ContentTemplateTreeEntity> GetAllContentTemplateTreeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetContentTemplateTreeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ContentTemplateTreeGetAll", new IDataParameter[] { }));
        }
        public List<ContentTemplateTreeEntity> GetAllContentTemplateTreeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetContentTemplateTreeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ContentTemplateTreeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ContentTemplateTreeEntity> GetPageContentTemplateTreeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_ContentTemplateTree";
            parameters[5].Value = "ContentTemplateTreeId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetContentTemplateTreeDBCollectionFromDataSet(ds, out count);
        }

        public ContentTemplateTreeEntity GetContentTemplateTreeDBFromDataReader(IDataReader reader)
        {
            return new ContentTemplateTreeEntity(Guid.Parse(reader["ContentTemplateTreeId"].ToString()),
                Convert.IsDBNull(reader["ContentTemplateTreeOwnerId"]) ? null : (Guid?)reader["ContentTemplateTreeOwnerId"],                                         
                                    reader["ContentTemplateTreeTitle"].ToString(),
                                    int.Parse(reader["ContentTemplateTreeChildNo"].ToString()),
                                    Convert.IsDBNull(reader["ObjectId"]) ? null : (Guid?)reader["ObjectId"],                                                                         
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ContentTemplateTreeEntity> GetContentTemplateTreeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ContentTemplateTreeEntity> lst = new List<ContentTemplateTreeEntity>();
                while (reader.Read())
                    lst.Add(GetContentTemplateTreeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ContentTemplateTreeEntity> GetContentTemplateTreeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetContentTemplateTreeDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<ContentTemplateTreeEntity> GetContentTemplateTreeDBCollectionByContentTemplateTreeDB(ContentTemplateTreeEntity contentTemplateTreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ContentTemplateTreeOwnerId", SqlDbType.UniqueIdentifier);
            parameter.Value = contentTemplateTreeEntityParam.ContentTemplateTreeOwnerId;
            return GetContentTemplateTreeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_ContentTemplateTreeGetByContentTemplateTree", new[] { parameter }));
        }

        public  List<ContentTemplateTreeEntity> GetContentTemplateTreeDBCollectionByObjectDB(ContentTemplateTreeEntity contentTemplateTreeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier);
            parameter.Value = contentTemplateTreeEntityParam.ObjectId;
            return GetContentTemplateTreeDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_ContentTemplateTreeGetByObject", new[] { parameter }));
        }


        #endregion



    }

}
