﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/27>
    /// Description: <محتوای فایل>
    /// </summary>


    public class EDocumentContentFileDB
    {
        #region Methods :

        public void AddEDocumentContentFileDB(EDocumentContentFileEntity eDocumentContentFileEntityParam,
            out Guid EDocumentContentFileId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@EDocumentContentFileId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),                
                new SqlParameter("@EDocumentContentExternalId", SqlDbType.UniqueIdentifier),  
                new SqlParameter("@FileTitle", SqlDbType.NVarChar, 350),
                new SqlParameter("@FileFormatMIMETitle", SqlDbType.NVarChar,250),
                new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FilePath", SqlDbType.VarBinary),
                new SqlParameter("@PageNo", SqlDbType.Int),
                new SqlParameter("@KeyWord", SqlDbType.NVarChar, -1),
                new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (eDocumentContentFileEntityParam.EDocumentContentId==null?System.DBNull.Value:(object)eDocumentContentFileEntityParam.EDocumentContentId);          
            parameters[2].Value=(eDocumentContentFileEntityParam.EDocumentContentExternalId==null?System.DBNull.Value:(object)eDocumentContentFileEntityParam.EDocumentContentExternalId);
            parameters[3].Value = (eDocumentContentFileEntityParam.FileTitle==""?System.DBNull.Value:(object)eDocumentContentFileEntityParam.FileTitle);
            parameters[4].Value = eDocumentContentFileEntityParam.FileFormatTypeMimeTitle;
            parameters[5].Value = eDocumentContentFileEntityParam.SecurityLevelId;
            parameters[6].Value = eDocumentContentFileEntityParam.FilePath;
            parameters[7].Value = (eDocumentContentFileEntityParam.PageNo == null ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.PageNo);
            parameters[8].Value = (eDocumentContentFileEntityParam.KeyWord == "" ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.KeyWord);
            parameters[9].Value = (eDocumentContentFileEntityParam.Description == "" ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.Description);


            parameters[10].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EDocumentContentFileAdd", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EDocumentContentFileId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEDocumentContentFileDB(EDocumentContentFileEntity eDocumentContentFileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@EDocumentContentFileId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),  
                new SqlParameter("@EDocumentContentExternalId", SqlDbType.UniqueIdentifier), 
                new SqlParameter("@FileTitle", SqlDbType.NVarChar, 350),
                new SqlParameter("@FileFormatMIMETitle", SqlDbType.NVarChar,250),
                new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@FilePath", SqlDbType.VarBinary),
                new SqlParameter("@PageNo", SqlDbType.Int),
                new SqlParameter("@KeyWord", SqlDbType.NVarChar, -1),
                new SqlParameter("@Description", SqlDbType.NVarChar, -1),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = eDocumentContentFileEntityParam.EDocumentContentFileId;
            parameters[1].Value = (eDocumentContentFileEntityParam.EDocumentContentId == null ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.EDocumentContentId);  
            parameters[2].Value = (eDocumentContentFileEntityParam.EDocumentContentExternalId == null ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.EDocumentContentExternalId);
            parameters[3].Value = (eDocumentContentFileEntityParam.FileTitle == "" ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.FileTitle);
            parameters[4].Value = eDocumentContentFileEntityParam.FileFormatTypeMimeTitle;
            parameters[5].Value = eDocumentContentFileEntityParam.SecurityLevelId;
            parameters[6].Value = (eDocumentContentFileEntityParam.FilePath==null?System.DBNull.Value:(object)eDocumentContentFileEntityParam.FilePath);
            parameters[7].Value = (eDocumentContentFileEntityParam.PageNo == null ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.PageNo);
            parameters[8].Value = (eDocumentContentFileEntityParam.KeyWord == "" ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.KeyWord);
            parameters[9].Value = (eDocumentContentFileEntityParam.Description == "" ? System.DBNull.Value : (object)eDocumentContentFileEntityParam.Description);


            parameters[10].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_EDocumentContentFileUpdate", parameters);
            var messageError = parameters[10].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEDocumentContentFileDB(EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {
                new SqlParameter("@EDocumentContentFileId", SqlDbType.UniqueIdentifier),
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };
            parameters[0].Value = EDocumentContentFileEntityParam.EDocumentContentFileId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EDocumentContentFileDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EDocumentContentFileEntity GetSingleEDocumentContentFileDB(
            EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@EDocumentContentFileId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = EDocumentContentFileEntityParam.EDocumentContentFileId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_EDocumentContentFileGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEDocumentContentFileDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }


        public EDocumentContentFileEntity GetSingleFilePathEDocumentContentFileDB(EDocumentContentFileEntity eDocumentContentFileEntity)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =
                {
                    new SqlParameter("@EDocumentContentFileId", SqlDbType.UniqueIdentifier)
                };
                parameters[0].Value = eDocumentContentFileEntity.EDocumentContentFileId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_EDocumentContentFileFilePatheGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEDocumentContentFileDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EDocumentContentFileEntity> GetAllEDocumentContentFileDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEDocumentContentFileDBCollectionFromDataReader(
                _intranetDB.RunProcedureReader
                    ("EArchive.p_EDocumentContentFileGetAll", new IDataParameter[] {}));
        }

        public List<EDocumentContentFileEntity> GetPageEDocumentContentFileDB(int PageSize, int CurrentPage,
            string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =
            {
                new SqlParameter("@PageSize", SqlDbType.Int),
                new SqlParameter("@CurrentPage", SqlDbType.Int),
                new SqlParameter("@WhereClause", SqlDbType.NVarChar, 1000),
                new SqlParameter("@OrderBy", SqlDbType.NVarChar, 100),
                new SqlParameter("@TableName", SqlDbType.NVarChar, 50),
                new SqlParameter("@PrimaryKey", SqlDbType.NVarChar, 50),
            };
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EDocumentContentFile";
            parameters[5].Value = "EDocumentContentFileId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetEDocumentContentFileDBCollectionFromDataSet(ds, out count);
        }

        public EDocumentContentFileEntity GetEDocumentContentFileDBFromDataReader(IDataReader reader)
        {
            return new EDocumentContentFileEntity(Guid.Parse(reader["EDocumentContentFileId"].ToString()),            
            Convert.IsDBNull(reader["EDocumentContentId"]) ? null : (Guid?)reader["EDocumentContentId"],  
            Convert.IsDBNull(reader["EDocumentContentExternalId"]) ? null : (Guid?)reader["EDocumentContentExternalId"],                
            reader["FileTitle"].ToString(),
            Guid.Parse(reader["FileFormatId"].ToString()),
            Guid.Parse(reader["SecurityLevelId"].ToString()),             
            reader["FilePath"] as byte[],
            Convert.IsDBNull(reader["PageNo"]) ? null : (int?)reader["PageNo"],                            
            reader["KeyWord"].ToString(),
            reader["Description"].ToString(),
            reader["CreationDate"].ToString(),
            reader["ModificationDate"].ToString(),
            reader["FileFormatTypeMIMETitle"].ToString(),
            reader["FileFormatTypeExtensionTitle"].ToString())
            ;
        }

        public List<EDocumentContentFileEntity> GetEDocumentContentFileDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EDocumentContentFileEntity> lst = new List<EDocumentContentFileEntity>();
                while (reader.Read())
                    lst.Add(GetEDocumentContentFileDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EDocumentContentFileEntity> GetEDocumentContentFileDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEDocumentContentFileDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public List<EDocumentContentFileEntity> GetEDocumentContentFileDBCollectionByFileFormatDB(
            EDocumentContentFileEntity eDocumentContentFileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@FileFormatId", SqlDbType.UniqueIdentifier);
            parameter.Value = eDocumentContentFileEntityParam.FileFormatId;
            return
                GetEDocumentContentFileDBCollectionFromDataReader(
                    _intranetDB.RunProcedureReader("EArchive.p_EDocumentContentFileGetByFileFormat", new[] {parameter}));
        }

        public List<EDocumentContentFileEntity> GetEDocumentContentFileDBCollectionBySecurityLevelDB(
            EDocumentContentFileEntity eDocumentContentFileEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SecurityLevelId", SqlDbType.UniqueIdentifier);
            parameter.Value = eDocumentContentFileEntityParam.SecurityLevelId;
            return
                GetEDocumentContentFileDBCollectionFromDataReader(
                    _intranetDB.RunProcedureReader("EArchive.p_EDocumentContentFileGetBySecurityLevel",
                        new[] {parameter}));
        }


        #endregion

        public DataSet ObjectRelatedEDocumentContentFileDB(EdocumentEntity edocumentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								
								 new SqlParameter("@EdocumentNo", SqlDbType.NVarChar,15) ,											  
											  new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier) ,											  											  								 
							};            
            parameters[0].Value = edocumentEntityParam.EdocumentNo;            
            parameters[1].Value = edocumentEntityParam.EDocumentContentId;                        
         return   _intranetDB.RunProcedureDS("[EArchive].[p_ObjectinGridViewGetAll]", parameters);
        }

        public DataSet GetValueTileForShowinSelectControlDB(Guid eDocumentContentFileId, string eDocumentNo)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@eDocumentContentFileId", SqlDbType.UniqueIdentifier) ,											  											  								 
								 new SqlParameter("@EdocumentNo", SqlDbType.NVarChar,15) 										  											 
							};
            parameters[0].Value = eDocumentContentFileId;
            parameters[1].Value = eDocumentNo;
            return _intranetDB.RunProcedureDS("[EArchive].[p_GetValueTileForShowinSelectControl]", parameters);
        }


        public void InitializedEDocumentContentIdinEDocumentContentFileDB(EdocumentEntity edocumentEntity)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters =
            {                
                new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),                  
                new SqlParameter("@EdocumentNo", SqlDbType.NVarChar,50),                
                new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
            };

            parameters[0].Value = edocumentEntity.EDocumentContentId;
            parameters[1].Value = edocumentEntity.EdocumentNo;
            parameters[2].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("[EArchive].[p_InitializedEDocumentContentIdinEDocumentContentFile]", parameters);
            var messageError = parameters[2].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }
    }
}


