﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <سطح دسترسی>
    /// </summary>
   
    public class AccessLevelDB
    {
        #region Methods :

        public void AddAccessLevelDB(AccessLevelEntity accessLevelEntityParam, out Guid AccessLevelId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@AccessLevelId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@AccessLevelTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(accessLevelEntityParam.AccessLevelTitle);            
            parameters[2].Value = accessLevelEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_AccessLevelAdd", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            AccessLevelId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateAccessLevelDB(AccessLevelEntity accessLevelEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@AccessLevelId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@AccessLevelTitle", SqlDbType.NVarChar,100) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = accessLevelEntityParam.AccessLevelId;
            parameters[1].Value = FarsiToArabic.ToArabic(accessLevelEntityParam.AccessLevelTitle);            
            parameters[2].Value = accessLevelEntityParam.IsActive;

            parameters[3].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_AccessLevelUpdate", parameters);
            var messageError = parameters[3].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteAccessLevelDB(AccessLevelEntity AccessLevelEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@AccessLevelId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = AccessLevelEntityParam.AccessLevelId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_AccessLevelDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public AccessLevelEntity GetSingleAccessLevelDB(AccessLevelEntity AccessLevelEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@AccessLevelId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = AccessLevelEntityParam.AccessLevelId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_AccessLevelGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetAccessLevelDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AccessLevelEntity> GetAllAccessLevelDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAccessLevelDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_AccessLevelGetAll", new IDataParameter[] { }));
        }

        public List<AccessLevelEntity> GetAllAccessLevelIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetAccessLevelDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_AccessLevelIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<AccessLevelEntity> GetPageAccessLevelDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_AccessLevel";
            parameters[5].Value = "AccessLevelId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetAccessLevelDBCollectionFromDataSet(ds, out count);
        }

        public AccessLevelEntity GetAccessLevelDBFromDataReader(IDataReader reader)
        {
            return new AccessLevelEntity(Guid.Parse(reader["AccessLevelId"].ToString()),
                                    reader["AccessLevelTitle"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<AccessLevelEntity> GetAccessLevelDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<AccessLevelEntity> lst = new List<AccessLevelEntity>();
                while (reader.Read())
                    lst.Add(GetAccessLevelDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<AccessLevelEntity> GetAccessLevelDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetAccessLevelDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion

    }   
}
