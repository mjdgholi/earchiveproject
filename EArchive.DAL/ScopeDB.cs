﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/07>
    /// Description: <حوزه>
    /// </summary>

    //-----------------------------------------------------
    #region Class "ScopeDB"

    public class ScopeDB
    {



        #region Methods :

        public void AddScopeDB(ScopeEntity ScopeEntityParam, out Guid ScopeId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ScopeTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = FarsiToArabic.ToArabic(ScopeEntityParam.ScopeTitle);
            parameters[2].Value = ScopeEntityParam.Priority;
            parameters[3].Value = ScopeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ScopeAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            ScopeId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateScopeDB(ScopeEntity ScopeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ScopeTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@Priority", SqlDbType.Int) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = ScopeEntityParam.ScopeId;
            parameters[1].Value = FarsiToArabic.ToArabic(ScopeEntityParam.ScopeTitle);
            parameters[2].Value = ScopeEntityParam.Priority;
            parameters[3].Value = ScopeEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_ScopeUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteScopeDB(ScopeEntity ScopeEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = ScopeEntityParam.ScopeId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_ScopeDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public ScopeEntity GetSingleScopeDB(ScopeEntity ScopeEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = ScopeEntityParam.ScopeId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_ScopeGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetScopeDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ScopeEntity> GetAllScopeDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetScopeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ScopeGetAll", new IDataParameter[] { }));
        }

        public List<ScopeEntity> GetAllScopeIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetScopeDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_ScopeIsActiveGetAll", new IDataParameter[] { }));
        }
        public List<ScopeEntity> GetPageScopeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Scope";
            parameters[5].Value = "ScopeId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetScopeDBCollectionFromDataSet(ds, out count);
        }

        public ScopeEntity GetScopeDBFromDataReader(IDataReader reader)
        {
            return new ScopeEntity(Guid.Parse(reader["ScopeId"].ToString()),
                                    reader["ScopeTitle"].ToString(),
                                    int.Parse(reader["Priority"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<ScopeEntity> GetScopeDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<ScopeEntity> lst = new List<ScopeEntity>();
                while (reader.Read())
                    lst.Add(GetScopeDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<ScopeEntity> GetScopeDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetScopeDBCollectionFromDataReader(ds.CreateDataReader());
        }


        #endregion



    }

    #endregion
}
