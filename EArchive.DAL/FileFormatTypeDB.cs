﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <نوع فرمت فایل>
    /// </summary>


        public class FileFormatTypeDB
        {
            #region Methods :

            public void AddFileFormatTypeDB(FileFormatTypeEntity fileFormatTypeEntityParam, out Guid FileFormatTypeId)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = 	{
								 new SqlParameter("@FileFormatTypeId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@FileFormatTypeExtensionTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@FileFormatTypeMIMETitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
                parameters[0].Direction = ParameterDirection.Output;
                parameters[1].Value = fileFormatTypeEntityParam.FileFormatTypeExtensionTitle;
                parameters[2].Value = fileFormatTypeEntityParam.FileFormatTypeMIMETitle;
                parameters[3].Value = fileFormatTypeEntityParam.IsActive;                

                parameters[4].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("EArchive.p_FileFormatTypeAdd", parameters);
                var messageError = parameters[4].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
                FileFormatTypeId = new Guid(parameters[0].Value.ToString());
            }

            public void UpdateFileFormatTypeDB(FileFormatTypeEntity FileFormatTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
			                            	new SqlParameter("@FileFormatTypeId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@FileFormatTypeExtensionTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@FileFormatTypeMIMETitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,					
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

                parameters[0].Value = FileFormatTypeEntityParam.FileFormatTypeId;
                parameters[1].Value = FileFormatTypeEntityParam.FileFormatTypeExtensionTitle;
                parameters[2].Value = FileFormatTypeEntityParam.FileFormatTypeMIMETitle;
                parameters[3].Value = FileFormatTypeEntityParam.IsActive;

                parameters[4].Direction = ParameterDirection.Output;

                _intranetDB.RunProcedure("EArchive.p_FileFormatTypeUpdate", parameters);
                var messageError = parameters[4].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public void DeleteFileFormatTypeDB(FileFormatTypeEntity FileFormatTypeEntityParam)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                SqlParameter[] parameters = {
		                            		new SqlParameter("@FileFormatTypeId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
                parameters[0].Value = FileFormatTypeEntityParam.FileFormatTypeId;
                parameters[1].Direction = ParameterDirection.Output;
                _intranetDB.RunProcedure("EArchive.p_FileFormatTypeDel", parameters);
                var messageError = parameters[1].Value.ToString();
                if (messageError != "")
                    throw (new Exception(messageError));
            }

            public FileFormatTypeEntity GetSingleFileFormatTypeDB(FileFormatTypeEntity FileFormatTypeEntityParam)
            {
                IDataReader reader = null;
                try
                {

                    IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                    IDataParameter[] parameters = {
													new SqlParameter("@FileFormatTypeId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                    parameters[0].Value = FileFormatTypeEntityParam.FileFormatTypeId;

                    reader = _intranetDB.RunProcedureReader("EArchive.p_FileFormatTypeGetSingle", parameters);
                    if (reader != null)
                        if (reader.Read())
                            return GetFileFormatTypeDBFromDataReader(reader);
                    return null;
                }

                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<FileFormatTypeEntity> GetAllFileFormatTypeDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetFileFormatTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("EArchive.p_FileFormatTypeGetAll", new IDataParameter[] { }));
            }

            public List<FileFormatTypeEntity> GetAllFileFormatTypeIsActiveDB()
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                return GetFileFormatTypeDBCollectionFromDataReader(
                            _intranetDB.RunProcedureReader
                            ("EArchive.p_FileFormatTypeIsActiveGetAll", new IDataParameter[] { }));
            }
            public List<FileFormatTypeEntity> GetPageFileFormatTypeDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
            {
                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
                parameters[0].Value = PageSize;
                parameters[1].Value = CurrentPage;
                parameters[2].Value = WhereClause;
                parameters[3].Value = OrderBy;
                parameters[4].Value = "t_FileFormatType";
                parameters[5].Value = "FileFormatTypeId";
                DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
                return GetFileFormatTypeDBCollectionFromDataSet(ds, out count);
            }

            public FileFormatTypeEntity GetFileFormatTypeDBFromDataReader(IDataReader reader)
            {
                return new FileFormatTypeEntity(Guid.Parse(reader["FileFormatTypeId"].ToString()),
                                        reader["FileFormatTypeExtensionTitle"].ToString(),
                                        reader["FileFormatTypeMIMETitle"].ToString(),
                                        bool.Parse(reader["IsActive"].ToString()),
                                        reader["CreationDate"].ToString(),
                                        reader["ModificationDate"].ToString());
            }

            public List<FileFormatTypeEntity> GetFileFormatTypeDBCollectionFromDataReader(IDataReader reader)
            {
                try
                {
                    List<FileFormatTypeEntity> lst = new List<FileFormatTypeEntity>();
                    while (reader.Read())
                        lst.Add(GetFileFormatTypeDBFromDataReader(reader));
                    return lst;
                }
                finally
                {
                    if (!reader.IsClosed)
                        reader.Close();
                }
            }

            public List<FileFormatTypeEntity> GetFileFormatTypeDBCollectionFromDataSet(DataSet ds, out int count)
            {
                count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                return GetFileFormatTypeDBCollectionFromDataReader(ds.CreateDataReader());
            }


            #endregion

        }


        
    }

