﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <محتوی>
    /// </summary>

    //-----------------------------------------------------
    #region Class "EDocumentContentDB"

    public class EDocumentContentDB
    {


        #region Methods :

        public void AddEDocumentContentDB(EDocumentContentEntity eDocumentContentEntityParam, out Guid EDocumentContentId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@EDocumentContentOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EDocumentContentTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EDocumentContentChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,											  
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (eDocumentContentEntityParam.EDocumentContentOwnerId==null?System.DBNull.Value:(object)eDocumentContentEntityParam.EDocumentContentOwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(eDocumentContentEntityParam.EDocumentContentTitle);
            parameters[3].Value = eDocumentContentEntityParam.EDocumentContentChildNo;
            parameters[4].Value = (eDocumentContentEntityParam.ObjectId == null ? System.DBNull.Value : (object)eDocumentContentEntityParam.ObjectId);            

            parameters[5].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EDocumentContentAdd", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            EDocumentContentId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateEDocumentContentDB(EDocumentContentEntity eDocumentContentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@EDocumentContentOwnerId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@EDocumentContentTitle", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@EDocumentContentChildNo", SqlDbType.Int) ,
											  new SqlParameter("@ObjectId", SqlDbType.UniqueIdentifier) ,											  
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = eDocumentContentEntityParam.EDocumentContentId;
            parameters[1].Value = (eDocumentContentEntityParam.EDocumentContentOwnerId == null ? System.DBNull.Value : (object)eDocumentContentEntityParam.EDocumentContentOwnerId);
            parameters[2].Value = FarsiToArabic.ToArabic(eDocumentContentEntityParam.EDocumentContentTitle);
            parameters[3].Value = eDocumentContentEntityParam.EDocumentContentChildNo;
            parameters[4].Value = (eDocumentContentEntityParam.ObjectId==null?System.DBNull.Value:(object)eDocumentContentEntityParam.ObjectId);            
            parameters[5].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_EDocumentContentUpdate", parameters);
            var messageError = parameters[5].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteEDocumentContentDB(EDocumentContentEntity EDocumentContentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = EDocumentContentEntityParam.EDocumentContentId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_EDocumentContentDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public EDocumentContentEntity GetSingleEDocumentContentDB(EDocumentContentEntity EDocumentContentEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = EDocumentContentEntityParam.EDocumentContentId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_EDocumentContentGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetEDocumentContentDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EDocumentContentEntity> GetAllEDocumentContentDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetEDocumentContentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_EDocumentContentGetAll", new IDataParameter[] { }));
        }

        public List<EDocumentContentEntity> GetPageEDocumentContentDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_EDocumentContent";
            parameters[5].Value = "EDocumentContentId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetEDocumentContentDBCollectionFromDataSet(ds, out count);
        }

        public EDocumentContentEntity GetEDocumentContentDBFromDataReader(IDataReader reader)
        {
            return new EDocumentContentEntity(Guid.Parse(reader["EDocumentContentId"].ToString()),
                       Convert.IsDBNull(reader["EDocumentContentOwnerId"]) ? null : (Guid?)reader["EDocumentContentOwnerId"],                                       
                                    reader["EDocumentContentTitle"].ToString(),
                                    int.Parse(reader["EDocumentContentChildNo"].ToString()),
                                    Convert.IsDBNull(reader["ObjectId"]) ? null : (Guid?)reader["ObjectId"],                                     
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString());
        }

        public List<EDocumentContentEntity> GetEDocumentContentDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<EDocumentContentEntity> lst = new List<EDocumentContentEntity>();
                while (reader.Read())
                    lst.Add(GetEDocumentContentDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<EDocumentContentEntity> GetEDocumentContentDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetEDocumentContentDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<EDocumentContentEntity> GetEDocumentContentDBCollectionByEDocumentContentDB(EDocumentContentEntity eDocumentContentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@EDocumentContentOwnerId", SqlDbType.UniqueIdentifier);
            parameter.Value = eDocumentContentEntityParam.EDocumentContentOwnerId;
            return GetEDocumentContentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EDocumentContentGetByEDocumentContent", new[] { parameter }));
        }

        public List<EDocumentContentEntity> GetEDocumentContentDBCollectionByObjectDB(EDocumentContentEntity eDocumentContentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ObjectId", SqlDbType.Int);
            parameter.Value = eDocumentContentEntityParam.ObjectId;
            return GetEDocumentContentDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_EDocumentContentGetByObject", new[] { parameter }));
        }


        #endregion

        public List<EDocumentContentEntity> GetAllEDocumentContentByEDocumentContentIdDB(EDocumentContentEntity eDocumentContentEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters = {
													new SqlParameter("@EDocumentContentId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
            parameters[0].Value = eDocumentContentEntityParam.EDocumentContentId;
            return GetEDocumentContentDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_EDocumentContentGetByEDocumentContentId", parameters));
        }
    }

    #endregion
}
