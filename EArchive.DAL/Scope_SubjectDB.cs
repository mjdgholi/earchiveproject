﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <حوزه و موضوع>
    /// </summary>

    //-----------------------------------------------------
    #region Class "Scope_SubjectDB"

    public class Scope_SubjectDB
    {



        #region Methods :

        public void AddScope_SubjectDB(Scope_SubjectEntity scopeSubjectEntityParam, out Guid Scope_SubjectId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@Scope_SubjectId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = scopeSubjectEntityParam.ScopeId;
            parameters[2].Value = scopeSubjectEntityParam.SubjectId;
            parameters[3].Value = scopeSubjectEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_Scope_SubjectAdd", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            Scope_SubjectId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateScope_SubjectDB(Scope_SubjectEntity scopeSubjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@Scope_SubjectId", SqlDbType.UniqueIdentifier),
						 new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier) ,
											  new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = scopeSubjectEntityParam.Scope_SubjectId;
            parameters[1].Value = scopeSubjectEntityParam.ScopeId;
            parameters[2].Value = scopeSubjectEntityParam.SubjectId;
            parameters[3].Value = scopeSubjectEntityParam.IsActive;

            parameters[4].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_Scope_SubjectUpdate", parameters);
            var messageError = parameters[4].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteScope_SubjectDB(Scope_SubjectEntity Scope_SubjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@Scope_SubjectId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = Scope_SubjectEntityParam.Scope_SubjectId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_Scope_SubjectDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public Scope_SubjectEntity GetSingleScope_SubjectDB(Scope_SubjectEntity Scope_SubjectEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@Scope_SubjectId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = Scope_SubjectEntityParam.Scope_SubjectId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_Scope_SubjectGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetScope_SubjectDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Scope_SubjectEntity> GetAllScope_SubjectDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetScope_SubjectDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_Scope_SubjectGetAll", new IDataParameter[] { }));
        }

        public List<Scope_SubjectEntity> GetPageScope_SubjectDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_Scope_Subject";
            parameters[5].Value = "Scope_SubjectId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetScope_SubjectDBCollectionFromDataSet(ds, out count);
        }

        public Scope_SubjectEntity GetScope_SubjectDBFromDataReader(IDataReader reader)
        {
            return new Scope_SubjectEntity(Guid.Parse(reader["Scope_SubjectId"].ToString()),
                                    Guid.Parse(reader["ScopeId"].ToString()),
                                    Guid.Parse(reader["SubjectId"].ToString()),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<Scope_SubjectEntity> GetScope_SubjectDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<Scope_SubjectEntity> lst = new List<Scope_SubjectEntity>();
                while (reader.Read())
                    lst.Add(GetScope_SubjectDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<Scope_SubjectEntity> GetScope_SubjectDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetScope_SubjectDBCollectionFromDataReader(ds.CreateDataReader());
        }

        public  List<Scope_SubjectEntity> GetScope_SubjectDBCollectionByScopeDB(Scope_SubjectEntity scopeSubjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@ScopeId", SqlDbType.UniqueIdentifier);
            parameter.Value = scopeSubjectEntityParam.ScopeId;
            return GetScope_SubjectDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_Scope_SubjectGetByScope", new[] { parameter }));
        }

        public  List<Scope_SubjectEntity> GetScope_SubjectDBCollectionBySubjectDB(Scope_SubjectEntity scopeSubjectEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter parameter = new SqlParameter("@SubjectId", SqlDbType.UniqueIdentifier);
            parameter.Value = scopeSubjectEntityParam.SubjectId;
            return GetScope_SubjectDBCollectionFromDataReader(_intranetDB.RunProcedureReader("EArchive.p_Scope_SubjectGetBySubject", new[] { parameter }));
        }


        #endregion



    }

    #endregion
}
