﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Intranet.Common;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;
using ITC.Library.Classes;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.DAL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <سیستم>
    /// </summary>
    
    public class SystemDB
    {        
        #region Methods :

        public void AddSystemDB(SystemEntity systemEntityParam, out Guid SystemId)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = 	{
								 new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier),
								 new SqlParameter("@SystemTitle", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@LinkedServerName", SqlDbType.NVarChar,200) ,
											  new SqlParameter("@DataBaseName", SqlDbType.NVarChar,350) ,
											  new SqlParameter("@SchemaName", SqlDbType.NVarChar,200) ,											  
											  new SqlParameter("@IsActive", SqlDbType.Bit) ,
								 new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
							};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = (systemEntityParam.SystemTitle == "" ? System.DBNull.Value : (Object)FarsiToArabic.ToArabic(systemEntityParam.SystemTitle));
            parameters[2].Value = (systemEntityParam.LinkedServerName == "" ? System.DBNull.Value : (Object)FarsiToArabic.ToArabic(systemEntityParam.LinkedServerName));
            parameters[3].Value = FarsiToArabic.ToArabic(systemEntityParam.DataBaseName);
            parameters[4].Value = FarsiToArabic.ToArabic(systemEntityParam.SchemaName);            
            parameters[5].Value = systemEntityParam.IsActive;
            parameters[6].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SystemAdd", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
            SystemId = new Guid(parameters[0].Value.ToString());
        }

        public void UpdateSystemDB(SystemEntity systemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
			                            	new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier),
						                    new SqlParameter("@SystemTitle", SqlDbType.NVarChar,350) ,
											new SqlParameter("@LinkedServerName", SqlDbType.NVarChar,200) ,
											new SqlParameter("@DataBaseName", SqlDbType.NVarChar,350) ,
											new SqlParameter("@SchemaName", SqlDbType.NVarChar,200) ,  
											new SqlParameter("@IsActive", SqlDbType.Bit) ,
						                    new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
				     		  };

            parameters[0].Value = systemEntityParam.SystemId;
            parameters[1].Value = (systemEntityParam.SystemTitle==""?System.DBNull.Value:(Object)FarsiToArabic.ToArabic(systemEntityParam.SystemTitle));
            parameters[2].Value = (systemEntityParam.LinkedServerName==""?System.DBNull.Value:(Object)FarsiToArabic.ToArabic(systemEntityParam.LinkedServerName));
            parameters[3].Value = FarsiToArabic.ToArabic(systemEntityParam.DataBaseName);
            parameters[4].Value = FarsiToArabic.ToArabic(systemEntityParam.SchemaName);            
            parameters[5].Value = systemEntityParam.IsActive;
            parameters[6].Direction = ParameterDirection.Output;

            _intranetDB.RunProcedure("EArchive.p_SystemUpdate", parameters);
            var messageError = parameters[6].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public void DeleteSystemDB(SystemEntity SystemEntityParam)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            SqlParameter[] parameters = {
		                            		new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier),
						new SqlParameter("@MessageError", SqlDbType.NVarChar, 300)
						  };
            parameters[0].Value = SystemEntityParam.SystemId;
            parameters[1].Direction = ParameterDirection.Output;
            _intranetDB.RunProcedure("EArchive.p_SystemDel", parameters);
            var messageError = parameters[1].Value.ToString();
            if (messageError != "")
                throw (new Exception(messageError));
        }

        public SystemEntity GetSingleSystemDB(SystemEntity SystemEntityParam)
        {
            IDataReader reader = null;
            try
            {

                IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
                IDataParameter[] parameters = {
													new SqlParameter("@SystemId", SqlDbType.UniqueIdentifier)		                                   		  
						      };
                parameters[0].Value = SystemEntityParam.SystemId;

                reader = _intranetDB.RunProcedureReader("EArchive.p_SystemGetSingle", parameters);
                if (reader != null)
                    if (reader.Read())
                        return GetSystemDBFromDataReader(reader);
                return null;
            }

            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SystemEntity> GetAllSystemDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSystemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SystemGetAll", new IDataParameter[] { }));
        }
        public List<SystemEntity> GetAllSystemIsActiveDB()
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            return GetSystemDBCollectionFromDataReader(
                        _intranetDB.RunProcedureReader
                        ("EArchive.p_SystemIsActiveGetAll", new IDataParameter[] { }));
        }

        public List<SystemEntity> GetPageSystemDB(int PageSize, int CurrentPage, string WhereClause, string OrderBy, out int count)
        {
            IntranetDB _intranetDB = IntranetDB.GetIntranetDB();
            IDataParameter[] parameters =	{
												new SqlParameter("@PageSize",SqlDbType.Int),
												new SqlParameter("@CurrentPage",SqlDbType.Int),
												new SqlParameter("@WhereClause",SqlDbType.NVarChar,1000),
												new SqlParameter("@OrderBy",SqlDbType.NVarChar,100),
												new SqlParameter("@TableName",SqlDbType.NVarChar,50),
												new SqlParameter("@PrimaryKey",SqlDbType.NVarChar,50),
											};
            parameters[0].Value = PageSize;
            parameters[1].Value = CurrentPage;
            parameters[2].Value = WhereClause;
            parameters[3].Value = OrderBy;
            parameters[4].Value = "t_System";
            parameters[5].Value = "SystemId";
            DataSet ds = _intranetDB.RunProcedureDS("EArchive.p_TablesGetPage", parameters);
            return GetSystemDBCollectionFromDataSet(ds, out count);
        }

        public SystemEntity GetSystemDBFromDataReader(IDataReader reader)
        {
            return new SystemEntity(Guid.Parse(reader["SystemId"].ToString()),
                                    reader["SystemTitle"].ToString(),
                                    reader["LinkedServerName"].ToString(),
                                    reader["DataBaseName"].ToString(),
                                    reader["SchemaName"].ToString(),
                                    reader["CreationDate"].ToString(),
                                    reader["ModificationDate"].ToString(),
                                    bool.Parse(reader["IsActive"].ToString()));
        }

        public List<SystemEntity> GetSystemDBCollectionFromDataReader(IDataReader reader)
        {
            try
            {
                List<SystemEntity> lst = new List<SystemEntity>();
                while (reader.Read())
                    lst.Add(GetSystemDBFromDataReader(reader));
                return lst;
            }
            finally
            {
                if (!reader.IsClosed)
                    reader.Close();
            }
        }

        public List<SystemEntity> GetSystemDBCollectionFromDataSet(DataSet ds, out int count)
        {
            count = int.Parse(ds.Tables[1].Rows[0][0].ToString());
            return GetSystemDBCollectionFromDataReader(ds.CreateDataReader());
        }
        #endregion
    }
    
}
