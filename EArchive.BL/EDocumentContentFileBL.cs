﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/27>
    /// Description: <محتوای فایل>
    /// </summary>

    public class EDocumentContentFileBL
    {
        private readonly EDocumentContentFileDB _EDocumentContentFileDB;

        public EDocumentContentFileBL()
        {
            _EDocumentContentFileDB = new EDocumentContentFileDB();
        }

        public void Add(EDocumentContentFileEntity EDocumentContentFileEntityParam, out Guid EDocumentContentFileId)
        {
            _EDocumentContentFileDB.AddEDocumentContentFileDB(EDocumentContentFileEntityParam,
                out EDocumentContentFileId);
        }

        public void Update(EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            _EDocumentContentFileDB.UpdateEDocumentContentFileDB(EDocumentContentFileEntityParam);
        }

        public void Delete(EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            _EDocumentContentFileDB.DeleteEDocumentContentFileDB(EDocumentContentFileEntityParam);
        }

        public EDocumentContentFileEntity GetSingleById(EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            EDocumentContentFileEntity o = GetEDocumentContentFileFromEDocumentContentFileDB(
                _EDocumentContentFileDB.GetSingleEDocumentContentFileDB(EDocumentContentFileEntityParam));

            return o;
        }
        public EDocumentContentFileEntity GetSingleFilePathById(EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            EDocumentContentFileEntity o = GetEDocumentContentFileFromEDocumentContentFileDB(
     _EDocumentContentFileDB.GetSingleFilePathEDocumentContentFileDB(EDocumentContentFileEntityParam));

            return o;
        }
        public List<EDocumentContentFileEntity> GetAll()
        {
            List<EDocumentContentFileEntity> lst = new List<EDocumentContentFileEntity>();
            //string key = "EDocumentContentFile_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EDocumentContentFileEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEDocumentContentFileCollectionFromEDocumentContentFileDBList(
                _EDocumentContentFileDB.GetAllEDocumentContentFileDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<EDocumentContentFileEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "EDocumentContentFile_List_Page_" + currentPage ;
            //string countKey = "EDocumentContentFile_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EDocumentContentFileEntity> lst = new List<EDocumentContentFileEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EDocumentContentFileEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEDocumentContentFileCollectionFromEDocumentContentFileDBList(
                _EDocumentContentFileDB.GetPageEDocumentContentFileDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EDocumentContentFileEntity GetEDocumentContentFileFromEDocumentContentFileDB(
            EDocumentContentFileEntity o)
        {
            if (o == null)
                return null;
            EDocumentContentFileEntity ret = new EDocumentContentFileEntity(o.EDocumentContentFileId,
                o.EDocumentContentId, o.EDocumentContentExternalId, o.FileTitle, o.FileFormatId, o.SecurityLevelId,
                o.FilePath, o.PageNo, o.KeyWord, o.Description, o.CreationDate, o.ModificationDate, o.FileFormatTypeMimeTitle,o.FileFormatTypeExtensionTitle);
            return ret;
        }

        private List<EDocumentContentFileEntity> GetEDocumentContentFileCollectionFromEDocumentContentFileDBList(
            List<EDocumentContentFileEntity> lst)
        {
            List<EDocumentContentFileEntity> RetLst = new List<EDocumentContentFileEntity>();
            foreach (EDocumentContentFileEntity o in lst)
            {
                RetLst.Add(GetEDocumentContentFileFromEDocumentContentFileDB(o));
            }
            return RetLst;

        }



        public List<EDocumentContentFileEntity> GetEDocumentContentFileCollectionByFileFormat(
            EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            return
                GetEDocumentContentFileCollectionFromEDocumentContentFileDBList(
                    _EDocumentContentFileDB.GetEDocumentContentFileDBCollectionByFileFormatDB(
                        EDocumentContentFileEntityParam));
        }

        public List<EDocumentContentFileEntity> GetEDocumentContentFileCollectionBySecurityLevel(
            EDocumentContentFileEntity EDocumentContentFileEntityParam)
        {
            return
                GetEDocumentContentFileCollectionFromEDocumentContentFileDBList(
                    _EDocumentContentFileDB.GetEDocumentContentFileDBCollectionBySecurityLevelDB(
                        EDocumentContentFileEntityParam));
        }


        public DataSet ObjectRelatedEDocumentContentFile(EdocumentEntity  edocumentEntityParam)
        {
            return _EDocumentContentFileDB.ObjectRelatedEDocumentContentFileDB(edocumentEntityParam);
        }

        public DataSet GetValueTileForShowinSelectControl(Guid EDocumentContentFileId, string EDocumentNo)
        {
           return _EDocumentContentFileDB.GetValueTileForShowinSelectControlDB(EDocumentContentFileId, EDocumentNo);
        }

        public void InitializedEDocumentContentIdinEDocumentContentFile(EdocumentEntity edocumentEntity)
        {
            _EDocumentContentFileDB.InitializedEDocumentContentIdinEDocumentContentFileDB(edocumentEntity);
        }
    }


}
