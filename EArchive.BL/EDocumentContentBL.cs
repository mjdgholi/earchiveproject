﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <محتوی>
    /// </summary>


    public class EDocumentContentBL
    {
        private readonly EDocumentContentDB _EDocumentContentDB;

        public EDocumentContentBL()
        {
            _EDocumentContentDB = new EDocumentContentDB();
        }

        public void Add(EDocumentContentEntity EDocumentContentEntityParam, out Guid EDocumentContentId)
        {
            _EDocumentContentDB.AddEDocumentContentDB(EDocumentContentEntityParam, out EDocumentContentId);
        }

        public void Update(EDocumentContentEntity EDocumentContentEntityParam)
        {
            _EDocumentContentDB.UpdateEDocumentContentDB(EDocumentContentEntityParam);
        }

        public void Delete(EDocumentContentEntity EDocumentContentEntityParam)
        {
            _EDocumentContentDB.DeleteEDocumentContentDB(EDocumentContentEntityParam);
        }

        public EDocumentContentEntity GetSingleById(EDocumentContentEntity EDocumentContentEntityParam)
        {
            EDocumentContentEntity o = GetEDocumentContentFromEDocumentContentDB(
                _EDocumentContentDB.GetSingleEDocumentContentDB(EDocumentContentEntityParam));

            return o;
        }

        public List<EDocumentContentEntity> GetAll()
        {
            List<EDocumentContentEntity> lst = new List<EDocumentContentEntity>();
            //string key = "EDocumentContent_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EDocumentContentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEDocumentContentCollectionFromEDocumentContentDBList(
                _EDocumentContentDB.GetAllEDocumentContentDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<EDocumentContentEntity> GetAllByEDocumentContentId(EDocumentContentEntity eDocumentContentEntityParam)
        {
            List<EDocumentContentEntity> lst = new List<EDocumentContentEntity>();
            //string key = "EDocumentContent_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EDocumentContentEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetEDocumentContentCollectionFromEDocumentContentDBList(
                _EDocumentContentDB.GetAllEDocumentContentByEDocumentContentIdDB(eDocumentContentEntityParam));
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<EDocumentContentEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "EDocumentContent_List_Page_" + currentPage ;
            //string countKey = "EDocumentContent_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<EDocumentContentEntity> lst = new List<EDocumentContentEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<EDocumentContentEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetEDocumentContentCollectionFromEDocumentContentDBList(
                _EDocumentContentDB.GetPageEDocumentContentDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private EDocumentContentEntity GetEDocumentContentFromEDocumentContentDB(EDocumentContentEntity o)
        {
            if (o == null)
                return null;
            EDocumentContentEntity ret = new EDocumentContentEntity(o.EDocumentContentId, o.EDocumentContentOwnerId,
                o.EDocumentContentTitle, o.EDocumentContentChildNo, o.ObjectId, o.CreationDate, o.ModificationDate);
            return ret;
        }

        private List<EDocumentContentEntity> GetEDocumentContentCollectionFromEDocumentContentDBList(
            List<EDocumentContentEntity> lst)
        {
            List<EDocumentContentEntity> RetLst = new List<EDocumentContentEntity>();
            foreach (EDocumentContentEntity o in lst)
            {
                RetLst.Add(GetEDocumentContentFromEDocumentContentDB(o));
            }
            return RetLst;

        }


        public List<EDocumentContentEntity> GetEDocumentContentCollectionByEDocumentContent(
            EDocumentContentEntity eDocumentContentEntityParam)
        {
            return
                GetEDocumentContentCollectionFromEDocumentContentDBList(
                    _EDocumentContentDB.GetEDocumentContentDBCollectionByEDocumentContentDB(eDocumentContentEntityParam));
        }

        public List<EDocumentContentEntity> GetEDocumentContentCollectionByObject(
            EDocumentContentEntity eDocumentContentEntityParam)
        {
            return
                GetEDocumentContentCollectionFromEDocumentContentDBList(
                    _EDocumentContentDB.GetEDocumentContentDBCollectionByObjectDB(eDocumentContentEntityParam));
        }

    }

}
