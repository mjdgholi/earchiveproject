﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <حوزه و موضوع>
    /// </summary>


	public class Scope_SubjectBL 
	{	
	  	 private readonly Scope_SubjectDB _Scope_SubjectDB;					
			
		public Scope_SubjectBL()
		{
			_Scope_SubjectDB = new Scope_SubjectDB();
		}			
	
		public  void Add(Scope_SubjectEntity  Scope_SubjectEntityParam, out Guid Scope_SubjectId)
		{ 
			_Scope_SubjectDB.AddScope_SubjectDB(Scope_SubjectEntityParam,out Scope_SubjectId);			
		}

		public  void Update(Scope_SubjectEntity  Scope_SubjectEntityParam)
		{
			_Scope_SubjectDB.UpdateScope_SubjectDB(Scope_SubjectEntityParam);		
		}

		public  void Delete(Scope_SubjectEntity  Scope_SubjectEntityParam)
		{
			 _Scope_SubjectDB.DeleteScope_SubjectDB(Scope_SubjectEntityParam);			
		}

		public  Scope_SubjectEntity GetSingleById(Scope_SubjectEntity  Scope_SubjectEntityParam)
		{
			Scope_SubjectEntity o = GetScope_SubjectFromScope_SubjectDB(
			_Scope_SubjectDB.GetSingleScope_SubjectDB(Scope_SubjectEntityParam));
			
			return o;
		}

		public  List<Scope_SubjectEntity> GetAll()
		{
			List<Scope_SubjectEntity> lst = new List<Scope_SubjectEntity>();
			//string key = "Scope_Subject_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Scope_SubjectEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetScope_SubjectCollectionFromScope_SubjectDBList(
				_Scope_SubjectDB.GetAllScope_SubjectDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<Scope_SubjectEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Scope_Subject_List_Page_" + currentPage ;
			//string countKey = "Scope_Subject_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<Scope_SubjectEntity> lst = new List<Scope_SubjectEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<Scope_SubjectEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetScope_SubjectCollectionFromScope_SubjectDBList(
				_Scope_SubjectDB.GetPageScope_SubjectDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  Scope_SubjectEntity GetScope_SubjectFromScope_SubjectDB(Scope_SubjectEntity o)
		{
	if(o == null)
                return null;
			Scope_SubjectEntity ret = new Scope_SubjectEntity(o.Scope_SubjectId ,o.ScopeId ,o.SubjectId ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<Scope_SubjectEntity> GetScope_SubjectCollectionFromScope_SubjectDBList( List<Scope_SubjectEntity> lst)
		{
			List<Scope_SubjectEntity> RetLst = new List<Scope_SubjectEntity>();
			foreach(Scope_SubjectEntity o in lst)
			{
				RetLst.Add(GetScope_SubjectFromScope_SubjectDB(o));
			}
			return RetLst;
			
		}

        public List<Scope_SubjectEntity> GetScope_SubjectCollectionByScope(Scope_SubjectEntity scopeSubjectEntityParam)
{
    return GetScope_SubjectCollectionFromScope_SubjectDBList(_Scope_SubjectDB.GetScope_SubjectDBCollectionByScopeDB(scopeSubjectEntityParam));
}

        public List<Scope_SubjectEntity> GetScope_SubjectCollectionBySubject(Scope_SubjectEntity scopeSubjectEntityParam)
{
    return GetScope_SubjectCollectionFromScope_SubjectDBList(_Scope_SubjectDB.GetScope_SubjectDBCollectionBySubjectDB(scopeSubjectEntityParam));
}




}

}
