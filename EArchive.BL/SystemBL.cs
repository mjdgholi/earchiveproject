﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <سیستم>
    /// </summary>   
	public class SystemBL 
	{	
	  	 private readonly SystemDB _SystemDB;					
			
		public SystemBL()
		{
			_SystemDB = new SystemDB();
		}			
	
		public  void Add(SystemEntity  SystemEntityParam, out Guid SystemId)
		{ 
			_SystemDB.AddSystemDB(SystemEntityParam,out SystemId);			
		}

		public  void Update(SystemEntity  SystemEntityParam)
		{
			_SystemDB.UpdateSystemDB(SystemEntityParam);		
		}

		public  void Delete(SystemEntity  SystemEntityParam)
		{
			 _SystemDB.DeleteSystemDB(SystemEntityParam);			
		}

		public  SystemEntity GetSingleById(SystemEntity  SystemEntityParam)
		{
			SystemEntity o = GetSystemFromSystemDB(
			_SystemDB.GetSingleSystemDB(SystemEntityParam));
			
			return o;
		}

		public  List<SystemEntity> GetAll()
		{
			List<SystemEntity> lst = new List<SystemEntity>();
			//string key = "System_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SystemEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSystemCollectionFromSystemDBList(
				_SystemDB.GetAllSystemDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<SystemEntity> GetAllIsActive()
        {
            List<SystemEntity> lst = new List<SystemEntity>();
            //string key = "System_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SystemEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSystemCollectionFromSystemDBList(
            _SystemDB.GetAllSystemIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<SystemEntity> GetAllPaging(int currentPage, int pageSize, string sortExpression, out int count, string whereClause)
		{			
			//string key = "System_List_Page_" + currentPage ;
			//string countKey = "System_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SystemEntity> lst = new List<SystemEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SystemEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSystemCollectionFromSystemDBList(
				_SystemDB.GetPageSystemDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SystemEntity GetSystemFromSystemDB(SystemEntity o)
		{
	if(o == null)
                return null;
			SystemEntity ret = new SystemEntity(o.SystemId ,o.SystemTitle ,o.LinkedServerName ,o.DataBaseName ,o.SchemaName ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<SystemEntity> GetSystemCollectionFromSystemDBList( List<SystemEntity> lst)
		{
			List<SystemEntity> RetLst = new List<SystemEntity>();
			foreach(SystemEntity o in lst)
			{
				RetLst.Add(GetSystemFromSystemDB(o));
			}
			return RetLst;			
		} 					
	}
}

