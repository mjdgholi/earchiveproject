﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/07>
    /// Description: <حوزه>
    /// </summary>

	public class ScopeBL 
	{	
	  	 private readonly ScopeDB _ScopeDB;					
			
		public ScopeBL()
		{
			_ScopeDB = new ScopeDB();
		}			
	
		public  void Add(ScopeEntity  ScopeEntityParam, out Guid ScopeId)
		{ 
			_ScopeDB.AddScopeDB(ScopeEntityParam,out ScopeId);			
		}

		public  void Update(ScopeEntity  ScopeEntityParam)
		{
			_ScopeDB.UpdateScopeDB(ScopeEntityParam);		
		}

		public  void Delete(ScopeEntity  ScopeEntityParam)
		{
			 _ScopeDB.DeleteScopeDB(ScopeEntityParam);			
		}

		public  ScopeEntity GetSingleById(ScopeEntity  ScopeEntityParam)
		{
			ScopeEntity o = GetScopeFromScopeDB(
			_ScopeDB.GetSingleScopeDB(ScopeEntityParam));
			
			return o;
		}

		public  List<ScopeEntity> GetAll()
		{
			List<ScopeEntity> lst = new List<ScopeEntity>();
			//string key = "Scope_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ScopeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetScopeCollectionFromScopeDBList(
				_ScopeDB.GetAllScopeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<ScopeEntity> GetAllIsActive()
        {
            List<ScopeEntity> lst = new List<ScopeEntity>();
            //string key = "Scope_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ScopeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetScopeCollectionFromScopeDBList(
            _ScopeDB.GetAllScopeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ScopeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Scope_List_Page_" + currentPage ;
			//string countKey = "Scope_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ScopeEntity> lst = new List<ScopeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ScopeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetScopeCollectionFromScopeDBList(
				_ScopeDB.GetPageScopeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ScopeEntity GetScopeFromScopeDB(ScopeEntity o)
		{
	if(o == null)
                return null;
			ScopeEntity ret = new ScopeEntity(o.ScopeId ,o.ScopeTitle ,o.Priority ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ScopeEntity> GetScopeCollectionFromScopeDBList( List<ScopeEntity> lst)
		{
			List<ScopeEntity> RetLst = new List<ScopeEntity>();
			foreach(ScopeEntity o in lst)
			{
				RetLst.Add(GetScopeFromScopeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}



}

