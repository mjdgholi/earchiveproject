﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <بایگانی>
    /// </summary>

    public class ArchiveBL
    {
        private readonly ArchiveDB _ArchiveDB;

        public ArchiveBL()
        {
            _ArchiveDB = new ArchiveDB();
        }

        public void Add(ArchiveEntity ArchiveEntityParam, out Guid ArchiveId)
        {
            _ArchiveDB.AddArchiveDB(ArchiveEntityParam, out ArchiveId);
        }

        public void Update(ArchiveEntity ArchiveEntityParam)
        {
            _ArchiveDB.UpdateArchiveDB(ArchiveEntityParam);
        }

        public void Delete(ArchiveEntity ArchiveEntityParam)
        {
            _ArchiveDB.DeleteArchiveDB(ArchiveEntityParam);
        }

        public ArchiveEntity GetSingleById(ArchiveEntity ArchiveEntityParam)
        {
            ArchiveEntity o = GetArchiveFromArchiveDB(
                _ArchiveDB.GetSingleArchiveDB(ArchiveEntityParam));

            return o;
        }

        public List<ArchiveEntity> GetAll()
        {
            List<ArchiveEntity> lst = new List<ArchiveEntity>();
            //string key = "Archive_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ArchiveEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetArchiveCollectionFromArchiveDBList(
                _ArchiveDB.GetAllArchiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<ArchiveEntity> GetAllIsActive()
        {
            List<ArchiveEntity> lst = new List<ArchiveEntity>();
            //string key = "Archive_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ArchiveEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetArchiveCollectionFromArchiveDBList(
                _ArchiveDB.GetAllArchiveIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<ArchiveEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "Archive_List_Page_" + currentPage ;
            //string countKey = "Archive_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ArchiveEntity> lst = new List<ArchiveEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ArchiveEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetArchiveCollectionFromArchiveDBList(
                _ArchiveDB.GetPageArchiveDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ArchiveEntity GetArchiveFromArchiveDB(ArchiveEntity o)
        {
            if (o == null)
                return null;
            ArchiveEntity ret = new ArchiveEntity(o.ArchiveId, o.ArchiveTitle, o.ArchiveOwnerId, o.ArchiveChildNo,
                o.ArchiveTypeId, o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ArchiveEntity> GetArchiveCollectionFromArchiveDBList(List<ArchiveEntity> lst)
        {
            List<ArchiveEntity> RetLst = new List<ArchiveEntity>();
            foreach (ArchiveEntity o in lst)
            {
                RetLst.Add(GetArchiveFromArchiveDB(o));
            }
            return RetLst;
        }

        public List<ArchiveEntity> GetArchiveCollectionByArchiveType(ArchiveEntity archiveEntityParam)
        {
            return GetArchiveCollectionFromArchiveDBList(_ArchiveDB.GetArchiveDBCollectionByArchiveTypeDB(archiveEntityParam));
        }

        public List<ArchiveEntity> GetAllIsActivePerRowAccess(int userId, string objectTitlesForRowAccessStr)
        {
            List<ArchiveEntity> lst = GetArchiveCollectionFromArchiveDBList(
                _ArchiveDB.GetAllArchiveIsActivePerRowAccessDB(userId,objectTitlesForRowAccessStr));
            return lst;
        }
    }
}


