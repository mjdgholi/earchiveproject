﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <شی پایگاه داده>
    /// </summary>
    
	public class ObjectBL 
	{	
	  	 private readonly ObjectDB _ObjectDB;					
			
		public ObjectBL()
		{
			_ObjectDB = new ObjectDB();
		}			
	
		public  void Add(ObjectEntity  ObjectEntityParam, out Guid ObjectId)
		{ 
			_ObjectDB.AddObjectDB(ObjectEntityParam,out ObjectId);			
		}

		public  void Update(ObjectEntity  ObjectEntityParam)
		{
			_ObjectDB.UpdateObjectDB(ObjectEntityParam);		
		}

		public  void Delete(ObjectEntity  ObjectEntityParam)
		{
			 _ObjectDB.DeleteObjectDB(ObjectEntityParam);			
		}

		public  ObjectEntity GetSingleById(ObjectEntity  ObjectEntityParam)
		{
			ObjectEntity o = GetObjectFromObjectDB(
			_ObjectDB.GetSingleObjectDB(ObjectEntityParam));
			
			return o;
		}

		public  List<ObjectEntity> GetAll()
		{
			List<ObjectEntity> lst = new List<ObjectEntity>();
			//string key = "Object_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ObjectEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetObjectCollectionFromObjectDBList(
				_ObjectDB.GetAllObjectDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<ObjectEntity> GetAllIsActive()
        {
            List<ObjectEntity> lst = new List<ObjectEntity>();
            //string key = "Object_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ObjectEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetObjectCollectionFromObjectDBList(
            _ObjectDB.GetAllObjectIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ObjectEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Object_List_Page_" + currentPage ;
			//string countKey = "Object_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ObjectEntity> lst = new List<ObjectEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ObjectEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetObjectCollectionFromObjectDBList(
				_ObjectDB.GetPageObjectDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ObjectEntity GetObjectFromObjectDB(ObjectEntity o)
		{
	if(o == null)
                return null;
			ObjectEntity ret = new ObjectEntity(o.ObjectId ,o.ObjectDataBaseName ,o.SystemId ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ObjectEntity> GetObjectCollectionFromObjectDBList( List<ObjectEntity> lst)
		{
			List<ObjectEntity> RetLst = new List<ObjectEntity>();
			foreach(ObjectEntity o in lst)
			{
				RetLst.Add(GetObjectFromObjectDB(o));
			}
			return RetLst;
			
		}

        public List<ObjectEntity> GetObjectCollectionBySystem(ObjectEntity ObjectEntityParam)
{
	return GetObjectCollectionFromObjectDBList(_ObjectDB.GetObjectDBCollectionBySystemDB(ObjectEntityParam));
}

}

}
