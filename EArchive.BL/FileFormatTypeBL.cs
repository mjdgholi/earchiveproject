﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{

    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <نوع فرمت فایل>
    /// </summary>

	public class FileFormatTypeBL 
	{	
	  	 private readonly FileFormatTypeDB _FileFormatTypeDB;					
			
		public FileFormatTypeBL()
		{
			_FileFormatTypeDB = new FileFormatTypeDB();
		}			
	
		public  void Add(FileFormatTypeEntity  FileFormatTypeEntityParam, out Guid FileFormatTypeId)
		{ 
			_FileFormatTypeDB.AddFileFormatTypeDB(FileFormatTypeEntityParam,out FileFormatTypeId);			
		}

		public  void Update(FileFormatTypeEntity  FileFormatTypeEntityParam)
		{
			_FileFormatTypeDB.UpdateFileFormatTypeDB(FileFormatTypeEntityParam);		
		}

		public  void Delete(FileFormatTypeEntity  FileFormatTypeEntityParam)
		{
			 _FileFormatTypeDB.DeleteFileFormatTypeDB(FileFormatTypeEntityParam);			
		}

		public  FileFormatTypeEntity GetSingleById(FileFormatTypeEntity  FileFormatTypeEntityParam)
		{
			FileFormatTypeEntity o = GetFileFormatTypeFromFileFormatTypeDB(
			_FileFormatTypeDB.GetSingleFileFormatTypeDB(FileFormatTypeEntityParam));
			
			return o;
		}

		public  List<FileFormatTypeEntity> GetAll()
		{
			List<FileFormatTypeEntity> lst = new List<FileFormatTypeEntity>();
			//string key = "FileFormatType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FileFormatTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetFileFormatTypeCollectionFromFileFormatTypeDBList(
				_FileFormatTypeDB.GetAllFileFormatTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<FileFormatTypeEntity> GetAllIsActive()
        {
            List<FileFormatTypeEntity> lst = new List<FileFormatTypeEntity>();
            //string key = "FileFormatType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<FileFormatTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetFileFormatTypeCollectionFromFileFormatTypeDBList(
            _FileFormatTypeDB.GetAllFileFormatTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<FileFormatTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "FileFormatType_List_Page_" + currentPage ;
			//string countKey = "FileFormatType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<FileFormatTypeEntity> lst = new List<FileFormatTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<FileFormatTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetFileFormatTypeCollectionFromFileFormatTypeDBList(
				_FileFormatTypeDB.GetPageFileFormatTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  FileFormatTypeEntity GetFileFormatTypeFromFileFormatTypeDB(FileFormatTypeEntity o)
		{
	if(o == null)
                return null;
			FileFormatTypeEntity ret = new FileFormatTypeEntity(o.FileFormatTypeId ,o.FileFormatTypeExtensionTitle ,o.FileFormatTypeMIMETitle ,o.IsActive ,o.CreationDate ,o.ModificationDate );
			return ret;
		}
		
		private  List<FileFormatTypeEntity> GetFileFormatTypeCollectionFromFileFormatTypeDBList( List<FileFormatTypeEntity> lst)
		{
			List<FileFormatTypeEntity> RetLst = new List<FileFormatTypeEntity>();
			foreach(FileFormatTypeEntity o in lst)
			{
				RetLst.Add(GetFileFormatTypeFromFileFormatTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}
	
		
	}



