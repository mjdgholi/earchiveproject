﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/10>
    /// Description: <موضوع>
    /// </summary>

	public class SubjectBL 
	{	
	  	 private readonly SubjectDB _SubjectDB;					
			
		public SubjectBL()
		{
			_SubjectDB = new SubjectDB();
		}			
	
		public  void Add(SubjectEntity  SubjectEntityParam, out Guid SubjectId)
		{ 
			_SubjectDB.AddSubjectDB(SubjectEntityParam,out SubjectId);			
		}

		public  void Update(SubjectEntity  SubjectEntityParam)
		{
			_SubjectDB.UpdateSubjectDB(SubjectEntityParam);		
		}

		public  void Delete(SubjectEntity  SubjectEntityParam)
		{
			 _SubjectDB.DeleteSubjectDB(SubjectEntityParam);			
		}

		public  SubjectEntity GetSingleById(SubjectEntity  SubjectEntityParam)
		{
			SubjectEntity o = GetSubjectFromSubjectDB(
			_SubjectDB.GetSingleSubjectDB(SubjectEntityParam));
			
			return o;
		}

		public  List<SubjectEntity> GetAll()
		{
			List<SubjectEntity> lst = new List<SubjectEntity>();
			//string key = "Subject_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SubjectEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetSubjectCollectionFromSubjectDBList(
				_SubjectDB.GetAllSubjectDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
        public List<SubjectEntity> GetAllIsActive()
        {
            List<SubjectEntity> lst = new List<SubjectEntity>();
            //string key = "Subject_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SubjectEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSubjectCollectionFromSubjectDBList(
            _SubjectDB.GetAllSubjectIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<SubjectEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Subject_List_Page_" + currentPage ;
			//string countKey = "Subject_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<SubjectEntity> lst = new List<SubjectEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<SubjectEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetSubjectCollectionFromSubjectDBList(
				_SubjectDB.GetPageSubjectDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  SubjectEntity GetSubjectFromSubjectDB(SubjectEntity o)
		{
	if(o == null)
                return null;
			SubjectEntity ret = new SubjectEntity(o.SubjectId ,o.SubjectTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<SubjectEntity> GetSubjectCollectionFromSubjectDBList( List<SubjectEntity> lst)
		{
			List<SubjectEntity> RetLst = new List<SubjectEntity>();
			foreach(SubjectEntity o in lst)
			{
				RetLst.Add(GetSubjectFromSubjectDB(o));
			}
			return RetLst;
			
		} 
						
	}

}

