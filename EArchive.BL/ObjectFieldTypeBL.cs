﻿using System;
using System.Collections.Generic;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/14>
    /// Description: <نوع شی پایگاه داده>
    /// </summary>


	public class ObjectFieldTypeBL 
	{	
	  	 private readonly ObjectFieldTypeDB _ObjectFieldTypeDB;					
			
		public ObjectFieldTypeBL()
		{
			_ObjectFieldTypeDB = new ObjectFieldTypeDB();
		}			
	
		public  void Add(ObjectFieldTypeEntity  ObjectFieldTypeEntityParam, out int ObjectFieldTypeId)
		{ 
			_ObjectFieldTypeDB.AddObjectFieldTypeDB(ObjectFieldTypeEntityParam,out ObjectFieldTypeId);			
		}

		public  void Update(ObjectFieldTypeEntity  ObjectFieldTypeEntityParam)
		{
			_ObjectFieldTypeDB.UpdateObjectFieldTypeDB(ObjectFieldTypeEntityParam);		
		}

		public  void Delete(ObjectFieldTypeEntity  ObjectFieldTypeEntityParam)
		{
			 _ObjectFieldTypeDB.DeleteObjectFieldTypeDB(ObjectFieldTypeEntityParam);			
		}

		public  ObjectFieldTypeEntity GetSingleById(ObjectFieldTypeEntity  ObjectFieldTypeEntityParam)
		{
			ObjectFieldTypeEntity o = GetObjectFieldTypeFromObjectFieldTypeDB(
			_ObjectFieldTypeDB.GetSingleObjectFieldTypeDB(ObjectFieldTypeEntityParam));
			
			return o;
		}

		public  List<ObjectFieldTypeEntity> GetAll()
		{
			List<ObjectFieldTypeEntity> lst = new List<ObjectFieldTypeEntity>();
			//string key = "ObjectFieldType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ObjectFieldTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetObjectFieldTypeCollectionFromObjectFieldTypeDBList(
				_ObjectFieldTypeDB.GetAllObjectFieldTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<ObjectFieldTypeEntity> GetAllIsActive()
        {
            List<ObjectFieldTypeEntity> lst = new List<ObjectFieldTypeEntity>();
            //string key = "ObjectFieldType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ObjectFieldTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetObjectFieldTypeCollectionFromObjectFieldTypeDBList(
            _ObjectFieldTypeDB.GetAllObjectFieldTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ObjectFieldTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ObjectFieldType_List_Page_" + currentPage ;
			//string countKey = "ObjectFieldType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ObjectFieldTypeEntity> lst = new List<ObjectFieldTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ObjectFieldTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetObjectFieldTypeCollectionFromObjectFieldTypeDBList(
				_ObjectFieldTypeDB.GetPageObjectFieldTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ObjectFieldTypeEntity GetObjectFieldTypeFromObjectFieldTypeDB(ObjectFieldTypeEntity o)
		{
	if(o == null)
                return null;
			ObjectFieldTypeEntity ret = new ObjectFieldTypeEntity(o.ObjectFieldTypeId ,o.ObjectFieldTypeTitle ,o.ObjectFieldTypeDescription ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ObjectFieldTypeEntity> GetObjectFieldTypeCollectionFromObjectFieldTypeDBList( List<ObjectFieldTypeEntity> lst)
		{
			List<ObjectFieldTypeEntity> RetLst = new List<ObjectFieldTypeEntity>();
			foreach(ObjectFieldTypeEntity o in lst)
			{
				RetLst.Add(GetObjectFieldTypeFromObjectFieldTypeDB(o));
			}
			return RetLst;
			
		}



	}

}
