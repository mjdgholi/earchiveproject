﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/13>
    /// Description: <فیلد>
    /// </summary>

    public class ObjectFieldBL
    {
        private readonly ObjectFieldDB _ObjectFieldDB;

        public ObjectFieldBL()
        {
            _ObjectFieldDB = new ObjectFieldDB();
        }

        public void Add(ObjectFieldEntity ObjectFieldEntityParam, out Guid ObjectFieldId)
        {
            _ObjectFieldDB.AddObjectFieldDB(ObjectFieldEntityParam, out ObjectFieldId);
        }

        public void Update(ObjectFieldEntity ObjectFieldEntityParam)
        {
            _ObjectFieldDB.UpdateObjectFieldDB(ObjectFieldEntityParam);
        }

        public void Delete(ObjectFieldEntity ObjectFieldEntityParam)
        {
            _ObjectFieldDB.DeleteObjectFieldDB(ObjectFieldEntityParam);
        }

        public ObjectFieldEntity GetSingleById(ObjectFieldEntity ObjectFieldEntityParam)
        {
            ObjectFieldEntity o = GetObjectFieldFromObjectFieldDB(
                _ObjectFieldDB.GetSingleObjectFieldDB(ObjectFieldEntityParam));

            return o;
        }

        public List<ObjectFieldEntity> GetAll()
        {
            List<ObjectFieldEntity> lst = new List<ObjectFieldEntity>();
            //string key = "ObjectField_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ObjectFieldEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetObjectFieldCollectionFromObjectFieldDBList(
                _ObjectFieldDB.GetAllObjectFieldDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }

        public List<ObjectFieldEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ObjectField_List_Page_" + currentPage ;
            //string countKey = "ObjectField_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ObjectFieldEntity> lst = new List<ObjectFieldEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ObjectFieldEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetObjectFieldCollectionFromObjectFieldDBList(
                _ObjectFieldDB.GetPageObjectFieldDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ObjectFieldEntity GetObjectFieldFromObjectFieldDB(ObjectFieldEntity o)
        {
            if (o == null)
                return null;
            ObjectFieldEntity ret = new ObjectFieldEntity(o.ObjectFieldId, o.ObjectFieldName, o.ObjectId,o.ObjectFieldTypeId, o.CreationDate,
                o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ObjectFieldEntity> GetObjectFieldCollectionFromObjectFieldDBList(List<ObjectFieldEntity> lst)
        {
            List<ObjectFieldEntity> RetLst = new List<ObjectFieldEntity>();
            foreach (ObjectFieldEntity o in lst)
            {
                RetLst.Add(GetObjectFieldFromObjectFieldDB(o));
            }
            return RetLst;

        }



        public List<ObjectFieldEntity> GetObjectFieldCollectionByObject(ObjectFieldEntity objectFieldEntityParam)
        {
            return
                GetObjectFieldCollectionFromObjectFieldDBList(
                    _ObjectFieldDB.GetObjectFieldDBCollectionByObjectDB(objectFieldEntityParam));
        }


        public void GetKeyAndTileForShowinSelectControl(EdocumentEntity edocumentEntity, out string  keyAndTileForShowinSelectControl)
        {
            _ObjectFieldDB.GetKeyAndTileForShowinSelectControlDB(edocumentEntity, out keyAndTileForShowinSelectControl);
        }
    }

}
