﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/22>
    /// Description: <سطح امنیت>
    /// </summary>

    public class SecurityLevelBL
    {
        private readonly SecurityLevelDB _SecurityLevelDB;

        public SecurityLevelBL()
        {
            _SecurityLevelDB = new SecurityLevelDB();
        }

        public void Add(SecurityLevelEntity SecurityLevelEntityParam, out Guid SecurityLevelId)
        {
            _SecurityLevelDB.AddSecurityLevelDB(SecurityLevelEntityParam, out SecurityLevelId);
        }

        public void Update(SecurityLevelEntity SecurityLevelEntityParam)
        {
            _SecurityLevelDB.UpdateSecurityLevelDB(SecurityLevelEntityParam);
        }

        public void Delete(SecurityLevelEntity SecurityLevelEntityParam)
        {
            _SecurityLevelDB.DeleteSecurityLevelDB(SecurityLevelEntityParam);
        }

        public SecurityLevelEntity GetSingleById(SecurityLevelEntity SecurityLevelEntityParam)
        {
            SecurityLevelEntity o = GetSecurityLevelFromSecurityLevelDB(
                _SecurityLevelDB.GetSingleSecurityLevelDB(SecurityLevelEntityParam));

            return o;
        }

        public List<SecurityLevelEntity> GetAll()
        {
            List<SecurityLevelEntity> lst = new List<SecurityLevelEntity>();
            //string key = "SecurityLevel_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SecurityLevelEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSecurityLevelCollectionFromSecurityLevelDBList(
                _SecurityLevelDB.GetAllSecurityLevelDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public object GetAllIsActive()
        {
            List<SecurityLevelEntity> lst = new List<SecurityLevelEntity>();
            //string key = "SecurityLevel_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SecurityLevelEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetSecurityLevelCollectionFromSecurityLevelDBList(
                _SecurityLevelDB.GetAllSecurityLevelIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<SecurityLevelEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "SecurityLevel_List_Page_" + currentPage ;
            //string countKey = "SecurityLevel_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<SecurityLevelEntity> lst = new List<SecurityLevelEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<SecurityLevelEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetSecurityLevelCollectionFromSecurityLevelDBList(
                _SecurityLevelDB.GetPageSecurityLevelDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private SecurityLevelEntity GetSecurityLevelFromSecurityLevelDB(SecurityLevelEntity o)
        {
            if (o == null)
                return null;
            SecurityLevelEntity ret = new SecurityLevelEntity(o.SecurityLevelId, o.SecurityLevelTitle, o.Priority,
                o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<SecurityLevelEntity> GetSecurityLevelCollectionFromSecurityLevelDBList(
            List<SecurityLevelEntity> lst)
        {
            List<SecurityLevelEntity> RetLst = new List<SecurityLevelEntity>();
            foreach (SecurityLevelEntity o in lst)
            {
                RetLst.Add(GetSecurityLevelFromSecurityLevelDB(o));
            }
            return RetLst;

        }


        
    }


}


