﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <درخت الگویی فهرست بندی>
    /// </summary>

    public class ContentTemplateTreeBL
    {
        private readonly ContentTemplateTreeDB _ContentTemplateTreeDB;

        public ContentTemplateTreeBL()
        {
            _ContentTemplateTreeDB = new ContentTemplateTreeDB();
        }

        public void Add(ContentTemplateTreeEntity ContentTemplateTreeEntityParam, out Guid ContentTemplateTreeId)
        {
            _ContentTemplateTreeDB.AddContentTemplateTreeDB(ContentTemplateTreeEntityParam, out ContentTemplateTreeId);
        }

        public void Update(ContentTemplateTreeEntity ContentTemplateTreeEntityParam)
        {
            _ContentTemplateTreeDB.UpdateContentTemplateTreeDB(ContentTemplateTreeEntityParam);
        }

        public void Delete(ContentTemplateTreeEntity ContentTemplateTreeEntityParam)
        {
            _ContentTemplateTreeDB.DeleteContentTemplateTreeDB(ContentTemplateTreeEntityParam);
        }

        public ContentTemplateTreeEntity GetSingleById(ContentTemplateTreeEntity ContentTemplateTreeEntityParam)
        {
            ContentTemplateTreeEntity o = GetContentTemplateTreeFromContentTemplateTreeDB(
                _ContentTemplateTreeDB.GetSingleContentTemplateTreeDB(ContentTemplateTreeEntityParam));

            return o;
        }

        public List<ContentTemplateTreeEntity> GetAll()
        {
            List<ContentTemplateTreeEntity> lst = new List<ContentTemplateTreeEntity>();
            //string key = "ContentTemplateTree_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ContentTemplateTreeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
                _ContentTemplateTreeDB.GetAllContentTemplateTreeDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<ContentTemplateTreeEntity> GetAllIsActive()
        {
            List<ContentTemplateTreeEntity> lst = new List<ContentTemplateTreeEntity>();
            //string key = "ContentTemplateTree_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ContentTemplateTreeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
                _ContentTemplateTreeDB.GetAllContentTemplateTreeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
        public List<ContentTemplateTreeEntity> GetAllPaging(int currentPage, int pageSize,
            string

                sortExpression, out int count, string whereClause)
        {
            //string key = "ContentTemplateTree_List_Page_" + currentPage ;
            //string countKey = "ContentTemplateTree_List_Page_" + currentPage + "_Count_" + count.ToString();
            List<ContentTemplateTreeEntity> lst = new List<ContentTemplateTreeEntity>();

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ContentTemplateTreeEntity>)HttpContext.Current.Cache[key];
            //	count = (int)HttpContext.Current.Cache[countkey];
            //}
            //else
            //{
            lst = GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
                _ContentTemplateTreeDB.GetPageContentTemplateTreeDB(pageSize, currentPage,
                    whereClause, sortExpression, out count));

            //	InsertIntoCache(lst, key, 600);
            //	InsertIntoCache(count, countkey, 600);
            return lst;
            //}

        }

        private ContentTemplateTreeEntity GetContentTemplateTreeFromContentTemplateTreeDB(ContentTemplateTreeEntity o)
        {
            if (o == null)
                return null;
            ContentTemplateTreeEntity ret = new ContentTemplateTreeEntity(o.ContentTemplateTreeId,
                o.ContentTemplateTreeOwnerId, o.ContentTemplateTreeTitle, o.ContentTemplateTreeChildNo, o.ObjectId,
                o.CreationDate, o.ModificationDate, o.IsActive);
            return ret;
        }

        private List<ContentTemplateTreeEntity> GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
            List<ContentTemplateTreeEntity> lst)
        {
            List<ContentTemplateTreeEntity> RetLst = new List<ContentTemplateTreeEntity>();
            foreach (ContentTemplateTreeEntity o in lst)
            {
                RetLst.Add(GetContentTemplateTreeFromContentTemplateTreeDB(o));
            }
            return RetLst;

        }



        public List<ContentTemplateTreeEntity> GetContentTemplateTreeCollectionByContentTemplateTree(
            ContentTemplateTreeEntity contentTemplateTreeEntityParam)
        {
            return
                GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
                    _ContentTemplateTreeDB.GetContentTemplateTreeDBCollectionByContentTemplateTreeDB(
                        contentTemplateTreeEntityParam));
        }

        public List<ContentTemplateTreeEntity> GetContentTemplateTreeCollectionByObject(
            ContentTemplateTreeEntity contentTemplateTreeEntityParam)
        {
            return
                GetContentTemplateTreeCollectionFromContentTemplateTreeDBList(
                    _ContentTemplateTreeDB.GetContentTemplateTreeDBCollectionByObjectDB(contentTemplateTreeEntityParam));
        }


    }
}

    

