﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/20>
    /// Description: <مستندالکترونیکی>
    /// </summary>

//-------------------------------------------------------


	/// <summary>
	/// 
	/// </summary>
	public class EdocumentBL 
	{	
	  	 private readonly EdocumentDB _EdocumentDB;					
			
		public EdocumentBL()
		{
			_EdocumentDB = new EdocumentDB();
		}			
	
		public  void Add(EdocumentEntity  edocumentEntityParam, out Guid EdocumentId)
		{ 
			_EdocumentDB.AddEdocumentDB(edocumentEntityParam,out EdocumentId);			
		}

		public  void Update(EdocumentEntity  edocumentEntityParam)
		{
			_EdocumentDB.UpdateEdocumentDB(edocumentEntityParam);		
		}

		public  void Delete(EdocumentEntity  edocumentEntityParam)
		{
			 _EdocumentDB.DeleteEdocumentDB(edocumentEntityParam);			
		}

		public  EdocumentEntity GetSingleById(EdocumentEntity  edocumentEntityParam)
		{
			EdocumentEntity o = GetEdocumentFromEdocumentDB(
			_EdocumentDB.GetSingleEdocumentDB(edocumentEntityParam));
			
			return o;
		}

		public  List<EdocumentEntity> GetAll()
		{
			List<EdocumentEntity> lst = new List<EdocumentEntity>();
			//string key = "Edocument_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EdocumentEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetEdocumentCollectionFromEdocumentDBList(
				_EdocumentDB.GetAllEdocumentDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}
		
		public  List<EdocumentEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "Edocument_List_Page_" + currentPage ;
			//string countKey = "Edocument_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<EdocumentEntity> lst = new List<EdocumentEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<EdocumentEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetEdocumentCollectionFromEdocumentDBList(
				_EdocumentDB.GetPageEdocumentDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  EdocumentEntity GetEdocumentFromEdocumentDB(EdocumentEntity o)
		{
	if(o == null)
                return null;
    EdocumentEntity ret = new EdocumentEntity(o.EdocumentId, o.EdocumentNo, o.EdocumentTitle, o.EDocumentDate, o.ContentTemplateTreeId, o.EDocumentContentId, o.ArchiveId, o.ScopeId, o.SubjectId, o.StatusId, o.Description, o.CreationDate, o.ModificationDate, o.ContentTemplateTreeTitle, o.EDocumentContentTitle);
			return ret;
		}
		
		private  List<EdocumentEntity> GetEdocumentCollectionFromEdocumentDBList( List<EdocumentEntity> lst)
		{
			List<EdocumentEntity> RetLst = new List<EdocumentEntity>();
			foreach(EdocumentEntity o in lst)
			{
				RetLst.Add(GetEdocumentFromEdocumentDB(o));
			}
			return RetLst;
			
		}




        public List<EdocumentEntity> GetEdocumentCollectionByArchive(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionByArchiveDB(edocumentEntityParam));
}

        public List<EdocumentEntity> GetEdocumentCollectionByContentTemplateTree(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionByContentTemplateTreeDB(edocumentEntityParam));
}

        public List<EdocumentEntity> GetEdocumentCollectionByEDocumentContent(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionByEDocumentContentDB(edocumentEntityParam));
}

        public List<EdocumentEntity> GetEdocumentCollectionByScope(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionByScopeDB(edocumentEntityParam));
}

        public List<EdocumentEntity> GetEdocumentCollectionByStatus(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionByStatusDB(edocumentEntityParam));
}

        public List<EdocumentEntity> GetEdocumentCollectionBySubject(EdocumentEntity edocumentEntityParam)
{
    return GetEdocumentCollectionFromEdocumentDBList(_EdocumentDB.GetEdocumentDBCollectionBySubjectDB(edocumentEntityParam));
}



}

}
