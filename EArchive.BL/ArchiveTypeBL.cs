﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/11>
    /// Description: <نوع بایگانی>
    /// </summary>

	public class ArchiveTypeBL 
	{	
	  	 private readonly ArchiveTypeDB _ArchiveTypeDB;					
			
		public ArchiveTypeBL()
		{
			_ArchiveTypeDB = new ArchiveTypeDB();
		}			
	
		public  void Add(ArchiveTypeEntity  ArchiveTypeEntityParam, out Guid ArchiveTypeId)
		{ 
			_ArchiveTypeDB.AddArchiveTypeDB(ArchiveTypeEntityParam,out ArchiveTypeId);			
		}

		public  void Update(ArchiveTypeEntity  ArchiveTypeEntityParam)
		{
			_ArchiveTypeDB.UpdateArchiveTypeDB(ArchiveTypeEntityParam);		
		}

		public  void Delete(ArchiveTypeEntity  ArchiveTypeEntityParam)
		{
			 _ArchiveTypeDB.DeleteArchiveTypeDB(ArchiveTypeEntityParam);			
		}

		public  ArchiveTypeEntity GetSingleById(ArchiveTypeEntity  ArchiveTypeEntityParam)
		{
			ArchiveTypeEntity o = GetArchiveTypeFromArchiveTypeDB(
			_ArchiveTypeDB.GetSingleArchiveTypeDB(ArchiveTypeEntityParam));
			
			return o;
		}

		public  List<ArchiveTypeEntity> GetAll()
		{
			List<ArchiveTypeEntity> lst = new List<ArchiveTypeEntity>();
			//string key = "ArchiveType_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ArchiveTypeEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetArchiveTypeCollectionFromArchiveTypeDBList(
				_ArchiveTypeDB.GetAllArchiveTypeDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<ArchiveTypeEntity> GetAllIsActive()
        {
            List<ArchiveTypeEntity> lst = new List<ArchiveTypeEntity>();
            //string key = "ArchiveType_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<ArchiveTypeEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetArchiveTypeCollectionFromArchiveTypeDBList(
            _ArchiveTypeDB.GetAllArchiveTypeIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<ArchiveTypeEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "ArchiveType_List_Page_" + currentPage ;
			//string countKey = "ArchiveType_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<ArchiveTypeEntity> lst = new List<ArchiveTypeEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<ArchiveTypeEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetArchiveTypeCollectionFromArchiveTypeDBList(
				_ArchiveTypeDB.GetPageArchiveTypeDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  ArchiveTypeEntity GetArchiveTypeFromArchiveTypeDB(ArchiveTypeEntity o)
		{
	if(o == null)
                return null;
			ArchiveTypeEntity ret = new ArchiveTypeEntity(o.ArchiveTypeId ,o.ArchiveTypeTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<ArchiveTypeEntity> GetArchiveTypeCollectionFromArchiveTypeDBList( List<ArchiveTypeEntity> lst)
		{
			List<ArchiveTypeEntity> RetLst = new List<ArchiveTypeEntity>();
			foreach(ArchiveTypeEntity o in lst)
			{
				RetLst.Add(GetArchiveTypeFromArchiveTypeDB(o));
			}
			return RetLst;
			
		} 
				
		
	}
}


