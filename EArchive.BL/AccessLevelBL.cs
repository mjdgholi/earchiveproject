﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Intranet.DesktopModules.EArchiveProject.EArchive.DAL;
using Intranet.DesktopModules.EArchiveProject.EArchive.Entity;

namespace Intranet.DesktopModules.EArchiveProject.EArchive.BL
{
    /// <summary>
    /// Author:		 <Narges.Kamran>
    /// Create date: <1393/03/12>
    /// Description: <سطح دسترسی>
    /// </summary>    
	public class AccessLevelBL 
	{	
	  	 private readonly AccessLevelDB _AccessLevelDB;					
			
		public AccessLevelBL()
		{
			_AccessLevelDB = new AccessLevelDB();
		}			
	
		public  void Add(AccessLevelEntity  AccessLevelEntityParam, out Guid AccessLevelId)
		{ 
			_AccessLevelDB.AddAccessLevelDB(AccessLevelEntityParam,out AccessLevelId);			
		}

		public  void Update(AccessLevelEntity  AccessLevelEntityParam)
		{
			_AccessLevelDB.UpdateAccessLevelDB(AccessLevelEntityParam);		
		}

		public  void Delete(AccessLevelEntity  AccessLevelEntityParam)
		{
			 _AccessLevelDB.DeleteAccessLevelDB(AccessLevelEntityParam);			
		}

		public  AccessLevelEntity GetSingleById(AccessLevelEntity  AccessLevelEntityParam)
		{
			AccessLevelEntity o = GetAccessLevelFromAccessLevelDB(
			_AccessLevelDB.GetSingleAccessLevelDB(AccessLevelEntityParam));
			
			return o;
		}

		public  List<AccessLevelEntity> GetAll()
		{
			List<AccessLevelEntity> lst = new List<AccessLevelEntity>();
			//string key = "AccessLevel_List";
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<AccessLevelEntity>)HttpContext.Current.Cache[key];
			//}
			//else
			//{
				lst = GetAccessLevelCollectionFromAccessLevelDBList(
				_AccessLevelDB.GetAllAccessLevelDB());
				return lst;
			//	InsertIntoCache(lst, key, 600);
			//}
		}

        public List<AccessLevelEntity> GetAllIsActive()
        {
            List<AccessLevelEntity> lst = new List<AccessLevelEntity>();
            //string key = "AccessLevel_List";

            //if(HttpContext.Current.Cache[key] != null)
            //{
            //	lst = (List<AccessLevelEntity>)HttpContext.Current.Cache[key];
            //}
            //else
            //{
            lst = GetAccessLevelCollectionFromAccessLevelDBList(
            _AccessLevelDB.GetAllAccessLevelIsActiveDB());
            return lst;
            //	InsertIntoCache(lst, key, 600);
            //}
        }
		public  List<AccessLevelEntity> GetAllPaging(int currentPage, int pageSize, 
														string 
	
sortExpression, out int count, string whereClause)
		{			
			//string key = "AccessLevel_List_Page_" + currentPage ;
			//string countKey = "AccessLevel_List_Page_" + currentPage + "_Count_" + count.ToString();
			List<AccessLevelEntity> lst = new List<AccessLevelEntity>();
			
			//if(HttpContext.Current.Cache[key] != null)
			//{
			//	lst = (List<AccessLevelEntity>)HttpContext.Current.Cache[key];
			//	count = (int)HttpContext.Current.Cache[countkey];
			//}
			//else
			//{
				lst = GetAccessLevelCollectionFromAccessLevelDBList(
				_AccessLevelDB.GetPageAccessLevelDB(pageSize, currentPage, 
				 whereClause, sortExpression, out count));
				 
			//	InsertIntoCache(lst, key, 600);
			//	InsertIntoCache(count, countkey, 600);
			    return lst;
			//}
		  
		}
		
		private  AccessLevelEntity GetAccessLevelFromAccessLevelDB(AccessLevelEntity o)
		{
	if(o == null)
                return null;
			AccessLevelEntity ret = new AccessLevelEntity(o.AccessLevelId ,o.AccessLevelTitle ,o.CreationDate ,o.ModificationDate ,o.IsActive );
			return ret;
		}
		
		private  List<AccessLevelEntity> GetAccessLevelCollectionFromAccessLevelDBList( List<AccessLevelEntity> lst)
		{
			List<AccessLevelEntity> RetLst = new List<AccessLevelEntity>();
			foreach(AccessLevelEntity o in lst)
			{
				RetLst.Add(GetAccessLevelFromAccessLevelDB(o));
			}
			return RetLst;
			
		} 
				
		
	}

}

